﻿namespace labFormTransparency
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Fm));
            this.buClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buClose
            // 
            this.buClose.Location = new System.Drawing.Point(118, 180);
            this.buClose.Name = "buClose";
            this.buClose.Size = new System.Drawing.Size(75, 23);
            this.buClose.TabIndex = 0;
            this.buClose.Text = "Close";
            this.buClose.UseVisualStyleBackColor = true;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(510, 503);
            this.Controls.Add(this.buClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Fm";
            this.Text = "labFormTransparency";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buClose;
    }
}

