﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labButtonsOnGrid
{
    public partial class Fm : Form
    {
        public int Rows { get; private set; }
        public int Cols { get; private set; }
        private Button[,] bu;

        public Fm()
        {
            InitializeComponent();

            textBox1.Text = "1";
            textBox2.Text = "1";

            button1.Click += BuGo_Click;

            //HW 
            //counts
            //reaction on moving of mouse

        }

        private void BuGo_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text == "") || (textBox2.Text == ""))
            {
                MessageBox.Show("Введите количество строк и столбцов больше 0 и меньше 10");
            }
            else
            {
                if ((Convert.ToInt32(textBox1.Text) > 0) && (Convert.ToInt32(textBox2.Text) > 0) && (Convert.ToInt32(textBox1.Text) < 10) && (Convert.ToInt32(textBox2.Text) < 10))
                {
                    Del();
                    Cols = Convert.ToInt32(textBox2.Text);
                    Rows = Convert.ToInt32(textBox1.Text);
                    CreateButtons();
                    ResizeButtons();
                    this.Resize += (s, e) => ResizeButtons();
                }
                else
                    MessageBox.Show("Введите количество строк и столбцов больше 0 и меньше 10");

            }

        }

        private void ResizeButtons()
        {

            int xCellWidth = this.ClientSize.Width / Cols;
            int xCellHeight = (this.ClientSize.Height - this.groupBox1.Height) / Rows;

            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    bu[i, j].Width = xCellWidth;
                    bu[i, j].Height = xCellHeight;
                    bu[i, j].Location = new Point(xCellWidth * j, xCellHeight * i + this.groupBox1.Height);
                }
        }

        private void CreateButtons()
        {
            bu = new Button[Rows, Cols];
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    bu[i, j] = new Button();
                        if (i == 0)
                            bu[i, j].Text = (j + 1).ToString();
                        else
                            bu[i, j].Text = (Cols*i+1+j).ToString();
                  

                    bu[i, j].Font = new Font("Segoe UI", 20);
                    bu[i, j].Click += BuAll_Click;
                    this.Controls.Add(bu[i, j]);
                    bu[i, j].MouseEnter += buAll_MouseEnter;
                    bu[i, j].MouseLeave += buAll_MouseLeave;
                }
        }

        private void buAll_MouseLeave(object sender, EventArgs e)
        {
            if (sender is Control x)
            {
                x.Font = new Font(x.Font, FontStyle.Regular);
                x.BackColor = Color.FromArgb(225,225,225);
            }
        }

        private void buAll_MouseEnter(object sender, EventArgs e)
        {
            if (sender is Control x)
            {
                x.Font = new Font(x.Font, FontStyle.Bold);
                x.BackColor = Color.LightGreen;
            }
        }
        private void Del()
        {
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    this.Controls.Remove(bu[i, j]);
                }

        }

        private void BuAll_Click(object sender, EventArgs e)
        {
            if (sender is Button x)
                MessageBox.Show(x.Text);
        }


    }
}
