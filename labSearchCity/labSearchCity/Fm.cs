﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labSearchCity
{
    public partial class Fm : Form
    {
        private string[] arr;

        public Fm()
        {
            InitializeComponent();

            arr = Properties.Resources.goroda.Split('\n');
            edSearch.TextChanged += EdSearch_TextChanged;

            //
        }

        private void EdSearch_TextChanged(object sender, EventArgs e)
        {
            this.Text = $"{Application.ProductName} : {edSearch.Text}";

            var r = arr.Where(v => v.ToUpper().Contains(edSearch.Text.ToUpper()))
                .Where(v => v.Length > 0)
                .OrderBy(v => v)
                .ToArray();
            edResult.Text = string.Join(Environment.NewLine, r);
            this.Text += $": Count = {r.Count()}";
        }
    }
}
