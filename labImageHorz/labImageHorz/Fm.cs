﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;

namespace labImageHorz
{
    public partial class Fm : Form
    {
        private readonly Bitmap b;
        private readonly Graphics g;
        private Bitmap imBG, imMount, imGrass, imTree;
        private int deltaX;
        private Point startPoint;
        System.Threading.Thread hold;
        int speed = 2;

        public Fm()
        {
            InitializeComponent();

            imBG = Properties.Resources.fonbig;
            imMount = new Bitmap(Properties.Resources.mountain, imBG.Width, imBG.Height);
            imGrass = new Bitmap(Properties.Resources.grass, imBG.Width+200, imBG.Height+250);
            imTree = new Bitmap(Properties.Resources.Tree, 100, 100);
            this.Height = imBG.Height;

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);

            this.DoubleBuffered = true;
            this.Paint += (s, e) => { UpdateBG(); e.Graphics.DrawImage(b, 0, 0); };

            this.MouseDown += (s, e) => startPoint = e.Location;
            this.MouseMove += Fm_MouseMove;
            this.KeyDown += Fm_KeyDown;
            this.KeyUp += Fm_KeyUp;
           // HW
           // research background asking "background hor game"
           // different speed bg

        }

        private void Fm_KeyUp(object sender, KeyEventArgs e)
        {
            speed = 2;
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            hold = new System.Threading.Thread(() => ButtonIsHeld());
            hold.Start();

            switch (e.KeyCode)
            {
                case Keys.Left:
                    UpdateDeltaX(speed);
                    break;
                case Keys.Right:
                    UpdateDeltaX(-speed);
                    break;
                default:
                    break;
            }
            this.Invalidate();
        }

        private void ButtonIsHeld()
        {
            System.Threading.Thread.Sleep(1000);
            if (speed < 20)
            {
            speed *= 2;
            }
        }

        private void Fm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                UpdateDeltaX(e.X - startPoint.X);
                startPoint = e.Location;
                this.Invalidate();
            }
        }

        private void UpdateDeltaX(int v)
        {
            this.Text = $"{Application.ProductName}: {deltaX}, {v}";
            deltaX += v;
            if (deltaX > 0)
                deltaX -= imBG.Width;
            else
                if (deltaX < -imBG.Width)
                deltaX += imBG.Width;
        }

        private void UpdateBG()
        {
            for (int i = 0; i < Screen.PrimaryScreen.Bounds.Width/imBG.Width*3; i++)
            {
            g.DrawImage(imBG, deltaX + i*imBG.Width, 0);
            g.DrawImage(imMount, deltaX + i* imMount.Width, 0);
            }
            for (int i = 0; i < Screen.PrimaryScreen.Bounds.Width / imGrass.Width * 4; i++)
            {
            g.DrawImage(imGrass, Convert.ToInt32(deltaX * 1.3) + i* imGrass.Width, 0);
            }
            for (int i = 0; i < Screen.PrimaryScreen.Bounds.Width / imTree.Width * 1; i++)
            { 
            g.DrawImage(imTree, Convert.ToInt32(deltaX * 2) + i* imTree.Width, imBG.Height-170);
            }
        }
    }
}
