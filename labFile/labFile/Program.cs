﻿using System;
using System.IO;

namespace labFile
{
    class Program
    {
        static void Main(string[] args)
        {
            //string path = @"C:\temp\temp.txt";
            //string path = "C:\\temp\\temp.txt";

            string path1 = Path.Combine(Directory.GetCurrentDirectory(), "temp123.txt");
            //string path2 = Path.Combine(Path.GetTempPath(), "temp123.txt");
            //Console.WriteLine(path1);
            //Console.WriteLine(path2);

            var s = new string[] { "Orel", "Tula", "Kursk", "Samara" };

            File.WriteAllText(path1, string.Join(Environment.NewLine, s));

            var s2 = File.ReadAllText(path1);
            Console.WriteLine(s2);

            Console.WriteLine("____________");
            Console.WriteLine(File.Exists(path1));

            File.Delete(path1);

            FileInfo fileInfo = new FileInfo(path1);
            Console.WriteLine(fileInfo.FullName);
            Console.WriteLine(fileInfo.Name); 

        }
    }
}
