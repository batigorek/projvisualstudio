﻿namespace labPaint
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buLine = new System.Windows.Forms.Button();
            this.buKar = new System.Windows.Forms.Button();
            this.buPrint = new System.Windows.Forms.Button();
            this.buDrawRectangle = new System.Windows.Forms.Button();
            this.buDrawCircle = new System.Windows.Forms.Button();
            this.buDrawTriangle = new System.Windows.Forms.Button();
            this.trPenRightWidth = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pxColorRight = new System.Windows.Forms.PictureBox();
            this.pxColorLeft = new System.Windows.Forms.PictureBox();
            this.buAddRandomStars = new System.Windows.Forms.Button();
            this.buLoadFromFile = new System.Windows.Forms.Button();
            this.buSaveToFile = new System.Windows.Forms.Button();
            this.buImageClear = new System.Windows.Forms.Button();
            this.trPenLeftWidth = new System.Windows.Forms.TrackBar();
            this.pxColor4 = new System.Windows.Forms.PictureBox();
            this.pxColor3 = new System.Windows.Forms.PictureBox();
            this.pxColor2 = new System.Windows.Forms.PictureBox();
            this.pxColor1 = new System.Windows.Forms.PictureBox();
            this.pxImage = new System.Windows.Forms.PictureBox();
            this.buFill = new System.Windows.Forms.Button();
            this.buPip = new System.Windows.Forms.Button();
            this.buText = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trPenRightWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColorRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColorLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trPenLeftWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buText);
            this.panel1.Controls.Add(this.buFill);
            this.panel1.Controls.Add(this.buPip);
            this.panel1.Controls.Add(this.buLine);
            this.panel1.Controls.Add(this.buKar);
            this.panel1.Controls.Add(this.buPrint);
            this.panel1.Controls.Add(this.buDrawRectangle);
            this.panel1.Controls.Add(this.buDrawCircle);
            this.panel1.Controls.Add(this.buDrawTriangle);
            this.panel1.Controls.Add(this.trPenRightWidth);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pxColorRight);
            this.panel1.Controls.Add(this.pxColorLeft);
            this.panel1.Controls.Add(this.buAddRandomStars);
            this.panel1.Controls.Add(this.buLoadFromFile);
            this.panel1.Controls.Add(this.buSaveToFile);
            this.panel1.Controls.Add(this.buImageClear);
            this.panel1.Controls.Add(this.trPenLeftWidth);
            this.panel1.Controls.Add(this.pxColor4);
            this.panel1.Controls.Add(this.pxColor3);
            this.panel1.Controls.Add(this.pxColor2);
            this.panel1.Controls.Add(this.pxColor1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 593);
            this.panel1.TabIndex = 0;
            // 
            // buLine
            // 
            this.buLine.Location = new System.Drawing.Point(14, 330);
            this.buLine.Name = "buLine";
            this.buLine.Size = new System.Drawing.Size(158, 25);
            this.buLine.TabIndex = 8;
            this.buLine.Text = "Line";
            this.buLine.UseVisualStyleBackColor = true;
            // 
            // buKar
            // 
            this.buKar.Location = new System.Drawing.Point(14, 299);
            this.buKar.Name = "buKar";
            this.buKar.Size = new System.Drawing.Size(158, 25);
            this.buKar.TabIndex = 7;
            this.buKar.Text = "Karandash";
            this.buKar.UseVisualStyleBackColor = true;
            // 
            // buPrint
            // 
            this.buPrint.Location = new System.Drawing.Point(14, 529);
            this.buPrint.Name = "buPrint";
            this.buPrint.Size = new System.Drawing.Size(158, 23);
            this.buPrint.TabIndex = 4;
            this.buPrint.Text = "Print";
            this.buPrint.UseVisualStyleBackColor = true;
            // 
            // buDrawRectangle
            // 
            this.buDrawRectangle.Location = new System.Drawing.Point(14, 237);
            this.buDrawRectangle.Name = "buDrawRectangle";
            this.buDrawRectangle.Size = new System.Drawing.Size(158, 25);
            this.buDrawRectangle.TabIndex = 2;
            this.buDrawRectangle.Text = "Draw rectangle";
            this.buDrawRectangle.UseVisualStyleBackColor = true;
            // 
            // buDrawCircle
            // 
            this.buDrawCircle.Location = new System.Drawing.Point(14, 206);
            this.buDrawCircle.Name = "buDrawCircle";
            this.buDrawCircle.Size = new System.Drawing.Size(158, 25);
            this.buDrawCircle.TabIndex = 2;
            this.buDrawCircle.Text = "Draw Circle";
            this.buDrawCircle.UseVisualStyleBackColor = true;
            // 
            // buDrawTriangle
            // 
            this.buDrawTriangle.Location = new System.Drawing.Point(14, 175);
            this.buDrawTriangle.Name = "buDrawTriangle";
            this.buDrawTriangle.Size = new System.Drawing.Size(158, 25);
            this.buDrawTriangle.TabIndex = 2;
            this.buDrawTriangle.Text = "Draw triangle";
            this.buDrawTriangle.UseVisualStyleBackColor = true;
            // 
            // trPenRightWidth
            // 
            this.trPenRightWidth.Location = new System.Drawing.Point(94, 124);
            this.trPenRightWidth.Minimum = 1;
            this.trPenRightWidth.Name = "trPenRightWidth";
            this.trPenRightWidth.Size = new System.Drawing.Size(78, 45);
            this.trPenRightWidth.TabIndex = 1;
            this.trPenRightWidth.Value = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(94, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "ПКМ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "ЛКМ";
            // 
            // pxColorRight
            // 
            this.pxColorRight.BackColor = System.Drawing.Color.Red;
            this.pxColorRight.Location = new System.Drawing.Point(133, 73);
            this.pxColorRight.Name = "pxColorRight";
            this.pxColorRight.Size = new System.Drawing.Size(35, 45);
            this.pxColorRight.TabIndex = 0;
            this.pxColorRight.TabStop = false;
            // 
            // pxColorLeft
            // 
            this.pxColorLeft.BackColor = System.Drawing.Color.Red;
            this.pxColorLeft.Location = new System.Drawing.Point(53, 73);
            this.pxColorLeft.Name = "pxColorLeft";
            this.pxColorLeft.Size = new System.Drawing.Size(35, 45);
            this.pxColorLeft.TabIndex = 0;
            this.pxColorLeft.TabStop = false;
            // 
            // buAddRandomStars
            // 
            this.buAddRandomStars.Location = new System.Drawing.Point(12, 558);
            this.buAddRandomStars.Name = "buAddRandomStars";
            this.buAddRandomStars.Size = new System.Drawing.Size(160, 23);
            this.buAddRandomStars.TabIndex = 5;
            this.buAddRandomStars.Text = "Add random Stars";
            this.buAddRandomStars.UseVisualStyleBackColor = true;
            // 
            // buLoadFromFile
            // 
            this.buLoadFromFile.Location = new System.Drawing.Point(14, 500);
            this.buLoadFromFile.Name = "buLoadFromFile";
            this.buLoadFromFile.Size = new System.Drawing.Size(158, 23);
            this.buLoadFromFile.TabIndex = 4;
            this.buLoadFromFile.Text = "Load from file";
            this.buLoadFromFile.UseVisualStyleBackColor = true;
            // 
            // buSaveToFile
            // 
            this.buSaveToFile.Location = new System.Drawing.Point(14, 471);
            this.buSaveToFile.Name = "buSaveToFile";
            this.buSaveToFile.Size = new System.Drawing.Size(158, 23);
            this.buSaveToFile.TabIndex = 3;
            this.buSaveToFile.Text = "Save to file";
            this.buSaveToFile.UseVisualStyleBackColor = true;
            // 
            // buImageClear
            // 
            this.buImageClear.Location = new System.Drawing.Point(14, 268);
            this.buImageClear.Name = "buImageClear";
            this.buImageClear.Size = new System.Drawing.Size(158, 25);
            this.buImageClear.TabIndex = 2;
            this.buImageClear.Text = "Clear";
            this.buImageClear.UseVisualStyleBackColor = true;
            // 
            // trPenLeftWidth
            // 
            this.trPenLeftWidth.Location = new System.Drawing.Point(10, 124);
            this.trPenLeftWidth.Minimum = 1;
            this.trPenLeftWidth.Name = "trPenLeftWidth";
            this.trPenLeftWidth.Size = new System.Drawing.Size(78, 45);
            this.trPenLeftWidth.TabIndex = 1;
            this.trPenLeftWidth.Value = 1;
            // 
            // pxColor4
            // 
            this.pxColor4.BackColor = System.Drawing.Color.MediumOrchid;
            this.pxColor4.Location = new System.Drawing.Point(135, 12);
            this.pxColor4.Name = "pxColor4";
            this.pxColor4.Size = new System.Drawing.Size(35, 45);
            this.pxColor4.TabIndex = 0;
            this.pxColor4.TabStop = false;
            // 
            // pxColor3
            // 
            this.pxColor3.BackColor = System.Drawing.Color.Yellow;
            this.pxColor3.Location = new System.Drawing.Point(94, 12);
            this.pxColor3.Name = "pxColor3";
            this.pxColor3.Size = new System.Drawing.Size(35, 45);
            this.pxColor3.TabIndex = 0;
            this.pxColor3.TabStop = false;
            // 
            // pxColor2
            // 
            this.pxColor2.BackColor = System.Drawing.Color.Blue;
            this.pxColor2.Location = new System.Drawing.Point(53, 12);
            this.pxColor2.Name = "pxColor2";
            this.pxColor2.Size = new System.Drawing.Size(35, 45);
            this.pxColor2.TabIndex = 0;
            this.pxColor2.TabStop = false;
            // 
            // pxColor1
            // 
            this.pxColor1.BackColor = System.Drawing.Color.Red;
            this.pxColor1.Location = new System.Drawing.Point(12, 12);
            this.pxColor1.Name = "pxColor1";
            this.pxColor1.Size = new System.Drawing.Size(35, 45);
            this.pxColor1.TabIndex = 0;
            this.pxColor1.TabStop = false;
            // 
            // pxImage
            // 
            this.pxImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pxImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pxImage.Location = new System.Drawing.Point(200, 0);
            this.pxImage.Name = "pxImage";
            this.pxImage.Size = new System.Drawing.Size(600, 593);
            this.pxImage.TabIndex = 1;
            this.pxImage.TabStop = false;
            // 
            // buFill
            // 
            this.buFill.Location = new System.Drawing.Point(14, 361);
            this.buFill.Name = "buFill";
            this.buFill.Size = new System.Drawing.Size(158, 23);
            this.buFill.TabIndex = 10;
            this.buFill.Text = "Залить";
            this.buFill.UseVisualStyleBackColor = true;
            // 
            // buPip
            // 
            this.buPip.Location = new System.Drawing.Point(14, 390);
            this.buPip.Name = "buPip";
            this.buPip.Size = new System.Drawing.Size(158, 23);
            this.buPip.TabIndex = 9;
            this.buPip.Text = "Пипетка";
            this.buPip.UseVisualStyleBackColor = true;
            // 
            // buText
            // 
            this.buText.Location = new System.Drawing.Point(14, 419);
            this.buText.Name = "buText";
            this.buText.Size = new System.Drawing.Size(158, 23);
            this.buText.TabIndex = 11;
            this.buText.Text = "Текст";
            this.buText.UseVisualStyleBackColor = true;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 593);
            this.Controls.Add(this.pxImage);
            this.Controls.Add(this.panel1);
            this.Name = "Fm";
            this.Text = "labPaint";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trPenRightWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColorRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColorLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trPenLeftWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TrackBar trPenLeftWidth;
        private System.Windows.Forms.PictureBox pxColor4;
        private System.Windows.Forms.PictureBox pxColor3;
        private System.Windows.Forms.PictureBox pxColor2;
        private System.Windows.Forms.PictureBox pxColor1;
        private System.Windows.Forms.PictureBox pxImage;
        private System.Windows.Forms.Button buImageClear;
        private System.Windows.Forms.Button buAddRandomStars;
        private System.Windows.Forms.Button buLoadFromFile;
        private System.Windows.Forms.Button buSaveToFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pxColorRight;
        private System.Windows.Forms.PictureBox pxColorLeft;
        private System.Windows.Forms.TrackBar trPenRightWidth;
        private System.Windows.Forms.Button buDrawRectangle;
        private System.Windows.Forms.Button buDrawCircle;
        private System.Windows.Forms.Button buDrawTriangle;
        private System.Windows.Forms.Button buPrint;
        private System.Windows.Forms.Button buLine;
        private System.Windows.Forms.Button buKar;
        private System.Windows.Forms.Button buFill;
        private System.Windows.Forms.Button buPip;
        private System.Windows.Forms.Button buText;
    }
}

