﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Configuration;
using System.Drawing.Printing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPaint
{
    public partial class Fm : Form
    {

        enum MyDrawMode
        {
            Pencil,
            Line,
            Circle,
            Rectangle,
            Text
        }
        private Bitmap b;
        private Graphics g;
        private MyDrawMode myDrawMode = MyDrawMode.Pencil;
        private Point startMouseDown;
        private Bitmap bb;
        private Pen myPenLeft;
        private Pen myPenRight;
        TextBox tb;
        public Fm()
        {
            InitializeComponent();

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            myPenLeft = new Pen(pxColorLeft.BackColor, 10);
            myPenLeft.StartCap = myPenLeft.EndCap = System.Drawing.Drawing2D.LineCap.Round;

            myPenRight = new Pen(pxColorRight.BackColor, 10);
            myPenRight.StartCap = myPenRight.EndCap = System.Drawing.Drawing2D.LineCap.Round;

            pxImage.MouseDown += PxImage_MouseDown;
            pxImage.MouseMove += PxImage_MouseMove;
            pxImage.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);

            trPenLeftWidth.Value = Convert.ToInt32(myPenLeft.Width);
            trPenLeftWidth.ValueChanged += (s, e) => myPenLeft.Width = trPenLeftWidth.Value;
            
            trPenRightWidth.Value = Convert.ToInt32(myPenRight.Width);
            trPenRightWidth.ValueChanged += (s, e) => myPenRight.Width = trPenRightWidth.Value;

            // перенес в PxColorAll_MouseDown
            //pxColor1.Click += (s, e) => myPen.Color = pxColor1.BackColor;
            //pxColor2.Click += (s, e) => myPen.Color = pxColor2.BackColor;
            //pxColor3.Click += (s, e) => myPen.Color = pxColor3.BackColor;
            //pxColor4.Click += (s, e) => myPen.Color = pxColor4.BackColor;

            pxColor1.MouseDown += PxColorAll_MouseDown;
            pxColor2.MouseDown += PxColorAll_MouseDown;
            pxColor3.MouseDown += PxColorAll_MouseDown;
            pxColor4.MouseDown += PxColorAll_MouseDown;

            pxColor1.MouseDoubleClick += PxColorAll_MouseDoubleClick;
            pxColor2.MouseDoubleClick += PxColorAll_MouseDoubleClick;
            pxColor3.MouseDoubleClick += PxColorAll_MouseDoubleClick;
            pxColor4.MouseDoubleClick += PxColorAll_MouseDoubleClick;

           // pxImage.Click += PxImage_Click;

            buImageClear.Click += delegate
            {
                g.Clear(SystemColors.Control);
                pxImage.Invalidate();
            };

            buSaveToFile.Click += BuSaveToFile_Click;
            buLoadFromFile.Click += BuLoadFromFile_Click;
            buAddRandomStars.Click += BuAddRandomStars_Click;
            buPrint.Click += BuPrint_Click;

            buDrawRectangle.Click += BuDrawRectangle_Click;
            buKar.Click += (s, e) => myDrawMode = MyDrawMode.Pencil;
            buLine.Click += (s, e) => myDrawMode = MyDrawMode.Line;
            buDrawCircle.Click += (s,e) => myDrawMode = MyDrawMode.Circle;
            buText.Click += BuText_Click;

            //HW
            // add more figures
            // draw with fill or without
            // add button copy image to buffer
            // drawing rect
            // выделение и перемещение прямоугольной области
            // добавить палитру (пипетка для определения цвета
            // текст, заливка
            // ластик
            // вращение изображения
        }

        private void BuText_Click(object sender, EventArgs e)
        {
            tb = new TextBox();
            tb.Font = new Font("", 20, FontStyle.Regular);
            tb.BackColor = Color.White;
            tb.Location = new Point(startMouseDown.X, startMouseDown.Y);
            myDrawMode = MyDrawMode.Text;
        }

        private void PxImage_MouseDown(object sender, MouseEventArgs e)
        {
            startMouseDown = e.Location;
            bb = (Bitmap)b.Clone();
        }

        private void BuPrint_Click(object sender, EventArgs e)
        {
            PrintDialog dialog = new PrintDialog();

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                PrintDocument pd = new PrintDocument();
                pd.PrintPage += (sender, args) =>
                {
                    Image i = b;
                    Point p = new Point(100, 100);
                    args.Graphics.DrawImage(i, 10, 10, i.Width, i.Height);
                };
                pd.Print();
            }
        }

        //private void PxImage_Click(object sender, EventArgs e) 
        //{
        //    throw new NotImplementedException();
        //}

        private void BuDrawRectangle_Click(object sender, EventArgs e)
        {
            myDrawMode = MyDrawMode.Rectangle;
        }

        private void PxColorAll_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (sender is PictureBox x)
                {
                    ColorDialog colorDialog = new ColorDialog();
                    colorDialog.Color = x.BackColor;
                    if (colorDialog.ShowDialog() == DialogResult.OK)
                    {
                        x.BackColor = colorDialog.Color;
                        myPenLeft.Color = x.BackColor;
                    }
                }
            }
        }

        private void BuAddRandomStars_Click(object sender, EventArgs e)
        {
            var rnd = new Random();
            for (int i = 0; i < 300; i++)
            {
                g.FillEllipse(new SolidBrush(Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256))),
                    rnd.Next(b.Width),
                    rnd.Next(b.Height),
                    rnd.Next(1,10),
                    rnd.Next(1,10)
                    );
            }
            pxImage.Invalidate();
        }

        private void BuLoadFromFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
                if (dialog.ShowDialog() == DialogResult.OK)
            {
                g.Clear(SystemColors.Control);
                g.DrawImage(Bitmap.FromFile(dialog.FileName), 0, 0);
                pxImage.Invalidate();
            }
        }

        private void BuSaveToFile_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "JPG Files(*.JPEG)|*.JPG";
            if (dialog.ShowDialog() ==DialogResult.OK) 
            {
                b.Save(dialog.FileName);
            }
        }

        private void PxColorAll_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (sender is PictureBox x)
                {
                    myPenLeft.Color = x.BackColor;
                    pxColorLeft.BackColor = x.BackColor;
                }
            } else if (e.Button == MouseButtons.Right)
            {
                if (sender is PictureBox x)
                {
                    myPenRight.Color = x.BackColor;
                    pxColorRight.BackColor = x.BackColor;
                }
            }
        }

        private void PxImage_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                switch (myDrawMode)
                {
                    case MyDrawMode.Pencil:
                        g.DrawLine(myPenLeft, startMouseDown, e.Location);
                        startMouseDown = e.Location;
                        break;
                    case MyDrawMode.Line:
                        restoreBitmap();

                        g.DrawLine(myPenLeft, startMouseDown, e.Location);
                        break;
                    case MyDrawMode.Circle:
                        restoreBitmap();

                        g.DrawEllipse(myPenLeft,new Rectangle(startMouseDown.X,startMouseDown.Y,e.X -startMouseDown.X,e.Y -startMouseDown.Y));
                        break;
                    case MyDrawMode.Rectangle:
                        restoreBitmap();
                        var p1 = new Point(Math.Min(startMouseDown.X, e.X - startMouseDown.X), Math.Min(startMouseDown.Y, e.Y - startMouseDown.Y));
                        var p2 = new Point(Math.Max(startMouseDown.X, e.X - startMouseDown.X), Math.Max(startMouseDown.Y, e.Y - startMouseDown.Y));
                        g.DrawRectangle(myPenLeft,
                            new Rectangle(p1.X, p1.Y, p2.X, p2.Y)); //todo
                        
                        //
                        //g.DrawRectangle(myPenLeft,
                        //    new Rectangle(startMouseDown.X, startMouseDown.Y, e.X - startMouseDown.X, e.Y - startMouseDown.Y)); //todo

                        break;
                    case MyDrawMode.Text:
                        tb.Size = new Size((startMouseDown.X - e.X), (startMouseDown.Y - e.Y));
                        this.Controls.Add(tb);
                        Brush b = Brushes.Black;
                        g.DrawString("STRING", new Font("", 20, FontStyle.Regular), b, new Point(startMouseDown.X - e.X, startMouseDown.Y - e.Y));
                        break;
                    default:
                        
                        break;
                }
            pxImage.Invalidate();
            } else if (e.Button == MouseButtons.Right)
            {
                g.DrawLine(myPenRight, startMouseDown, e.Location);
                startMouseDown = e.Location;
                pxImage.Invalidate();
            }


        }

        private void restoreBitmap()
        {
            g.Dispose();
            b = (Bitmap)bb.Clone();
            g = Graphics.FromImage(b);
        }
    }
}
