﻿namespace labGraphicsOfFunction
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbFunction = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbC = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbA = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buShow = new System.Windows.Forms.Button();
            this.buClose = new System.Windows.Forms.Button();
            this.pxImage = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // cbFunction
            // 
            this.cbFunction.FormattingEnabled = true;
            this.cbFunction.Items.AddRange(new object[] {
            "Ax+B",
            "Ax^2+Bx+C",
            "A/Bx"});
            this.cbFunction.Location = new System.Drawing.Point(47, 139);
            this.cbFunction.Name = "cbFunction";
            this.cbFunction.Size = new System.Drawing.Size(196, 23);
            this.cbFunction.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.cbFunction);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.tbC);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.tbB);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.tbA);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.buShow);
            this.panel1.Controls.Add(this.buClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(280, 450);
            this.panel1.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(47, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(196, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Выберите подходящее уравнение:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(47, 213);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(180, 15);
            this.label4.TabIndex = 4;
            this.label4.Text = "Введите значения переменных:";
            // 
            // tbC
            // 
            this.tbC.Location = new System.Drawing.Point(112, 308);
            this.tbC.Name = "tbC";
            this.tbC.Size = new System.Drawing.Size(100, 23);
            this.tbC.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(66, 311);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "C = ";
            // 
            // tbB
            // 
            this.tbB.Location = new System.Drawing.Point(112, 279);
            this.tbB.Name = "tbB";
            this.tbB.Size = new System.Drawing.Size(100, 23);
            this.tbB.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 282);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "B = ";
            // 
            // tbA
            // 
            this.tbA.Location = new System.Drawing.Point(112, 250);
            this.tbA.Name = "tbA";
            this.tbA.Size = new System.Drawing.Size(100, 23);
            this.tbA.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(66, 253);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "A = ";
            // 
            // buShow
            // 
            this.buShow.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.buShow.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buShow.Location = new System.Drawing.Point(0, 395);
            this.buShow.Name = "buShow";
            this.buShow.Size = new System.Drawing.Size(278, 53);
            this.buShow.TabIndex = 1;
            this.buShow.Text = "Show";
            this.buShow.UseVisualStyleBackColor = false;
            // 
            // buClose
            // 
            this.buClose.BackColor = System.Drawing.Color.Red;
            this.buClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buClose.Dock = System.Windows.Forms.DockStyle.Top;
            this.buClose.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.buClose.FlatAppearance.BorderSize = 0;
            this.buClose.Location = new System.Drawing.Point(0, 0);
            this.buClose.Name = "buClose";
            this.buClose.Size = new System.Drawing.Size(278, 27);
            this.buClose.TabIndex = 0;
            this.buClose.Text = "Close";
            this.buClose.UseVisualStyleBackColor = false;
            // 
            // pxImage
            // 
            this.pxImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pxImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pxImage.Location = new System.Drawing.Point(280, 0);
            this.pxImage.Name = "pxImage";
            this.pxImage.Size = new System.Drawing.Size(520, 450);
            this.pxImage.TabIndex = 1;
            this.pxImage.TabStop = false;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pxImage);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Fm";
            this.Text = "labGraphicsOfFunction";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pxImage;
        private System.Windows.Forms.Button buClose;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbC;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbA;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buShow;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbFunction;
    }
}

