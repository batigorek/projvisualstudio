﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace labGraphicsOfFunction
{
    public partial class Fm : Form
    {

        private readonly Bitmap b;
        private readonly Graphics g;
        private Pen PenBack, PenCenter, PenFunction;

        public Fm()
        {
            InitializeComponent();

            b = new Bitmap(pxImage.Width, pxImage.Height);
            g = Graphics.FromImage(b);
            pxImage.Image = b;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g.TranslateTransform(pxImage.Width / 2, pxImage.Height / 2);

            PenBack = new Pen(Color.Black, 1);
            PenCenter = new Pen(Color.Black, 2);
            PenFunction = new Pen(Color.Red, 2);
            PenBack.StartCap = PenBack.EndCap = System.Drawing.Drawing2D.LineCap.Round;
            PenCenter.StartCap = PenCenter.EndCap = System.Drawing.Drawing2D.LineCap.Round;
            PenFunction.StartCap = PenFunction.EndCap = System.Drawing.Drawing2D.LineCap.Round;

            cbFunction.SelectedIndex = 0;

            tbA.Text = "1";
            tbB.Text = "1";
            tbC.Text = "1";

            buClose.Click += (s, e) => this.Close();
            buShow.Click += BuShow_Click;
            DrawLines();
        }

        private void BuShow_Click(object sender, EventArgs e)
        {
            g.Clear(Color.White);
            DrawLines();
            int A = Convert.ToInt32(tbA.Text);
            int B = Convert.ToInt32(tbB.Text);
            int C = Convert.ToInt32(tbC.Text);
            
            switch (cbFunction.SelectedIndex)
            {
                case 0:
                    for (int i = -pxImage.Width/2; i < pxImage.Width; i+=10)
                    {
                        g.DrawLine(PenFunction, new Point(i-10, -(A * (i - 10) + B)), new Point(i, - (A * i + B)));
                        
                    }
                    break;
                case 1:
                    for (int i = -pxImage.Width / 2; i < pxImage.Width; i += 10)
                    {
                        g.DrawLine(PenFunction, new Point(i - 10, -(A * Convert.ToInt32(Math.Pow((i - 10), 2)) + B*(i - 10) + C)), new Point(i, -(A * Convert.ToInt32(Math.Pow(i, 2)) + B*i + C)));

                    }
                    break;
                case 2:
                    for (int i = -pxImage.Width / 2; i < pxImage.Width; i += 10)
                    {
                        if ((B*i != 0)&&(B*(i-10)!=0))
                        {
                        g.DrawLine(PenFunction, new Point(i - 10, -(A/(B * (i - 10)))), new Point(i, -(A/(B * i))));
                        }

                    }
                    break;
                //case 3:
                //    for (int i = -pxImage.Width / 2; i < pxImage.Width; i += 10)
                //    {
                //        g.DrawLine(myPenRight, new Point(i - 10, -Convert.ToInt32(Math.Cos(A * (i - 10)))), new Point(i, -Convert.ToInt32(Math.Cos(A * i))));
                //    }
                //    break;
                //case 4:
                //    for (int i = -pxImage.Width / 2; i < pxImage.Width; i += 10)
                //    {
                //        g.DrawLine(myPenRight, new Point(i - 10, -Convert.ToInt32(Math.Sin(A * (i - 10)))), new Point(i, -Convert.ToInt32(Math.Sin(A * i))));
                //    }
                //    break;
                default:
                    break;
            }
            pxImage.Invalidate();
        }

        public void DrawLines()
        {
            g.DrawLine(PenCenter, new Point(0, -pxImage.Height/2), new Point(0, pxImage.Height/2));
            g.DrawLine(PenCenter, new Point(-pxImage.Width/2, 0), new Point(pxImage.Width/2, 0));
            for (int i = 10; i <= pxImage.Width / 2; i += 10)
            {
                g.DrawLine(PenBack, new Point(-i, -pxImage.Height / 2), new Point( -i, pxImage.Height/2));
                g.DrawLine(PenBack, new Point( i, -pxImage.Height / 2), new Point(i, pxImage.Height/2));

                g.DrawLine(PenBack, new Point(-pxImage.Width / 2, -i), new Point(pxImage.Width/2, -i));
                g.DrawLine(PenBack, new Point(-pxImage.Width / 2, i), new Point(pxImage.Width/2, i));
                pxImage.Invalidate();
            }
        }
    }
}
