﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labFormSDI
{
    public partial class FmMainMenu : Form
    {
        private FmNote fmNote = new FmNote();

        public FmMainMenu()
        {
            InitializeComponent();

            button1.Click += (s, e) => new FmNote().Show();
            button2.Click += (s, e) => new FmNote().ShowDialog();
            button3.Click += (s, e) => fmNote.Show();
        }

    }
}
