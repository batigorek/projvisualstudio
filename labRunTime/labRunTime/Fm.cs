﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labRunTime
{
    public partial class Fm : Form
    {
        public Fm()
        {
            InitializeComponent();

            button1.Click += Button1_Click;
        }

        private void Button1_Click(object sender, EventArgs e)
        {

            var beginRunTime = Stopwatch.StartNew();
            int r;
            for (int i = 0; i < 100000000; i++)
            {
                r = i * 123;
            }

            beginRunTime.Stop();
            var resultRunTime = beginRunTime.Elapsed;
            button1.Text = $"{resultRunTime}";
        }
    }
}
