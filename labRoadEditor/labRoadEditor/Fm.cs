﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labRoadEditor
{
    public partial class Fm : Form
    {
        private readonly Bitmap b;
        private PictureBox chosen;
        private readonly Graphics g;
        private readonly ImageBox imageBox;
        private int CellX, CellY;
        

        public Fm()
        {
            InitializeComponent();

            b = new Bitmap(panel2.Width, panel2.Height);
            //b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);

            imageBox = new ImageBox(Properties.Resources.preview, 3, 4);

            this.DoubleBuffered = true;
            this.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);

            ShowRoads();
            DrawCells();
            buClear.Click += BuClear_Click;
        }

        private void BuClear_Click(object sender, EventArgs e)
        {
            //TODO

        }

        private void DrawCells()
        {
            CellX = b.Width / 10;
            CellY = b.Height / 10;
            for (int i = 0; i < CellX; i++)
            {
                for (int j = 0; j < CellY; j++)
                {
                    var a = new PictureBox();
                    a.Size = new Size(CellX, CellY);
                    a.Location = new Point(i * a.Width, j * a.Height);
                    a.BorderStyle = BorderStyle.FixedSingle;
                    a.Visible = true;
                    this.Controls.Add(a);
                }
            }
        }

        private void ShowRoads()
        {
            pictureBox1.Image = imageBox.images[0];
            pictureBox2.Image = imageBox.images[1];
            pictureBox3.Image = imageBox.images[2];
            pictureBox4.Image = imageBox.images[3];
            pictureBox5.Image = imageBox.images[4];
            pictureBox6.Image = imageBox.images[5];
            pictureBox7.Image = imageBox.images[6];
            pictureBox8.Image = imageBox.images[7];
            pictureBox9.Image = imageBox.images[8];
            pictureBox10.Image = imageBox.images[9];
            pictureBox11.Image = imageBox.images[10];
            pictureBox12.Image = imageBox.images[11];

            pictureBox1.MouseDown += PictureBoxAll_MouseDown;
            pictureBox2.MouseDown += PictureBoxAll_MouseDown;
            pictureBox3.MouseDown += PictureBoxAll_MouseDown;
            pictureBox4.MouseDown += PictureBoxAll_MouseDown;
            pictureBox5.MouseDown += PictureBoxAll_MouseDown;
            pictureBox6.MouseDown += PictureBoxAll_MouseDown;
            pictureBox7.MouseDown += PictureBoxAll_MouseDown;
            pictureBox8.MouseDown += PictureBoxAll_MouseDown;
            pictureBox9.MouseDown += PictureBoxAll_MouseDown;
            pictureBox10.MouseDown += PictureBoxAll_MouseDown;
            pictureBox11.MouseDown += PictureBoxAll_MouseDown;
            pictureBox12.MouseDown += PictureBoxAll_MouseDown;

            pictureBox1.MouseUp += PictureBoxAll_MouseUp;
        }

        private void PictureBoxAll_MouseUp(object sender, MouseEventArgs e)
        {
            chosen.BorderStyle = BorderStyle.FixedSingle;
            if (e.Button == MouseButtons.Left)
            {
                if (sender is Control x)
                {
                    var xLocation = x.Location;
                    for (int i = 0; i < imageBox.Rows; i++)
                    {
                        for (int j = 0; j < imageBox.Cols; j++)
                        {
                            if (x.Location.X > j * CellX - HOLD_PIXEL && x.Location.X < j * CellX + HOLD_PIXEL)
                                xLocation.X = j * CellX;
                            if (x.Location.Y > i * CellY - HOLD_PIXEL && x.Location.Y < i * CellY + HOLD_PIXEL)
                                xLocation.Y = i * CellY;
                        }
                    }
                    x.Location = xLocation;
                }

            }
        }

        //bool flag = false;

        public int HOLD_PIXEL { get; private set; } = 5;

        private void PictureBoxAll_MouseDown(object sender, MouseEventArgs e)
        {
            chosen = (PictureBox) sender;
            startMouseDown = e.Location;
            var a = new PictureBox();
            a.Size = new Size(CellX, CellY);
            a.Location = chosen.Location;
            a.BorderStyle = BorderStyle.FixedSingle;
            a.Visible = true;
            a.Image = chosen.Image;
            a.MouseMove += A_MouseMove;
            a.SizeMode = PictureBoxSizeMode.Zoom;
            this.Controls.Add(a);
            a.BringToFront();
            chosen.BorderStyle = BorderStyle.Fixed3D;
        }

        private Point startMouseDown;

        private void A_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (sender is Control x)
                {
                    x.Location = new Point(
                        x.Location.X + e.X - startMouseDown.X,
                        x.Location.Y + e.Y - startMouseDown.Y);

                    var xLocation = x.Location;
                    for (int i = 0; i < imageBox.Rows; i++)
                    {
                        for (int j = 0; j < imageBox.Cols; j++)
                        {
                            if (x.Location.X > j * CellX - HOLD_PIXEL && x.Location.X < j * CellX + HOLD_PIXEL)
                                xLocation.X = j * CellX;
                            if (x.Location.Y > i * CellY - HOLD_PIXEL && x.Location.Y < i * CellY + HOLD_PIXEL)
                                xLocation.Y = i * CellY;
                        }
                    }
                    x.Location = xLocation;
                }
            }
        }
    }
}
