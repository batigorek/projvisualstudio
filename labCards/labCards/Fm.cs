﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labCards
{
    public partial class Fm : Form
    {
        private readonly Bitmap b;
        private readonly Graphics g;
        private  Gamer gamer;
        private int gamerid = 0;
        int i, x, y;

        public Fm()
        {
            InitializeComponent();

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);

            //gamer = new Gamer();

            //imageBox = new ImageBox(Properties.Resources.cards, 5, 13, 4*13+16);



            

            this.DoubleBuffered = true;
            this.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);

            this.Click += Fm_Click;
            this.MouseHover += Fm_MouseHover;
            this.MouseMove += Fm_MouseMove;

            // HW
            // search images
            // change imagebox with cutting
            // веер карт поворот нижний центр (по f3) and moving by mouse (отдельный буфер)
            // change card size by mouse wheel
        }

        private void Fm_MouseMove(object sender, MouseEventArgs e)
        {
            x = e.X;
            y = e.Y;
            this.Text = $"{Application.ProductName}: ({e.X},{e.Y}) : {x}, {y}";
        }

        private void Fm_MouseHover(object sender, EventArgs e)
        {
            //int y = 5;
            //i = 0;
           // DrawingCard(true);
        }

        private void Fm_Click(object sender, EventArgs e)
        {
            //g.DrawImage(imageBox[1], 0, 0);
            //Random rnd = new Random();

            gamer = new Gamer();
            i = 0;
            //x = rnd.Next(100, Screen.PrimaryScreen.Bounds.Width);
            //y = rnd.Next(100, Screen.PrimaryScreen.Bounds.Height);
            //g.DrawImage(imageBox[rnd.Next(imageBox.Count)], 0, 0);
            DrawingCard(true);
            gamerid++;
        }

        public void DrawingCard(bool flag)
        {
           
                i++;
                //g.TranslateTransform(0, gamer.getHeigth());
                if (flag == true)
                {
                    g.RotateTransform(-i*5.0F);
                    g.DrawImage(gamer.getIndex(), // need cardPack instead of imageBox
                   x- i*50,
                   y - (i*2*gamer.getHeigth())/3);
                    g.RotateTransform(i*5.0F);
                }
                else
                {
                    g.RotateTransform(i*5.0F);
                    g.DrawImage(gamer.getIndex(), // need cardPack instead of imageBox
                   x - i*50,
                   y - (i*2 * gamer.getHeigth()) / 3);
                    g.RotateTransform(-i*5.0F);
                }
                //g.TranslateTransform(0, -gamer.getHeigth());
                flag = true ? false : true;
            if (gamer.getLength() > i)
                DrawingCard(flag);

            this.Invalidate();
            
        }
    }
}
