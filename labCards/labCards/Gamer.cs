﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace labCards
{
    class Gamer
    {  
        private CardPack cp;
        private readonly ImageBox imageBox;
            Random rnd = new Random();
        private int[] arr;
        private int i = 0;

        public Gamer()
        {
            imageBox = new ImageBox(Properties.Resources.cards, 4, 13);

            arr = new int[6];
            cp = new CardPack(imageBox.Count);
            cp.CardRandom();
        }

        public Image getIndex()
        {
            arr[i] = rnd.Next(imageBox.Count);
            return imageBox[cp[arr[i++]]];
        }

        public int getLength()
        {
            return arr.Length;
        }

        public int getHeigth()
        {
            return imageBox.he;
        }
    }
}
