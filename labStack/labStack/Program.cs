﻿using System;
using System.Collections;

namespace labStack
{
    class Program
    {
        static void Main(string[] args)
        {

            // HW
            // labStackT

            Stack x = new Stack();
            x.Push("Text");
            x.Push(123);
            x.Push("Text1");

            Console.WriteLine(x.Peek()); //watch
            Console.WriteLine("_________");

            Console.WriteLine(x.Pop()); //out of stack
            Console.WriteLine(x.Pop()); //out of stack
            Console.WriteLine(x.Pop()); //out of stack

            #region //here try-catch block
            try
            {
            Console.WriteLine(x.Pop()); //out of stack 

            }
            catch (Exception ex)
            {

                Console.WriteLine($"Something goes wrong: {ex.Message}");
            }
            #endregion
        }
    }
}
