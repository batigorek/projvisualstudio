﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPyatnashki
{
    public partial class Fm : Form
    {
        public int Rows { get; private set; } = 3;
        public int Cols { get; private set; } = 3;
        private Pyat p;
        public Button[,] bu;
        int hh = 0, mm = 0, ss = 0;
        TimeSpan duration;
        private System.Timers.Timer aTimer;

        public Fm()
        {
            InitializeComponent();

            p = new Pyat();

            bu = new Button[Rows, Cols];
            CreateButtons();
            ResizeButtons();
            this.Resize += (s, e) => ResizeButtons();

            buShake.Click += ButtonShake_Click;
            buPlus.Click += ButtonPlus_Click;
            buMinus.Click += ButtonMinus_Click;
            this.KeyPreview = true;
            this.KeyUp += Fm_KeyUp;
            
            // логика отдельно от интерфейса пользователя, т.е. логика в классах 
        }

        private void Fm_KeyUp(object sender, KeyEventArgs e)
        {
           if ((p.FindRow(bu) > 0)|| (p.FindCol(bu) > 0) || (p.FindRow(bu) < Rows) || (p.FindCol(bu) < Cols))
           { 
            switch (e.KeyCode)
            {
                case Keys.Left:
                    p.Move(bu[p.FindRow(bu), p.FindCol(bu) + 1], bu);
                    break;
                case Keys.Right:
                    p.Move(bu[p.FindRow(bu), p.FindCol(bu) - 1], bu);
                    break;
                case Keys.Up:
                    p.Move(bu[p.FindRow(bu) + 1, p.FindCol(bu)], bu);
                    break;
                case Keys.Down:
                    p.Move(bu[p.FindRow(bu) - 1, p.FindCol(bu)], bu);
                    break;
                default:
                    break;
            }
           }
        }

        private void ButtonMinus_Click(object sender, EventArgs e)
        {
            aTimer.Stop();
            hh = 0; mm = 0; ss = 0;
            if (Rows > 3)
            {
                Rows--;
                Cols--;
                Del();
                CreateButtons();
                ResizeButtons();
                this.Resize += (s, e) => ResizeButtons();
                
            }
        }

        private void ButtonPlus_Click(object sender, EventArgs e)
        {
            aTimer.Stop();
            hh = 0; mm = 0; ss = 0;
            if (Rows < 5)
            {
                Rows++;
                Cols++;
                Del();
                CreateButtons();
                ResizeButtons();
                this.Resize += (s, e) => ResizeButtons();
            }
        }

        private void ButtonShake_Click(object sender, EventArgs e)
        {
            aTimer.Stop();
            hh = 0; mm = 0; ss = 0;
            Del();
            CreateButtons();
            ResizeButtons();
            this.Resize += (s, e) => ResizeButtons();
        }

        private void ResizeButtons()
        {

            int xCellWidth = this.ClientSize.Width / Cols;
            int xCellHeight = (this.ClientSize.Height - this.groupBox1.Height) / Rows;

            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    
                    bu[i, j].Width = xCellWidth;
                    bu[i, j].Height = xCellHeight;
                    bu[i, j].Location = new Point(xCellWidth * j, xCellHeight * i + this.groupBox1.Height);

                }
        }

        private void CreateButtons()
        {
            Random rnd = new Random();
            setTimer();

            
            
            int n = 0;
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                        bu[i, j] = new Button();
                        bu[i, j].Text = n.ToString();
                        bu[i, j].Font = new Font("Segoe UI", 20);
                        this.Controls.Add(bu[i, j]);
                        bu[i, j].Click += buAll_Click;
                    n++;
                }
            bu[0, 0].Text = "";
            p.Name(bu);
            p.times = 0;
            label1.Text = $"Количество нажатий: {p.times}";
        }
       

        private void buAll_Click(object sender, EventArgs e)
        {
            if (sender is Control x)
            {
                p.Move(x, bu);
            }
            label1.Text = $"Количество нажатий: {p.times}";
            Check(bu);
        }

        public void Check(Button[,] bu)
        {
            int n;
            if (bu[0,0].Text == "") n = 0;
            else n = Convert.ToInt32(bu[0, 0].Text);
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                    if (bu[i, j].Text == n.ToString())
                    {
                        n++;
                    }
            if (n == (Rows * Cols - 1))
            {
                aTimer.Stop();
                MessageBox.Show("Вы победили!");
                //if (MessageBoxButtons.OK.)
                //{

               // }
            }
        }

        public void Del()
        {
            for (int i = 0; i < bu.GetLength(0); i++)
                for (int j = 0; j < bu.GetLength(1); j++)
                {
                    Controls.Remove(bu[i, j]);
                }

        }


        private void setTimer()
        {
            timer.Text = String.Format("Затрачено времени: {0:c}", duration);
            aTimer = new System.Timers.Timer(1000);
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        private void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            if (++ss == 60)
            {
                ss = 0;
                mm++;
            }
            if (mm == 60)
            {
                mm = 0;
                hh++;
            }
            if (hh == 24) hh = 0;

            duration = new TimeSpan(hh, mm, ss);

            try
            {
                this.Invoke((MethodInvoker)delegate ()
                {
                    timer.Text = String.Format("Затрачено времени: {0:c}", duration);
                });
            }
            catch (Exception)
            {

            }
        }
    }
}
