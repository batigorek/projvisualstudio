﻿namespace labPyatnashki
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.timer = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buShake = new System.Windows.Forms.Button();
            this.buMinus = new System.Windows.Forms.Button();
            this.buPlus = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.timer);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.buShake);
            this.groupBox1.Controls.Add(this.buMinus);
            this.groupBox1.Controls.Add(this.buPlus);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(462, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
           
            // 
            // timer
            // 
            this.timer.AutoSize = true;
            this.timer.Location = new System.Drawing.Point(296, 58);
            this.timer.Name = "timer";
            this.timer.Size = new System.Drawing.Size(164, 15);
            this.timer.TabIndex = 3;
            this.timer.Text = "Затрачено времени: 00:00:00";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(296, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Количество нажатий: 0";
            // 
            // buShake
            // 
            this.buShake.Location = new System.Drawing.Point(149, 22);
            this.buShake.Name = "buShake";
            this.buShake.Size = new System.Drawing.Size(141, 52);
            this.buShake.TabIndex = 2;
            this.buShake.Text = "Перемешать поле";
            this.buShake.UseVisualStyleBackColor = true;
            // 
            // buMinus
            // 
            this.buMinus.Location = new System.Drawing.Point(14, 58);
            this.buMinus.Name = "buMinus";
            this.buMinus.Size = new System.Drawing.Size(129, 42);
            this.buMinus.TabIndex = 1;
            this.buMinus.Text = "Уменьшить поле";
            this.buMinus.UseVisualStyleBackColor = true;
            // 
            // buPlus
            // 
            this.buPlus.Location = new System.Drawing.Point(14, 10);
            this.buPlus.Name = "buPlus";
            this.buPlus.Size = new System.Drawing.Size(129, 42);
            this.buPlus.TabIndex = 0;
            this.buPlus.Text = "Увеличить поле";
            this.buPlus.UseVisualStyleBackColor = true;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 571);
            this.Controls.Add(this.groupBox1);
            this.Name = "Fm";
            this.Text = "labPyatnashki";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buMinus;
        private System.Windows.Forms.Button buPlus;
        private System.Windows.Forms.Button buShake;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label timer;
    }
}

