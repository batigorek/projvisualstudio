﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace labPyatnashki
{
    class Pyat
    {
        //private Button[,] bu;
        public int times = 0;
        int row = 0, col = 1;

        public Pyat()
        {

        }

        public void Move(Control x, Button[,] bu)
        {
            FindRow(bu);
            FindCol(bu);


            for (int i = 0; i < bu.GetLength(0); i++)
                for (int j = 0; j < bu.GetLength(1); j++)
                    if (bu[i, j].Text == "")
                    {
                        row = i;
                        col = j;
                    }

                if (row != 0)
                {
                    if (x.Text == bu[row - 1, col].Text)
                    {
                        bu[row, col].Text = x.Text;
                        bu[row, col].Font = new Font("Segoe UI", 20);
                        x.Text = "";
                    }
                }
                if (row != (Convert.ToInt32(bu.GetLength(0)) - 1))
                {
                    if (x.Text == bu[row + 1, col].Text)
                    {
                        bu[row, col].Text = x.Text;
                        bu[row, col].Font = new Font("Segoe UI", 20);
                        x.Text = "";
                    }
                }
                if (col != 0)
                {
                        if (x.Text == bu[row, col - 1].Text)
                        {
                            bu[row, col].Text = x.Text;
                            bu[row, col].Font = new Font("Segoe UI", 20);
                            x.Text = "";
                        }
                }
                if (col != (Convert.ToInt32(bu.GetLength(1)) - 1))
                {
                    if (x.Text == bu[row, col + 1].Text)
                    {
                        bu[row, col].Text = x.Text;
                        bu[row, col].Font = new Font("Segoe UI", 20);
                        x.Text = "";
                    }
                }
               

            times++;

           // Check(bu);
        }

        public int FindRow(Button[,] bu)
        {
            

            for (int i = 0; i < bu.GetLength(0); i++)
                for (int j = 0; j < bu.GetLength(1); j++)
                    if (bu[i, j].Text == "")
                    {
                        row = i;
                    }
            return row;
        }

        public int FindCol(Button[,] bu)
        {

            for (int i = 0; i < bu.GetLength(0); i++)
                for (int j = 0; j < bu.GetLength(1); j++)
                    if (bu[i, j].Text == "")
                    {
                        col = j;
                    }
            return col;
        }

        public void Name(Button[,] bu)
        {
            Random rnd = new Random();
            int count = 0, i, j;
            //string nam;
                    while (count != bu.GetLength(0) * bu.GetLength(1))
                    {
                        i = rnd.Next(0, bu.GetLength(0));
                        j = rnd.Next(0, bu.GetLength(1));
                        bu[i, j].Font = new Font("Segoe UI", 20);
                        if (bu[i, j].Text == (bu.GetLength(0) * bu.GetLength(1)).ToString()) continue;
                        //nam = bu[i, j].Text;
                        Move(bu[i,j],bu);
                        //bu[i, j].Text = rnd.Next(1, bu.GetLength(0) * bu.GetLength(1)).ToString();
                        count++;
                    }
        }

        //public void Check(Button[,] bu)
        //{
        //    int n=Convert.ToInt32(bu[0,0].Text);
        //    for (int i = 0; i < bu.GetLength(0); i++)
        //        for (int j = 0; j < bu.GetLength(1); j++)
        //            if (bu[i, j].Text == n.ToString())
        //            {
        //                n++;
        //            }
        //    if (n == (bu.GetLength(0)*bu.GetLength(1)-1))
        //    {
        //    MessageBox.Show("Вы победили!");
        //    }
        //}
    }
}
