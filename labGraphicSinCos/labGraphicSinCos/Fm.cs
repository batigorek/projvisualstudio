﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labGraphicSinCos
{
    public partial class Fm : Form
    {
        private const int DOT_RADIUS = 5;

        public Fm()
        {
            InitializeComponent();

            this.BackgroundImageLayout = ImageLayout.None;

            DrawAll();
            this.Resize += (s, e) => DrawAll();
            this.Text += ": (Sin - red, Cos - green, Tan - blue)";

            // hw
            // graphics by ellipse and lines...
            // 
        }

        private void DrawAll()
        {
            var b = new Bitmap(this.ClientSize.Width, this.ClientSize.Height);
            var g = Graphics.FromImage(b);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            var grCount = 5; //vawes
            var grShiftY = b.Height / 2;
            var grHeight = grShiftY*0.8;
            var grWidthPI = Math.PI / b.Width;

            //OX
            g.DrawLine(new Pen(Color.Black), 0, grShiftY, b.Width, grShiftY);
            for (int i = 0; i < grCount; i++)
            {
                var pointX = b.Width / grCount * i;
                g.DrawLine(Pens.Black, pointX, grShiftY - 5, pointX, grShiftY + 5);
                g.DrawString($"{i}PI", new Font("", 10), new SolidBrush(Color.Black), pointX - 10, grShiftY + 5);
            }


            //OY
            g.DrawLine(Pens.Black, 0, 0, 0, b.Height);
            g.DrawLine(Pens.Black, -5, (int)(grShiftY - grHeight), +5, (int)(grShiftY - grHeight));
            g.DrawString("+1", new Font("", 10), new SolidBrush(Color.Black), +10, grShiftY - (int)grHeight - 5);
            g.DrawString("-1", new Font("", 10), new SolidBrush(Color.Black), +10, grShiftY + (int)grHeight - 5);

            //Graphics
            float x;
            float y;
            for (int i = 0; i < b.Width; i++)
            {
                x = i;
                y = (float)(grHeight * -Math.Sin(i * grCount * grWidthPI)+grShiftY);
                g.FillEllipse(new SolidBrush(Color.Red), PointToRectangle(x, y));

                y = (float)(grHeight * -Math.Cos(i * grCount * grWidthPI) + grShiftY);
                g.FillEllipse(new SolidBrush(Color.Green), PointToRectangle(x, y));

                y = (float)(grHeight * -Math.Tan(i * grCount * grWidthPI) + grShiftY);
                if (y > 0 && y < b.Height)
                g.FillEllipse(new SolidBrush(Color.Blue), PointToRectangle(x, y));

                y = (float)(grHeight * -Math.Sin(i *5* grCount * grWidthPI)*i/grHeight/3 + grShiftY);
                g.FillEllipse(new SolidBrush(Color.Orange), PointToRectangle(x, y));
            }


            this.BackgroundImage = (Bitmap)b.Clone();
        }

        private RectangleF PointToRectangle(float x, float y) => PointToRectangle(x, y, DOT_RADIUS);

        private RectangleF PointToRectangle(float x, float y, int r) => new RectangleF(x - r, y - r, r * 2, r * 2);
    }
}
