﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace SpanishTrainer
{
    internal partial class Fm2 : Form
    {
        private Label label1;
        private Panel panel1;
        private Button buStart;
        private CheckBox cbChoose;
        private CheckBox cbEdSp;
        private CheckBox cbEdRu;
        private CheckBox cbRu;
        private CheckBox cbSp;
        private Label laWord;
        private Button buAnswerOne;
        private Button buAnswerTwo;
        private Button buAnswerThree;
        private Button buAnswerFour;
        private TextBox edUnswer;
        private Button buTrue;
        private Button buFalse;
        private Label laStatics;
        private Button buStop;
        private CheckBox cbTrueOrNot;
        private Label laAnswer;
        Fm1 frm1 = new Fm1();
        Fm frm = new Fm();
        private int ind = 0, an = 0, one = 0, two = 0, three = 0, four = 0;
        private int countTrue, countFalse;
        private Button buPrint;
        List<int> arr = new List<int>();
        private string result;

        public Fm2()
        {
            InitializeComponent();
            ChangeVis();
            //this.Close();
            buStart.Click += BuStart_Click;
            buTrue.Click += BuTrue_Click;
            buFalse.Click += BuFalse_Click;
            buAnswerOne.Click += BuAnswerOne_Click;
            buAnswerTwo.Click += BuAnswerTwo_Click;
            buAnswerThree.Click += BuAnswerThree_Click;
            buAnswerFour.Click += BuAnswerFour_Click;
            buStop.Click += BuStop_Click;
            edUnswer.KeyDown += EdUnswer_KeyDown;
            buPrint.Click += BuPrint_Click;

            this.FormClosed += Fm2_FormClosed;
        }

        private void Fm2_FormClosed(object sender, FormClosedEventArgs e)
        {
            frm.Show();
        }


        private void BuPrint_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < arr.Count; i++)
            {
                result += $"{frm1.arrSp[arr[i]]} - {frm1.arrRu[arr[i]]}\n";
            }
            PrintDocument printDocument = new PrintDocument();
            printDocument.PrintPage += PrintPageHandler;
            PrintDialog printDialog = new PrintDialog();
            printDialog.Document = printDocument;
            if (printDialog.ShowDialog() == DialogResult.OK)
                printDialog.Document.Print();
        }

        private void PrintPageHandler(object sender, PrintPageEventArgs e)
        {
            e.Graphics.DrawString(result, new Font("Arial", 14), Brushes.Black, 0, 0);
            result = "";
        }

        private void BuAnswerFour_Click(object sender, EventArgs e)
        {
            if (ind == four)
                countTrue++;
            else
            {
                countFalse++;
                arr.Add(ind);
            }
            laStatics.Text = $"Верных ответов: {countTrue}, неверных ответов: {countFalse}.";
            buAnswerFour.Text = "";
            NewWord();
        }

        private void BuAnswerThree_Click(object sender, EventArgs e)
        {
            if (ind == three)
                countTrue++;
            else
            {
                countFalse++;
                arr.Add(ind);
            }
            laStatics.Text = $"Верных ответов: {countTrue}, неверных ответов: {countFalse}.";
            buAnswerThree.Text = "";
            NewWord();
        }

        private void BuAnswerTwo_Click(object sender, EventArgs e)
        {
            if (ind == two)
                countTrue++;
            else
            {
                countFalse++;
                arr.Add(ind);
            }
            laStatics.Text = $"Верных ответов: {countTrue}, неверных ответов: {countFalse}.";
            buAnswerTwo.Text = "";
            NewWord();
        }

        private void EdUnswer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if ((edUnswer.Text == frm1.arrRu[ind]) || (edUnswer.Text == frm1.arrSp[ind]))
                    countTrue++;
                else
                {
                    countFalse++;
                    arr.Add(ind);
                }
                laStatics.Text = $"Верных ответов: {countTrue}, неверных ответов: {countFalse}.";
                NewWord();
                edUnswer.Text = "";
            }
        }

        private void BuStop_Click(object sender, EventArgs e)
        {
            string message = "Распечатать слова, в которых была допущена ошибка?";
            string caption = "Тест завершен";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult res;

            // Displays the MessageBox.
            res = MessageBox.Show(message, caption, buttons);
            if (res == System.Windows.Forms.DialogResult.Yes)
            {
                // Closes the parent form.
                BuPrint_Click(sender, e);
            }
        }

        private void ChangeVis()
        {
            buAnswerOne.Visible = false;
            buAnswerTwo.Visible = false;
            buAnswerThree.Visible = false;
            buAnswerFour.Visible = false;
            buTrue.Visible = false;
            buFalse.Visible = false;
            edUnswer.Visible = false;
            laAnswer.Visible = false;
        }

        private void BuAnswerOne_Click(object sender, EventArgs e)
        {
            if (ind == one)
                countTrue++;
            else
            {
                countFalse++;
                arr.Add(ind);
            }
            laStatics.Text = $"Верных ответов: {countTrue}, неверных ответов: {countFalse}.";
            buAnswerOne.Text = "";
            NewWord();
        }

        private void BuFalse_Click(object sender, EventArgs e)
        {
            if (ind != an)
                countTrue++;
            else
            {
                countFalse++;
                arr.Add(ind);
            }
            laStatics.Text = $"Верных ответов: {countTrue}, неверных ответов: {countFalse}.";
            NewWord();
        }

        private void BuTrue_Click(object sender, EventArgs e)
        {
            if (ind == an)
                countTrue++;
            else
            {
                countFalse++;
                arr.Add(ind);
            }
            laStatics.Text = $"Верных ответов: {countTrue}, неверных ответов: {countFalse}.";
            NewWord();
        }

        private void BuStart_Click(object sender, EventArgs e)
        {
            NewWord();
        }

        private void NewWord()
        {
            Random rnd = new Random();
                ind = rnd.Next(0, frm1.arrRu.Count);
                ChooseMany(rnd);
                an = rnd.Next(0, frm1.arrRu.Count);
            if (cbRu.Checked == true)
            {
                if (cbSp.Checked == true)
                {
                    if (rnd.Next(0, 2) == 0)
                    {
                        laWord.Text = frm1.arrRu[ind];
                        laAnswer.Text = frm1.arrSp[an];
                        buAnswerOne.Text = frm1.arrSp[one];
                        buAnswerTwo.Text = frm1.arrSp[two];
                        buAnswerThree.Text = frm1.arrSp[three];
                        buAnswerFour.Text = frm1.arrSp[four];
                    }
                    else
                    {
                        laWord.Text = frm1.arrSp[ind];
                        laAnswer.Text = frm1.arrRu[an];
                        buAnswerOne.Text = frm1.arrRu[one];
                        buAnswerTwo.Text = frm1.arrRu[two];
                        buAnswerThree.Text = frm1.arrRu[three];
                        buAnswerFour.Text = frm1.arrRu[four];
                    }
                }
                else
                {
                    laWord.Text = frm1.arrRu[ind];
                    laAnswer.Text = frm1.arrSp[an];
                    buAnswerOne.Text = frm1.arrSp[one];
                    buAnswerTwo.Text = frm1.arrSp[two];
                    buAnswerThree.Text = frm1.arrSp[three];
                    buAnswerFour.Text = frm1.arrSp[four];
                }

            }
            else
            {
                laWord.Text = frm1.arrSp[ind];
                laAnswer.Text = frm1.arrRu[an];
                buAnswerOne.Text = frm1.arrRu[one];
                buAnswerTwo.Text = frm1.arrRu[two];
                buAnswerThree.Text = frm1.arrRu[three];
                buAnswerFour.Text = frm1.arrRu[four];
            }
                FindUnswer(rnd);
        }

        private void ChooseMany(Random rnd)
        {
            switch (rnd.Next(0,4))
            {
                case 0:
                    one = ind;
                    do
                    {
                        two = rnd.Next(0, frm1.arrRu.Count);
                        three = rnd.Next(0, frm1.arrRu.Count);
                        four = rnd.Next(0, frm1.arrRu.Count);
                    } while ((one == two) || (one == three) || (one == four) || (two == three) || (two == four) || (three == four));
                    break;
                case 1:
                    two = ind;
                    do
                    {
                        one = rnd.Next(0, frm1.arrRu.Count);
                        three = rnd.Next(0, frm1.arrRu.Count);
                        four = rnd.Next(0, frm1.arrRu.Count);
                    } while ((one == two) || (one == three) || (one == four) || (two == three) || (two == four) || (three == four));
                    break;
                case 2:
                    three = ind;
                    do
                    {
                        two = rnd.Next(0, frm1.arrRu.Count);
                        one = rnd.Next(0, frm1.arrRu.Count);
                        four = rnd.Next(0, frm1.arrRu.Count);
                    } while ((one == two) || (one == three) || (one == four) || (two == three) || (two == four) || (three == four));
                    break;
                case 3:
                    four = ind;
                    do
                    {
                        two = rnd.Next(0, frm1.arrRu.Count);
                        three = rnd.Next(0, frm1.arrRu.Count);
                        one = rnd.Next(0, frm1.arrRu.Count);
                    } while ((one == two) || (one == three) || (one == four) || (two == three) || (two == four) || (three == four));
                    break;
                default:
                    break;
            }
        }

        private void FindUnswer(Random rnd)
        {
            ChangeVis();
            RandomTask(rnd);
        }

        private void RandomTask(Random rnd)
        {
            switch (rnd.Next(0, 3))
            {
                case 0:
                    if (cbTrueOrNot.Checked == false)
                    {
                        RandomTask(rnd);
                        break;
                    }
                    laAnswer.Visible = true;
                    buTrue.Visible = true;
                    buFalse.Visible = true;
                    break;
                case 1:
                    if (cbChoose.Checked == false)
                    {
                        RandomTask(rnd);
                        break;
                    }
                    buAnswerOne.Visible = true;
                    buAnswerTwo.Visible = true;
                    buAnswerThree.Visible = true;
                    buAnswerFour.Visible = true;
                    break;
                case 2:
                    if ((cbEdRu.Checked == false) && (cbEdSp.Checked == false))
                    {
                        RandomTask(rnd);
                        break;
                    }
                    edUnswer.Visible = true;
                    break;
                default:
                    break;
            }
        }

        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbTrueOrNot = new System.Windows.Forms.CheckBox();
            this.buStart = new System.Windows.Forms.Button();
            this.cbChoose = new System.Windows.Forms.CheckBox();
            this.cbEdSp = new System.Windows.Forms.CheckBox();
            this.cbEdRu = new System.Windows.Forms.CheckBox();
            this.cbRu = new System.Windows.Forms.CheckBox();
            this.cbSp = new System.Windows.Forms.CheckBox();
            this.laWord = new System.Windows.Forms.Label();
            this.buStop = new System.Windows.Forms.Button();
            this.buAnswerOne = new System.Windows.Forms.Button();
            this.buAnswerTwo = new System.Windows.Forms.Button();
            this.buAnswerThree = new System.Windows.Forms.Button();
            this.buAnswerFour = new System.Windows.Forms.Button();
            this.edUnswer = new System.Windows.Forms.TextBox();
            this.buTrue = new System.Windows.Forms.Button();
            this.buFalse = new System.Windows.Forms.Button();
            this.laStatics = new System.Windows.Forms.Label();
            this.laAnswer = new System.Windows.Forms.Label();
            this.buPrint = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(205, 88);
            this.label1.TabIndex = 0;
            this.label1.Text = "Выберите параметры теста";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.cbTrueOrNot);
            this.panel1.Controls.Add(this.buStart);
            this.panel1.Controls.Add(this.cbChoose);
            this.panel1.Controls.Add(this.cbEdSp);
            this.panel1.Controls.Add(this.cbEdRu);
            this.panel1.Controls.Add(this.cbRu);
            this.panel1.Controls.Add(this.cbSp);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(684, 99);
            this.panel1.TabIndex = 1;
            // 
            // cbTrueOrNot
            // 
            this.cbTrueOrNot.AutoSize = true;
            this.cbTrueOrNot.Location = new System.Drawing.Point(414, 64);
            this.cbTrueOrNot.Name = "cbTrueOrNot";
            this.cbTrueOrNot.Size = new System.Drawing.Size(111, 19);
            this.cbTrueOrNot.TabIndex = 1;
            this.cbTrueOrNot.Text = "Верно/неверно";
            this.cbTrueOrNot.UseVisualStyleBackColor = true;
            // 
            // buStart
            // 
            this.buStart.BackColor = System.Drawing.Color.PowderBlue;
            this.buStart.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buStart.Location = new System.Drawing.Point(541, 14);
            this.buStart.Name = "buStart";
            this.buStart.Size = new System.Drawing.Size(137, 69);
            this.buStart.TabIndex = 2;
            this.buStart.Text = "Начать";
            this.buStart.UseVisualStyleBackColor = false;
            // 
            // cbChoose
            // 
            this.cbChoose.AutoSize = true;
            this.cbChoose.Location = new System.Drawing.Point(223, 11);
            this.cbChoose.Name = "cbChoose";
            this.cbChoose.Size = new System.Drawing.Size(136, 19);
            this.cbChoose.TabIndex = 1;
            this.cbChoose.Text = "Вывод из вариантов";
            this.cbChoose.UseVisualStyleBackColor = true;
            // 
            // cbEdSp
            // 
            this.cbEdSp.AutoSize = true;
            this.cbEdSp.Location = new System.Drawing.Point(222, 61);
            this.cbEdSp.Name = "cbEdSp";
            this.cbEdSp.Size = new System.Drawing.Size(186, 19);
            this.cbEdSp.TabIndex = 1;
            this.cbEdSp.Text = "Ввод перевода на испанском";
            this.cbEdSp.UseVisualStyleBackColor = true;
            // 
            // cbEdRu
            // 
            this.cbEdRu.AutoSize = true;
            this.cbEdRu.Location = new System.Drawing.Point(223, 36);
            this.cbEdRu.Name = "cbEdRu";
            this.cbEdRu.Size = new System.Drawing.Size(172, 19);
            this.cbEdRu.TabIndex = 1;
            this.cbEdRu.Text = "Ввод перевода на русском";
            this.cbEdRu.UseVisualStyleBackColor = true;
            // 
            // cbRu
            // 
            this.cbRu.AutoSize = true;
            this.cbRu.Location = new System.Drawing.Point(414, 36);
            this.cbRu.Name = "cbRu";
            this.cbRu.Size = new System.Drawing.Size(105, 19);
            this.cbRu.TabIndex = 1;
            this.cbRu.Text = "Русские слова";
            this.cbRu.UseVisualStyleBackColor = true;
            // 
            // cbSp
            // 
            this.cbSp.AutoSize = true;
            this.cbSp.Location = new System.Drawing.Point(414, 11);
            this.cbSp.Name = "cbSp";
            this.cbSp.Size = new System.Drawing.Size(121, 19);
            this.cbSp.TabIndex = 1;
            this.cbSp.Text = "Испанские слова";
            this.cbSp.UseVisualStyleBackColor = true;
            // 
            // laWord
            // 
            this.laWord.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.laWord.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laWord.Location = new System.Drawing.Point(84, 242);
            this.laWord.Name = "laWord";
            this.laWord.Size = new System.Drawing.Size(134, 43);
            this.laWord.TabIndex = 2;
            this.laWord.Text = "Word";
            // 
            // buStop
            // 
            this.buStop.BackColor = System.Drawing.Color.PowderBlue;
            this.buStop.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buStop.Location = new System.Drawing.Point(436, 492);
            this.buStop.Name = "buStop";
            this.buStop.Size = new System.Drawing.Size(146, 41);
            this.buStop.TabIndex = 3;
            this.buStop.Text = "Закончить";
            this.buStop.UseVisualStyleBackColor = false;
            // 
            // buAnswerOne
            // 
            this.buAnswerOne.BackColor = System.Drawing.Color.PowderBlue;
            this.buAnswerOne.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buAnswerOne.Location = new System.Drawing.Point(436, 163);
            this.buAnswerOne.Name = "buAnswerOne";
            this.buAnswerOne.Size = new System.Drawing.Size(177, 45);
            this.buAnswerOne.TabIndex = 4;
            this.buAnswerOne.Text = "Answer";
            this.buAnswerOne.UseVisualStyleBackColor = false;
            // 
            // buAnswerTwo
            // 
            this.buAnswerTwo.BackColor = System.Drawing.Color.PowderBlue;
            this.buAnswerTwo.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buAnswerTwo.Location = new System.Drawing.Point(436, 217);
            this.buAnswerTwo.Name = "buAnswerTwo";
            this.buAnswerTwo.Size = new System.Drawing.Size(177, 45);
            this.buAnswerTwo.TabIndex = 4;
            this.buAnswerTwo.Text = "Answer";
            this.buAnswerTwo.UseVisualStyleBackColor = false;
            // 
            // buAnswerThree
            // 
            this.buAnswerThree.BackColor = System.Drawing.Color.PowderBlue;
            this.buAnswerThree.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buAnswerThree.Location = new System.Drawing.Point(436, 268);
            this.buAnswerThree.Name = "buAnswerThree";
            this.buAnswerThree.Size = new System.Drawing.Size(177, 45);
            this.buAnswerThree.TabIndex = 4;
            this.buAnswerThree.Text = "Answer";
            this.buAnswerThree.UseVisualStyleBackColor = false;
            // 
            // buAnswerFour
            // 
            this.buAnswerFour.BackColor = System.Drawing.Color.PowderBlue;
            this.buAnswerFour.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buAnswerFour.Location = new System.Drawing.Point(436, 319);
            this.buAnswerFour.Name = "buAnswerFour";
            this.buAnswerFour.Size = new System.Drawing.Size(177, 45);
            this.buAnswerFour.TabIndex = 4;
            this.buAnswerFour.Text = "Answer";
            this.buAnswerFour.UseVisualStyleBackColor = false;
            // 
            // edUnswer
            // 
            this.edUnswer.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.edUnswer.Location = new System.Drawing.Point(415, 242);
            this.edUnswer.Name = "edUnswer";
            this.edUnswer.Size = new System.Drawing.Size(213, 39);
            this.edUnswer.TabIndex = 5;
            // 
            // buTrue
            // 
            this.buTrue.BackColor = System.Drawing.Color.PowderBlue;
            this.buTrue.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buTrue.Location = new System.Drawing.Point(451, 217);
            this.buTrue.Name = "buTrue";
            this.buTrue.Size = new System.Drawing.Size(146, 41);
            this.buTrue.TabIndex = 3;
            this.buTrue.Text = "Верно";
            this.buTrue.UseVisualStyleBackColor = false;
            // 
            // buFalse
            // 
            this.buFalse.BackColor = System.Drawing.Color.PowderBlue;
            this.buFalse.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buFalse.Location = new System.Drawing.Point(451, 272);
            this.buFalse.Name = "buFalse";
            this.buFalse.Size = new System.Drawing.Size(146, 41);
            this.buFalse.TabIndex = 3;
            this.buFalse.Text = "Неверно";
            this.buFalse.UseVisualStyleBackColor = false;
            // 
            // laStatics
            // 
            this.laStatics.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.laStatics.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laStatics.Location = new System.Drawing.Point(94, 423);
            this.laStatics.Name = "laStatics";
            this.laStatics.Size = new System.Drawing.Size(503, 43);
            this.laStatics.TabIndex = 2;
            this.laStatics.Text = "Верных ответов: 0, неверных ответов: 0.";
            // 
            // laAnswer
            // 
            this.laAnswer.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.laAnswer.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laAnswer.Location = new System.Drawing.Point(275, 242);
            this.laAnswer.Name = "laAnswer";
            this.laAnswer.Size = new System.Drawing.Size(134, 43);
            this.laAnswer.TabIndex = 2;
            this.laAnswer.Text = "Answer";
            // 
            // buPrint
            // 
            this.buPrint.BackColor = System.Drawing.Color.PowderBlue;
            this.buPrint.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buPrint.Location = new System.Drawing.Point(84, 492);
            this.buPrint.Name = "buPrint";
            this.buPrint.Size = new System.Drawing.Size(258, 41);
            this.buPrint.TabIndex = 3;
            this.buPrint.Text = "Распечатать ошибки";
            this.buPrint.UseVisualStyleBackColor = false;
            // 
            // Fm2
            // 
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(684, 561);
            this.Controls.Add(this.buPrint);
            this.Controls.Add(this.laAnswer);
            this.Controls.Add(this.laStatics);
            this.Controls.Add(this.buFalse);
            this.Controls.Add(this.buTrue);
            this.Controls.Add(this.edUnswer);
            this.Controls.Add(this.buAnswerFour);
            this.Controls.Add(this.buAnswerThree);
            this.Controls.Add(this.buAnswerTwo);
            this.Controls.Add(this.buAnswerOne);
            this.Controls.Add(this.buStop);
            this.Controls.Add(this.laWord);
            this.Controls.Add(this.panel1);
            this.MaximumSize = new System.Drawing.Size(700, 600);
            this.MinimumSize = new System.Drawing.Size(700, 600);
            this.Name = "Fm2";
            this.Text = "Тест";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}