﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpanishTrainer
{
    public partial class Fm : Form
    {
         //где Form2 - название вашей формы
        private string result = "";
        private Fm1 frm1;

        public Fm()
        {
            InitializeComponent();
            
            buTranslator.Click += (s, e) => Translator();
            buTest.Click += BuTest_Click;
            buVerbs.Click += BuVerbs_Click;
            buAbout.Click += BuAbout_Click;
            buPrint.Click += BuPrint_Click;
        }

        private void BuPrint_Click(object sender, EventArgs e)
        {
            frm1 = new Fm1();
            for (int i = 0; i < frm1.arrSp.Count; i++)
            {
                result += $"{frm1.arrSp[i]} - {frm1.arrRu[i]}\n";
            }
            PrintDocument printDocument = new PrintDocument();
            printDocument.PrintPage += PrintPageHandler;
            PrintDialog printDialog = new PrintDialog();
            printDialog.Document = printDocument;
            if (printDialog.ShowDialog() == DialogResult.OK)
                printDialog.Document.Print();
                
        }

        private void PrintPageHandler(object sender, PrintPageEventArgs e)
        {
            e.Graphics.DrawString(result, new Font("Arial", 14), Brushes.Black, 0, 0);
            result = "";
        }

        private void BuAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show("   Чтобы выбрать необходимую функцию программы, нажмите на соответствующую кнопку.\n\n" +
                "   Перевод - позволяет найти перевод необходимого слова с испанского на русский язык, посмотреть примеры использования, а также добавить новое слово.\n" +
                "   Тест - позволяет проверить изученные слова в игровой форме.\n" +
                "   Спряжения - позволяет проверить спряжения различных глаголов испанского языка в наиболее часто используемых временах.\n" +
                "   Печать - позволяет распечатать документ со словами.\n\n" +
                "   Программа создана в рамках дисциплины 'Мобильная разработка' Крюковой М.Д., студентки группы 181-324.");
        }

        private void BuVerbs_Click(object sender, EventArgs e)
        {
            Fm3 frm3 = new Fm3(); //где Form2 - название вашей формы
            frm3.Show();
            this.Hide();
        }

        private void BuTest_Click(object sender, EventArgs e)
        {
            Fm2 frm2 = new Fm2(); //где Form2 - название вашей формы
            frm2.Show();
            this.Hide();
        }

        private void Translator()
        {
            frm1 = new Fm1();
            frm1.Show();
            this.Hide();
        }
    }
}
