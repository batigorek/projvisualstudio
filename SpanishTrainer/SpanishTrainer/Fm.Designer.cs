﻿namespace SpanishTrainer
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buTranslator = new System.Windows.Forms.Button();
            this.buTest = new System.Windows.Forms.Button();
            this.buVerbs = new System.Windows.Forms.Button();
            this.buPrint = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buAbout = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SuspendLayout();
            // 
            // buTranslator
            // 
            this.buTranslator.BackColor = System.Drawing.Color.PowderBlue;
            this.buTranslator.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buTranslator.Location = new System.Drawing.Point(223, 120);
            this.buTranslator.Name = "buTranslator";
            this.buTranslator.Size = new System.Drawing.Size(239, 81);
            this.buTranslator.TabIndex = 0;
            this.buTranslator.Text = "Переводчик";
            this.buTranslator.UseVisualStyleBackColor = false;
            // 
            // buTest
            // 
            this.buTest.BackColor = System.Drawing.Color.PowderBlue;
            this.buTest.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buTest.Location = new System.Drawing.Point(223, 207);
            this.buTest.Name = "buTest";
            this.buTest.Size = new System.Drawing.Size(239, 81);
            this.buTest.TabIndex = 0;
            this.buTest.Text = "Тест";
            this.buTest.UseVisualStyleBackColor = false;
            // 
            // buVerbs
            // 
            this.buVerbs.BackColor = System.Drawing.Color.PowderBlue;
            this.buVerbs.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buVerbs.Location = new System.Drawing.Point(223, 294);
            this.buVerbs.Name = "buVerbs";
            this.buVerbs.Size = new System.Drawing.Size(239, 81);
            this.buVerbs.TabIndex = 0;
            this.buVerbs.Text = "Спряжения";
            this.buVerbs.UseVisualStyleBackColor = false;
            // 
            // buPrint
            // 
            this.buPrint.BackColor = System.Drawing.Color.PowderBlue;
            this.buPrint.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buPrint.Location = new System.Drawing.Point(223, 381);
            this.buPrint.Name = "buPrint";
            this.buPrint.Size = new System.Drawing.Size(239, 81);
            this.buPrint.TabIndex = 0;
            this.buPrint.Text = "Печать";
            this.buPrint.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 49F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(684, 88);
            this.label1.TabIndex = 1;
            this.label1.Text = "Trotadora de epañol";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buAbout
            // 
            this.buAbout.BackColor = System.Drawing.Color.PowderBlue;
            this.buAbout.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buAbout.Location = new System.Drawing.Point(223, 468);
            this.buAbout.Name = "buAbout";
            this.buAbout.Size = new System.Drawing.Size(239, 81);
            this.buAbout.TabIndex = 0;
            this.buAbout.Text = "О программе";
            this.buAbout.UseVisualStyleBackColor = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(684, 561);
            this.Controls.Add(this.buAbout);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buPrint);
            this.Controls.Add(this.buVerbs);
            this.Controls.Add(this.buTest);
            this.Controls.Add(this.buTranslator);
            this.MaximumSize = new System.Drawing.Size(700, 600);
            this.MinimumSize = new System.Drawing.Size(700, 600);
            this.Name = "Fm";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buTranslator;
        private System.Windows.Forms.Button buTest;
        private System.Windows.Forms.Button buVerbs;
        private System.Windows.Forms.Button buPrint;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buAbout;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
    }
}

