﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpanishTrainer
{
    internal partial class Fm3 : Form
    {
        private TextBox edAsk;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label laPresento;
        private Label laPreterito;
        private Button buFind;
        private Label laImperfecto;
        private Label laFuturo;
        private Label laTextP;
        private Label label8;
        private Label laTextF;
        private Panel panel1;
        private char[] word;
        Fm frm = new Fm();

        public Fm3()
        {
            InitializeComponent();


            buFind.Click += BuFind_Click;
            //this.Close();
            this.FormClosed += Fm3_FormClosed;
        }

        private void Fm3_FormClosed(object sender, FormClosedEventArgs e)
        {
            frm.Show();
        }

        private void BuFind_Click(object sender, EventArgs e)
        {
            word = edAsk.Text.ToCharArray();

                switch (word[word.Length-2])
                {
                    case 'a':
                    Array.Resize(ref word, word.Length - 2);
                    laPresento.Text = $"{string.Join("", word)}o\n" +
                        $"{string.Join("", word)}as\n" +
                        $"{string.Join("", word)}a\n" +
                        $"{string.Join("", word)}amos\n" +
                        $"{string.Join("", word)}áis\n" +
                        $"{string.Join("", word)}an\n";
                    laPreterito.Text = $"{string.Join("", word)}é\n" +
                        $"{string.Join("", word)}aste\n" +
                        $"{string.Join("", word)}aó\n" +
                        $"{string.Join("", word)}amos\n" +
                        $"{string.Join("", word)}asteis\n" +
                        $"{string.Join("", word)}aron\n";
                    laFuturo.Text = $"{string.Join("", word)}aré\n" +
                        $"{string.Join("", word)}arás\n" +
                        $"{string.Join("", word)}ará\n" +
                        $"{string.Join("", word)}aremos\n" +
                        $"{string.Join("", word)}aréis\n" +
                        $"{string.Join("", word)}arán\n";
                    laImperfecto.Text = $"{string.Join("", word)}aba\n" +
                        $"{string.Join("", word)}abas\n" +
                        $"{string.Join("", word)}aba\n" +
                        $"{string.Join("", word)}ábamos\n" +
                        $"{string.Join("", word)}abais\n" +
                        $"{string.Join("", word)}aban\n";
                    break;
                    case 'i':
                    Array.Resize(ref word, word.Length - 2);
                    laPresento.Text = $"{string.Join("", word)}o\n" +
                        $"{string.Join("", word)}es\n" +
                        $"{string.Join("", word)}e\n" +
                        $"{string.Join("", word)}emos\n" +
                        $"{string.Join("", word)}éis\n" +
                        $"{string.Join("", word)}en\n";
                    laPreterito.Text = $"{string.Join("", word)}í\n" +
                        $"{string.Join("", word)}iste\n" +
                        $"{string.Join("", word)}ió\n" +
                        $"{string.Join("", word)}imos\n" +
                        $"{string.Join("", word)}isteis\n" +
                        $"{string.Join("", word)}ieron\n";
                    laFuturo.Text = $"{string.Join("", word)}iré\n" +
                        $"{string.Join("", word)}irás\n" +
                        $"{string.Join("", word)}irá\n" +
                        $"{string.Join("", word)}iremos\n" +
                        $"{string.Join("", word)}iréis\n" +
                        $"{string.Join("", word)}irán\n";
                    laImperfecto.Text = $"{string.Join("", word)}ía\n" +
                        $"{string.Join("", word)}ías\n" +
                        $"{string.Join("", word)}ía\n" +
                        $"{string.Join("", word)}íamos\n" +
                        $"{string.Join("", word)}íais\n" +
                        $"{string.Join("", word)}ían\n";
                    break;
                    case 'e':
                    Array.Resize(ref word, word.Length - 2);
                    laPresento.Text = $"{string.Join("", word)}o\n" +
                        $"{string.Join("", word)}es\n" +
                        $"{string.Join("", word)}e\n" +
                        $"{string.Join("", word)}imos\n" +
                        $"{string.Join("", word)}ís\n" +
                        $"{string.Join("", word)}en\n";
                    laPreterito.Text = $"{string.Join("", word)}í\n" +
                        $"{string.Join("", word)}iste\n" +
                        $"{string.Join("", word)}ió\n" +
                        $"{string.Join("", word)}imos\n" +
                        $"{string.Join("", word)}isteis\n" +
                        $"{string.Join("", word)}ieron\n";
                    laFuturo.Text = $"{string.Join("", word)}eré\n" +
                        $"{string.Join("", word)}erás\n" +
                        $"{string.Join("", word)}erá\n" +
                        $"{string.Join("", word)}eremos\n" +
                        $"{string.Join("", word)}eréis\n" +
                        $"{string.Join("", word)}erán\n";
                    laImperfecto.Text = $"{string.Join("", word)}ía\n" +
                        $"{string.Join("", word)}ías\n" +
                        $"{string.Join("", word)}ía\n" +
                        $"{string.Join("", word)}íamos\n" +
                        $"{string.Join("", word)}íais\n" +
                        $"{string.Join("", word)}ían\n";
                    break;
                default:
                        break;
                }
        }

        private void InitializeComponent()
        {
            this.edAsk = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.laPresento = new System.Windows.Forms.Label();
            this.laPreterito = new System.Windows.Forms.Label();
            this.buFind = new System.Windows.Forms.Button();
            this.laImperfecto = new System.Windows.Forms.Label();
            this.laFuturo = new System.Windows.Forms.Label();
            this.laTextP = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.laTextF = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // edAsk
            // 
            this.edAsk.Location = new System.Drawing.Point(287, 7);
            this.edAsk.Name = "edAsk";
            this.edAsk.Size = new System.Drawing.Size(324, 23);
            this.edAsk.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(62, -2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 32);
            this.label1.TabIndex = 1;
            this.label1.Text = "Введите слово:";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(158, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(251, 32);
            this.label2.TabIndex = 2;
            this.label2.Text = "Presento de indicativo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(0, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 192);
            this.label3.TabIndex = 3;
            this.label3.Text = "yo\r\ntú\r\nél, ella, usted\r\nnosotros\r\nvosotros\r\nustedes, ellos\r\n";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(424, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(221, 32);
            this.label4.TabIndex = 2;
            this.label4.Text = "Pretérito indefinido";
            // 
            // laPresento
            // 
            this.laPresento.AutoSize = true;
            this.laPresento.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laPresento.Location = new System.Drawing.Point(207, 125);
            this.laPresento.Name = "laPresento";
            this.laPresento.Size = new System.Drawing.Size(14, 32);
            this.laPresento.TabIndex = 3;
            this.laPresento.Text = "\r\n";
            // 
            // laPreterito
            // 
            this.laPreterito.AutoSize = true;
            this.laPreterito.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laPreterito.Location = new System.Drawing.Point(457, 125);
            this.laPreterito.Name = "laPreterito";
            this.laPreterito.Size = new System.Drawing.Size(14, 64);
            this.laPreterito.TabIndex = 3;
            this.laPreterito.Text = "\r\n\r\n";
            // 
            // buFind
            // 
            this.buFind.BackColor = System.Drawing.Color.PowderBlue;
            this.buFind.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buFind.Location = new System.Drawing.Point(391, 36);
            this.buFind.Name = "buFind";
            this.buFind.Size = new System.Drawing.Size(120, 43);
            this.buFind.TabIndex = 4;
            this.buFind.Text = "Найти";
            this.buFind.UseVisualStyleBackColor = false;
            // 
            // laImperfecto
            // 
            this.laImperfecto.AutoSize = true;
            this.laImperfecto.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laImperfecto.Location = new System.Drawing.Point(457, 372);
            this.laImperfecto.Name = "laImperfecto";
            this.laImperfecto.Size = new System.Drawing.Size(14, 32);
            this.laImperfecto.TabIndex = 3;
            this.laImperfecto.Text = "\r\n";
            // 
            // laFuturo
            // 
            this.laFuturo.AutoSize = true;
            this.laFuturo.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laFuturo.Location = new System.Drawing.Point(207, 372);
            this.laFuturo.Name = "laFuturo";
            this.laFuturo.Size = new System.Drawing.Size(14, 32);
            this.laFuturo.TabIndex = 3;
            this.laFuturo.Text = "\r\n";
            // 
            // laTextP
            // 
            this.laTextP.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.laTextP.AutoSize = true;
            this.laTextP.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laTextP.Location = new System.Drawing.Point(424, 329);
            this.laTextP.Name = "laTextP";
            this.laTextP.Size = new System.Drawing.Size(228, 32);
            this.laTextP.TabIndex = 2;
            this.laTextP.Text = "Pretérito imperfecto";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label8.Location = new System.Drawing.Point(0, 372);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(157, 192);
            this.label8.TabIndex = 3;
            this.label8.Text = "yo\r\ntú\r\nél, ella, usted\r\nnosotros\r\nvosotros\r\nustedes, ellos\r\n";
            // 
            // laTextF
            // 
            this.laTextF.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.laTextF.AutoSize = true;
            this.laTextF.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laTextF.Location = new System.Drawing.Point(158, 329);
            this.laTextF.Name = "laTextF";
            this.laTextF.Size = new System.Drawing.Size(201, 32);
            this.laTextF.TabIndex = 2;
            this.laTextF.Text = "Futuro indecativo";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(-11, 320);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(701, 244);
            this.panel1.TabIndex = 5;
            // 
            // Fm3
            // 
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(684, 561);
            this.Controls.Add(this.laTextF);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.laTextP);
            this.Controls.Add(this.laFuturo);
            this.Controls.Add(this.laImperfecto);
            this.Controls.Add(this.buFind);
            this.Controls.Add(this.laPreterito);
            this.Controls.Add(this.laPresento);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.edAsk);
            this.Controls.Add(this.panel1);
            this.MaximumSize = new System.Drawing.Size(700, 600);
            this.MinimumSize = new System.Drawing.Size(700, 600);
            this.Name = "Fm3";
            this.Text = "Спряжения";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

    }
}