﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpanishTrainer
{
    internal partial class Fm1 : Form
    {
        private TextBox edSearch;
        private Label laMistake;
        private TextBox edTranslate;
        private Label label2;
        private TextBox textBox1;
        private Button buAdd;
        private Label label3;
        private TextBox tbSp;
        private Label label4;
        private Label label1;
        private string docSp;
        private string docRu;
        private string docText;


        Fm frm = new Fm();
        public List<string> arrSp = new List<string>();
        public List<string> arrRu = new List<string>();
        public List<string> arrText = new List<string>();
        private TextBox tbExp;
        private TextBox tbRu;

        public Fm1()
        {
            InitializeComponent();

            UpdateFiles();

            edSearch.TextChanged += EdSearch_TextChanged;
            buAdd.Click += BuAdd_Click;
            this.FormClosed += Fm1_FormClosed;
            CurDir = Directory.GetCurrentDirectory();
        }

        private void Fm1_FormClosed(object sender, FormClosedEventArgs e)
        {
            frm.Show();
        }

        private void UpdateFiles()
        {
            docSp = @"words.txt";
            docRu = @"translate.txt";
            docText = @"text.txt";

            using (StreamReader sr = new StreamReader(docText, System.Text.Encoding.Default))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    arrText.Add(line);
                }
            }
            using (StreamReader sr = new StreamReader(docSp, System.Text.Encoding.Default))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    arrSp.Add(line);
                }
            }
            using (StreamReader sr = new StreamReader(docRu, System.Text.Encoding.Default))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    arrRu.Add(line);
                }
            }
            
        }

        private void BuAdd_Click(object sender, EventArgs e)
        {
            if ((tbRu.Text != null)&& (tbSp.Text != null))
            {
                File.AppendAllText(docSp, $"\n{tbSp.Text}");
                File.AppendAllText(docRu, $"\n{tbRu.Text}");
                UpdateFiles();
                tbRu.Text = "";
                tbSp.Text = "";
            }
        }

        private void EdSearch_TextChanged(object sender, EventArgs e)
        {
            if (edSearch.Text == "")
                laMistake.Text = "Вввдите слово";
            else
            {
                for (int i = 0; i < arrSp.Count; i++)
                {
                    if (arrSp[i].ToUpper() == edSearch.Text.ToUpper())
                    {
                        laMistake.Text = "Перевод";
                        edTranslate.Text = string.Join(Environment.NewLine, arrRu[i]);
                        break;
                    }
                    else
                    {
                        laMistake.Text = "Проверьте написание слова или добавьте слово в словарь";
                    }
                }
            var r = arrText.Where(v => v.ToUpper().Contains(edSearch.Text.ToUpper()))
                .Where(v => v.Length > 0)
                .OrderBy(v => v)
                .ToArray();
            tbExp.Text = string.Join(Environment.NewLine, r);
            }
        }

        private void InitializeComponent()
        {
            this.edSearch = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.laMistake = new System.Windows.Forms.Label();
            this.edTranslate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbExp = new System.Windows.Forms.TextBox();
            this.buAdd = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tbSp = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbRu = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // edSearch
            // 
            this.edSearch.Location = new System.Drawing.Point(348, 9);
            this.edSearch.Name = "edSearch";
            this.edSearch.Size = new System.Drawing.Size(324, 23);
            this.edSearch.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(339, 32);
            this.label1.TabIndex = 1;
            this.label1.Text = "Введите слово для перевода:";
            // 
            // laMistake
            // 
            this.laMistake.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.laMistake.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laMistake.Location = new System.Drawing.Point(0, 35);
            this.laMistake.Name = "laMistake";
            this.laMistake.Size = new System.Drawing.Size(686, 57);
            this.laMistake.TabIndex = 2;
            this.laMistake.Text = "Перевод:";
            // 
            // edTranslate
            // 
            this.edTranslate.Location = new System.Drawing.Point(0, 116);
            this.edTranslate.MaximumSize = new System.Drawing.Size(684, 60);
            this.edTranslate.MinimumSize = new System.Drawing.Size(684, 50);
            this.edTranslate.Multiline = true;
            this.edTranslate.Name = "edTranslate";
            this.edTranslate.ReadOnly = true;
            this.edTranslate.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.edTranslate.Size = new System.Drawing.Size(684, 50);
            this.edTranslate.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(0, 186);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(686, 43);
            this.label2.TabIndex = 2;
            this.label2.Text = "Примеры использования:";
            // 
            // tbExp
            // 
            this.tbExp.Location = new System.Drawing.Point(0, 232);
            this.tbExp.MaximumSize = new System.Drawing.Size(684, 160);
            this.tbExp.MinimumSize = new System.Drawing.Size(684, 160);
            this.tbExp.Multiline = true;
            this.tbExp.Name = "tbExp";
            this.tbExp.ReadOnly = true;
            this.tbExp.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbExp.Size = new System.Drawing.Size(684, 160);
            this.tbExp.TabIndex = 3;
            // 
            // buAdd
            // 
            this.buAdd.BackColor = System.Drawing.Color.PowderBlue;
            this.buAdd.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buAdd.Location = new System.Drawing.Point(443, 444);
            this.buAdd.Name = "buAdd";
            this.buAdd.Size = new System.Drawing.Size(229, 83);
            this.buAdd.TabIndex = 4;
            this.buAdd.Text = "Добавить";
            this.buAdd.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(0, 409);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(441, 32);
            this.label3.TabIndex = 5;
            this.label3.Text = "Введите слово на иностранном языке:";
            // 
            // tbSp
            // 
            this.tbSp.Location = new System.Drawing.Point(8, 444);
            this.tbSp.Name = "tbSp";
            this.tbSp.Size = new System.Drawing.Size(429, 23);
            this.tbSp.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(0, 481);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(210, 32);
            this.label4.TabIndex = 5;
            this.label4.Text = "Введите перевод:";
            // 
            // tbRu
            // 
            this.tbRu.Location = new System.Drawing.Point(8, 516);
            this.tbRu.Name = "tbRu";
            this.tbRu.Size = new System.Drawing.Size(429, 23);
            this.tbRu.TabIndex = 6;
            // 
            // Fm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(684, 561);
            this.Controls.Add(this.tbRu);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbSp);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buAdd);
            this.Controls.Add(this.tbExp);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.edTranslate);
            this.Controls.Add(this.laMistake);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.edSearch);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(700, 600);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(700, 600);
            this.Name = "Fm1";
            this.Text = "Переводчик";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}