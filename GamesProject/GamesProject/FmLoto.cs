﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GamesProject
{
    public partial class FmLoto : Form
    {
        const int countX = 6;
        const int countY = 4;
        private TextBox[,] arrTb = new TextBox[countX, countY];
        private int sizeButtonX = 60;
        private int sizeButtonY = 50;
        List<int> listNum = new List<int>();
        private int crossOut = 0;
        private DateTime startUp;
        private int gameCount = countX * countY;
        Timer tmUp = new Timer();
        
        private int x;

        public FmLoto(Fm fm)
        {
            InitializeComponent();

            fm.Hide();

            this.FormClosed += (s, e) => fm.Show();
            ShowField();
            buStart.Click += (s, e) => Start();
            buOther.Click += (s, e) => Play();
            tbReset.Click += (s, e) => { tmUp.Stop(); tmUp.Dispose(); ReStart();};
            buStop.Click += (s, e) => { buOther.Enabled = false; tmUp.Stop(); tmUp.Dispose(); ReStart(); };
            tbRules.Click += (s, e) => MessageBox.Show(
                "Чтобы начать игру, заполните все поля числами от 0 до 100 включительно и нажмите старт." +
                "Если выпало число, совпадающее с тем, что вы указали в поле, дважды кликните на необходимое поле." +
                "Чтобы посмотреть другое число, нажмите на кнопку 'Другое число'." +
                "Игра считается завершенной, если вам выпали все числа, которые вы указали в полях." +
                "Чтобы завершить игру досрочно, нажмите 'Завершить'." +
                "Чтобы начать игру заново, нажмите 'Начать заново', заполните поля и нажмите 'Начать'.");

        }

        private void ReStart()
        {
            for (int i = 0; i < countX; i++)
            {
                for (int j = 0; j < countY; j++)
                {
                    arrTb[i, j].Clear();
                }
            }
        }

        private void Start()
        {
            buOther.Enabled = true;
            for (int i = 0; i < countX; i++)
                for (int j = 0; j < countY; j++)
                    for (int k = 0; k < countX; k++)
                        for (int m = 0; m < countY; m++)
                            if (arrTb[i, j].Text == arrTb[k, m].Text && (i != k || j != m))
                            {
                                MessageBox.Show("Значения в поле не должны повторяться или оставаться пустыми.");
                                return;
                            }
            Play(); 
            tmUp.Enabled = !tmUp.Enabled;
            startUp = DateTime.Now;
            tmUp.Tick += TmUp_Tick;
        }

        private void TmUp_Tick(object sender, EventArgs e)
        {
            var x = DateTime.Now - startUp;
            laTime.Text = $"Время игры: {x.ToString(@"mm\:ss")}";
        }

        private void Play()
        {
            if (gameCount > 0)
            {
                Random rnd = new Random();
                x = rnd.Next(0, 101);
                while (listNum.Contains(x) == true)
                {
                    x = rnd.Next(0, 101);
                }
                listNum.Add(x);
                laNumber.Text = $"{x}";
                gameCount--;
            }
            else if (crossOut==24)
            {
                MessageBox.Show("Поздравляем! Вы победили!");
            }
            else if (gameCount == 0)
            {
                MessageBox.Show("К сожалению, вы проиграли");
                buOther.Enabled = false;
            }
        }

        private void ShowField()
        {
            for (int i = 0; i < countX; i++)
            {
                for (int j = 0; j < countY; j++)
                {
                    this.Controls.Remove(arrTb[i, j]);
                    arrTb[i, j] = new TextBox();
                    TextBox tb = new TextBox();
                    arrTb[i, j] = tb;
                    tb.Size = new Size(sizeButtonX, sizeButtonY);
                    tb.Font = new Font("", 20, FontStyle.Regular);
                    tb.Location = new Point(350 + i * sizeButtonX, 200 + j * sizeButtonY);
                    tb.DoubleClick += Tb_Click;
                    this.Controls.Add(tb);
                }
            }
        }

        private void Tb_Click(object sender, EventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (x.ToString() == tb.Text)
            {
                tb.Font = new Font("", 20, FontStyle.Strikeout);
                tb.Enabled = false;
                crossOut++;
            }
        }
    }
}
