﻿
namespace GamesProject
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.buCities = new System.Windows.Forms.Button();
            this.buSudoky = new System.Windows.Forms.Button();
            this.buBingo = new System.Windows.Forms.Button();
            this.buVisel = new System.Windows.Forms.Button();
            this.buAbout = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(407, 113);
            this.label1.TabIndex = 0;
            this.label1.Text = "Добро пожаловать на главный экран! Выберите игру для продолжения.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buCities
            // 
            this.buCities.BackColor = System.Drawing.Color.LightCyan;
            this.buCities.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point);
            this.buCities.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buCities.Location = new System.Drawing.Point(77, 129);
            this.buCities.Name = "buCities";
            this.buCities.Size = new System.Drawing.Size(262, 61);
            this.buCities.TabIndex = 1;
            this.buCities.Text = "Города";
            this.buCities.UseVisualStyleBackColor = false;
            // 
            // buSudoky
            // 
            this.buSudoky.BackColor = System.Drawing.Color.LightCyan;
            this.buSudoky.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point);
            this.buSudoky.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buSudoky.Location = new System.Drawing.Point(77, 196);
            this.buSudoky.Name = "buSudoky";
            this.buSudoky.Size = new System.Drawing.Size(262, 61);
            this.buSudoky.TabIndex = 2;
            this.buSudoky.Text = "Судоку";
            this.buSudoky.UseVisualStyleBackColor = false;
            // 
            // buBingo
            // 
            this.buBingo.BackColor = System.Drawing.Color.LightCyan;
            this.buBingo.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point);
            this.buBingo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buBingo.Location = new System.Drawing.Point(77, 263);
            this.buBingo.Name = "buBingo";
            this.buBingo.Size = new System.Drawing.Size(262, 61);
            this.buBingo.TabIndex = 3;
            this.buBingo.Text = "Лото";
            this.buBingo.UseVisualStyleBackColor = false;
            // 
            // buVisel
            // 
            this.buVisel.BackColor = System.Drawing.Color.LightCyan;
            this.buVisel.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point);
            this.buVisel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buVisel.Location = new System.Drawing.Point(77, 330);
            this.buVisel.Name = "buVisel";
            this.buVisel.Size = new System.Drawing.Size(262, 61);
            this.buVisel.TabIndex = 4;
            this.buVisel.Text = "Виселица";
            this.buVisel.UseVisualStyleBackColor = false;
            // 
            // buAbout
            // 
            this.buAbout.BackColor = System.Drawing.Color.LightCyan;
            this.buAbout.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point);
            this.buAbout.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buAbout.Location = new System.Drawing.Point(77, 394);
            this.buAbout.Name = "buAbout";
            this.buAbout.Size = new System.Drawing.Size(262, 61);
            this.buAbout.TabIndex = 5;
            this.buAbout.Text = "О программе";
            this.buAbout.UseVisualStyleBackColor = false;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(407, 467);
            this.Controls.Add(this.buAbout);
            this.Controls.Add(this.buVisel);
            this.Controls.Add(this.buBingo);
            this.Controls.Add(this.buSudoky);
            this.Controls.Add(this.buCities);
            this.Controls.Add(this.label1);
            this.Name = "Fm";
            this.Text = "Главное меню";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buCities;
        private System.Windows.Forms.Button buSudoky;
        private System.Windows.Forms.Button buBingo;
        private System.Windows.Forms.Button buVisel;
        private System.Windows.Forms.Button buAbout;
    }
}

