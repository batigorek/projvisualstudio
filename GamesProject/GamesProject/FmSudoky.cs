﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GamesProject
{
    public partial class FmSudoky : Form
    {

        const int sizeField = 3;
        private int[,] arrSud = new int [sizeField * sizeField, sizeField * sizeField];
        private Button[,] arrBu = new Button[sizeField * sizeField, sizeField * sizeField];
        private int sizeButton = 35;
        private int hard = 1; 
        private DateTime startUp;

        Timer tmUp = new Timer();

        public FmSudoky(Fm fm)
        {
            InitializeComponent();

            //tableLayoutPanel1.Visible = false;
            fm.Hide();

            this.FormClosed += (s, e) => fm.Show();

            tbRules.Click += (s, e) => MessageBox.Show("От игрока требуется заполнить свободные клетки цифрами от 1 до 9 так, " +
                "чтобы в каждой строке, в каждом столбце и в каждом малом квадрате 3×3 каждая цифра встречалась бы только один " +
                "раз. Сложность судоку зависит от количества изначально заполненных клеток и от методов, которые нужно применять для её решения." +
                "Чтобы посмотреть ход прошлой игры, нажмите на кнопку в меню 'Прошлая игра'." +
                "Чтобы начать игру заново, нажмите на кнопку в меню 'Начать заново'." +
                "Чтобы начать игру, нажмите на кнопку 'Начать'." +
                "Чтобы завершить игру, нажмите на кнопку 'Завершить'." +
                "Чтобы ввести цифру, нажмите на поле, в которое хотите ввести значение, пока не появится нужное для вас число.");
            buStart.Click += (s, e) => Start();
            buStop.Click += BuStop_Click;
            buPlus.Click += (s, e) => { if (hard < 6) hard++; laDifficult.Text = $"Сложность: {hard}"; };
            buMinus.Click += (s, e) => { if (hard > 1) hard--; laDifficult.Text = $"Сложность: {hard}"; };
            tbReset.Click += (s, e) => { tmUp.Stop(); tmUp.Dispose(); Start(); };

        }

        private void BuStop_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < sizeField*sizeField; i++)
            {
                for (int j = 0; j < sizeField*sizeField; j++)
                {
                    var btnText = arrBu[i, j].Text;
                    if (btnText != arrSud[i,j].ToString())
                    {
                        MessageBox.Show("Вы проиграли!");
                        return;
                    }
                }
            }
            MessageBox.Show("Поздравляем, вы выиграли!");
        }

        private void Start()
        {
            //tableLayoutPanel1.Visible = true;
            for (int i = 0; i < sizeField*sizeField; i++)
            {
                for (int j = 0; j < sizeField*sizeField; j++)
                {
                    this.Controls.Remove(arrBu[i, j]);
                    arrSud[i, j] = (i * sizeField + i / sizeField + j) % (sizeField * sizeField) + 1;
                    arrBu[i, j] = new Button();
                }
            }
            Copy();
            Shake();
            CreateField();
            HideBu();
            tmUp.Enabled = !tmUp.Enabled;
            startUp = DateTime.Now;
            tmUp.Tick += TmUp_Tick;
        }

        private void TmUp_Tick(object sender, EventArgs e)
        {
            var x = DateTime.Now - startUp;
            laTime.Text = $"Время игры: {x.ToString(@"mm\:ss")}";
        }

        private void FmSudoky_Click(object sender, EventArgs e)
        {
            Button btnProcess = sender as Button;
            string btnTextChange = btnProcess.Text;
            if (btnTextChange == "")
                btnProcess.Text = "1";
            else
            {
                int num = int.Parse(btnTextChange);
                num++;
                if (num==10)
                    num = 1;
                btnProcess.Text = num.ToString();
            }
        }

        private void HideBu()
        {
            Random rnd = new Random();
            int hideBu = hard * 30+81;
            while (hideBu > 0)
            {
            for (int i = 0; i < sizeField*sizeField; i++)
            {
                for (int j = 0; j < sizeField*sizeField; j++)
                {
                    int a = rnd.Next(0, 3);
                    arrBu[i, j].Text = a == 0 ? "" : arrBu[i, j].Text;
                        arrBu[i, j].Enabled = a == 0 ? true : false;
                        hideBu--;
                        if (hideBu == 0)
                        {
                            break;
                        }
                }
                    if (hideBu == 0)
                    {
                        break;
                    }
                }
            }
        }

        private void Shake()
        {
            Random rnd = new Random();
            var block = rnd.Next(0, sizeField);
            var row1 = rnd.Next(0, sizeField);
            var line1 = block * sizeField + row1;
            var row2 = rnd.Next(0, sizeField);
            while (row1 == row2)
                row2 = rnd.Next(0, sizeField);
            var line2 = block * sizeField + row2;
            for (int i = 0; i < sizeField*sizeField; i++)
            {
                var temp = arrSud[line1, i];
                arrSud[line1, i] = arrSud[line2, i];
                arrSud[line2, i] = temp;
                var temp1 = arrSud[i, line1];
                arrSud[i, line1] = arrSud[i, line2];
                arrSud[i, line2] = temp1;
            }

            var block1 = rnd.Next(0, sizeField);
            var block2 = rnd.Next(0, sizeField);
            while (block1 == block2)
                block2 = rnd.Next(0, sizeField);

            block1 *= sizeField;
            block2 *= sizeField;

            for (int i = 0; i < sizeField * sizeField; i++)
            {
                var k = block2;
                for (int j = block1; j < block1 + sizeField; j++)
                {
                    var temp2 = arrSud[j, i];
                    arrSud[j, i] = arrSud[k, i];
                    arrSud[k, i] = temp2;
                    k++;
                }
            }
        }

        private void Copy()
        {
            int[,] syd = new int[sizeField * sizeField, sizeField * sizeField];
            for (int i = 0; i < sizeField * sizeField; i++)
            {
                for (int j = 0; j < sizeField * sizeField; j++)
                {
                    syd[i, j] = arrSud[i, j];
                }
            }
            arrSud = syd;
        }

        private void CreateField()
        {
            for (int i = 0; i < sizeField * sizeField; i++)
            {
                for (int j = 0; j < sizeField * sizeField; j++)
                {
                    Button btn = new Button();
                    arrBu[i, j] = btn;
                    btn.Size = new Size(sizeButton, sizeButton);
                    btn.Text = arrSud[i, j].ToString();
                    btn.Location = new Point(300 + j * sizeButton, 80 + i * sizeButton);
                    btn.Click += FmSudoky_Click;
                    this.Controls.Add(btn);
                }
            }
        }

    }
}
