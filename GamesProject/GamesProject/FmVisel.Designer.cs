﻿
namespace GamesProject
{
    partial class FmVisel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.laTime = new System.Windows.Forms.Label();
            this.buStop = new System.Windows.Forms.Button();
            this.buStart = new System.Windows.Forms.Button();
            this.pxVisel = new System.Windows.Forms.PictureBox();
            this.laWord = new System.Windows.Forms.Label();
            this.tbEdLetter = new System.Windows.Forms.TextBox();
            this.tbWord = new System.Windows.Forms.TextBox();
            this.laLetters = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tbRules = new System.Windows.Forms.ToolStripMenuItem();
            this.tbReset = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pxVisel)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // laTime
            // 
            this.laTime.AutoSize = true;
            this.laTime.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.laTime.Location = new System.Drawing.Point(69, 24);
            this.laTime.Name = "laTime";
            this.laTime.Size = new System.Drawing.Size(234, 32);
            this.laTime.TabIndex = 15;
            this.laTime.Text = "Время игры: 00:00";
            this.laTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buStop
            // 
            this.buStop.BackColor = System.Drawing.Color.LightCyan;
            this.buStop.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.buStop.Location = new System.Drawing.Point(186, 382);
            this.buStop.Name = "buStop";
            this.buStop.Size = new System.Drawing.Size(164, 62);
            this.buStop.TabIndex = 14;
            this.buStop.Text = "Закончить";
            this.buStop.UseVisualStyleBackColor = false;
            // 
            // buStart
            // 
            this.buStart.BackColor = System.Drawing.Color.LightCyan;
            this.buStart.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.buStart.Location = new System.Drawing.Point(12, 382);
            this.buStart.Name = "buStart";
            this.buStart.Size = new System.Drawing.Size(161, 62);
            this.buStart.TabIndex = 13;
            this.buStart.Text = "Начать";
            this.buStart.UseVisualStyleBackColor = false;
            // 
            // pxVisel
            // 
            this.pxVisel.Location = new System.Drawing.Point(326, 41);
            this.pxVisel.Name = "pxVisel";
            this.pxVisel.Size = new System.Drawing.Size(433, 316);
            this.pxVisel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pxVisel.TabIndex = 17;
            this.pxVisel.TabStop = false;
            // 
            // laWord
            // 
            this.laWord.AutoSize = true;
            this.laWord.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laWord.Location = new System.Drawing.Point(485, 393);
            this.laWord.Name = "laWord";
            this.laWord.Size = new System.Drawing.Size(118, 37);
            this.laWord.TabIndex = 18;
            this.laWord.Text = "_ _ _ _ _ _";
            // 
            // tbEdLetter
            // 
            this.tbEdLetter.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.tbEdLetter.Location = new System.Drawing.Point(79, 271);
            this.tbEdLetter.Name = "tbEdLetter";
            this.tbEdLetter.Size = new System.Drawing.Size(185, 43);
            this.tbEdLetter.TabIndex = 19;
            this.tbEdLetter.Text = "Введите букву";
            // 
            // tbWord
            // 
            this.tbWord.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.tbWord.Location = new System.Drawing.Point(79, 333);
            this.tbWord.Name = "tbWord";
            this.tbWord.Size = new System.Drawing.Size(185, 43);
            this.tbWord.TabIndex = 23;
            this.tbWord.Text = "Назвать слово";
            // 
            // laLetters
            // 
            this.laLetters.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laLetters.Location = new System.Drawing.Point(12, 164);
            this.laLetters.Name = "laLetters";
            this.laLetters.Size = new System.Drawing.Size(308, 104);
            this.laLetters.TabIndex = 24;
            this.laLetters.Text = "Назывались:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbRules,
            this.tbReset});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 25;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tbRules
            // 
            this.tbRules.Name = "tbRules";
            this.tbRules.Size = new System.Drawing.Size(67, 20);
            this.tbRules.Text = "Правила";
            // 
            // tbReset
            // 
            this.tbReset.Name = "tbReset";
            this.tbReset.Size = new System.Drawing.Size(99, 20);
            this.tbReset.Text = "Начать заново";
            // 
            // FmVisel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.laLetters);
            this.Controls.Add(this.tbWord);
            this.Controls.Add(this.tbEdLetter);
            this.Controls.Add(this.laWord);
            this.Controls.Add(this.pxVisel);
            this.Controls.Add(this.laTime);
            this.Controls.Add(this.buStop);
            this.Controls.Add(this.buStart);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FmVisel";
            this.Text = "Виселица";
            ((System.ComponentModel.ISupportInitialize)(this.pxVisel)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label laTime;
        private System.Windows.Forms.Button buStop;
        private System.Windows.Forms.Button buStart;
        private System.Windows.Forms.PictureBox pxVisel;
        private System.Windows.Forms.Label laWord;
        private System.Windows.Forms.TextBox tbEdLetter;
        private System.Windows.Forms.TextBox tbWord;
        private System.Windows.Forms.Label laLetters;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tbRules;
        private System.Windows.Forms.ToolStripMenuItem tbReset;
    }
}