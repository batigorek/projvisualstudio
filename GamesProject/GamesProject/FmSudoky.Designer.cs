﻿
namespace GamesProject
{
    partial class FmSudoky
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tbRules = new System.Windows.Forms.ToolStripMenuItem();
            this.tbReset = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.laTime = new System.Windows.Forms.Label();
            this.buStop = new System.Windows.Forms.Button();
            this.buStart = new System.Windows.Forms.Button();
            this.laDifficult = new System.Windows.Forms.Label();
            this.buPlus = new System.Windows.Forms.Button();
            this.buMinus = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbRules,
            this.tbReset});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(683, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tbRules
            // 
            this.tbRules.Name = "tbRules";
            this.tbRules.Size = new System.Drawing.Size(67, 20);
            this.tbRules.Text = "Правила";
            // 
            // tbReset
            // 
            this.tbReset.Name = "tbReset";
            this.tbReset.Size = new System.Drawing.Size(99, 20);
            this.tbReset.Text = "Начать заново";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 3;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 3;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(34, 32);
            this.tableLayoutPanel8.TabIndex = 2;
            // 
            // laTime
            // 
            this.laTime.AutoSize = true;
            this.laTime.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.laTime.Location = new System.Drawing.Point(12, 48);
            this.laTime.Name = "laTime";
            this.laTime.Size = new System.Drawing.Size(234, 32);
            this.laTime.TabIndex = 10;
            this.laTime.Text = "Время игры: 00:00";
            this.laTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buStop
            // 
            this.buStop.BackColor = System.Drawing.Color.LightCyan;
            this.buStop.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.buStop.Location = new System.Drawing.Point(51, 376);
            this.buStop.Name = "buStop";
            this.buStop.Size = new System.Drawing.Size(164, 62);
            this.buStop.TabIndex = 9;
            this.buStop.Text = "Закончить";
            this.buStop.UseVisualStyleBackColor = false;
            // 
            // buStart
            // 
            this.buStart.BackColor = System.Drawing.Color.LightCyan;
            this.buStart.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.buStart.Location = new System.Drawing.Point(54, 289);
            this.buStart.Name = "buStart";
            this.buStart.Size = new System.Drawing.Size(161, 62);
            this.buStart.TabIndex = 8;
            this.buStart.Text = "Начать";
            this.buStart.UseVisualStyleBackColor = false;
            // 
            // laDifficult
            // 
            this.laDifficult.AutoSize = true;
            this.laDifficult.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.laDifficult.Location = new System.Drawing.Point(54, 140);
            this.laDifficult.Name = "laDifficult";
            this.laDifficult.Size = new System.Drawing.Size(172, 32);
            this.laDifficult.TabIndex = 11;
            this.laDifficult.Text = "Сложность: 1";
            this.laDifficult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buPlus
            // 
            this.buPlus.BackColor = System.Drawing.Color.LightCyan;
            this.buPlus.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.buPlus.Location = new System.Drawing.Point(157, 183);
            this.buPlus.Name = "buPlus";
            this.buPlus.Size = new System.Drawing.Size(58, 45);
            this.buPlus.TabIndex = 23;
            this.buPlus.Text = "+";
            this.buPlus.UseVisualStyleBackColor = false;
            // 
            // buMinus
            // 
            this.buMinus.BackColor = System.Drawing.Color.LightCyan;
            this.buMinus.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.buMinus.Location = new System.Drawing.Point(54, 183);
            this.buMinus.Name = "buMinus";
            this.buMinus.Size = new System.Drawing.Size(58, 45);
            this.buMinus.TabIndex = 22;
            this.buMinus.Text = "-";
            this.buMinus.UseVisualStyleBackColor = false;
            // 
            // FmSudoky
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(683, 455);
            this.Controls.Add(this.buPlus);
            this.Controls.Add(this.buMinus);
            this.Controls.Add(this.laDifficult);
            this.Controls.Add(this.laTime);
            this.Controls.Add(this.buStop);
            this.Controls.Add(this.buStart);
            this.Controls.Add(this.menuStrip1);
            this.Name = "FmSudoky";
            this.Text = "Судоку";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tbRules;
        private System.Windows.Forms.ToolStripMenuItem tbReset;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label laTime;
        private System.Windows.Forms.Button buStop;
        private System.Windows.Forms.Button buStart;
        private System.Windows.Forms.Label laDifficult;
        private System.Windows.Forms.Button buPlus;
        private System.Windows.Forms.Button buMinus;
    }
}