﻿
namespace GamesProject
{
    partial class FmLoto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.laTime = new System.Windows.Forms.Label();
            this.buStop = new System.Windows.Forms.Button();
            this.buStart = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tbRules = new System.Windows.Forms.ToolStripMenuItem();
            this.tbReset = new System.Windows.Forms.ToolStripMenuItem();
            this.buOther = new System.Windows.Forms.Button();
            this.laNumber = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // laTime
            // 
            this.laTime.AutoSize = true;
            this.laTime.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.laTime.Location = new System.Drawing.Point(12, 37);
            this.laTime.Name = "laTime";
            this.laTime.Size = new System.Drawing.Size(234, 32);
            this.laTime.TabIndex = 16;
            this.laTime.Text = "Время игры: 00:00";
            this.laTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buStop
            // 
            this.buStop.BackColor = System.Drawing.Color.LightCyan;
            this.buStop.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.buStop.Location = new System.Drawing.Point(51, 380);
            this.buStop.Name = "buStop";
            this.buStop.Size = new System.Drawing.Size(164, 62);
            this.buStop.TabIndex = 15;
            this.buStop.Text = "Закончить";
            this.buStop.UseVisualStyleBackColor = false;
            // 
            // buStart
            // 
            this.buStart.BackColor = System.Drawing.Color.LightCyan;
            this.buStart.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.buStart.Location = new System.Drawing.Point(54, 293);
            this.buStart.Name = "buStart";
            this.buStart.Size = new System.Drawing.Size(161, 62);
            this.buStart.TabIndex = 14;
            this.buStart.Text = "Начать";
            this.buStart.UseVisualStyleBackColor = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbRules,
            this.tbReset});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tbRules
            // 
            this.tbRules.Name = "tbRules";
            this.tbRules.Size = new System.Drawing.Size(67, 20);
            this.tbRules.Text = "Правила";
            // 
            // tbReset
            // 
            this.tbReset.Name = "tbReset";
            this.tbReset.Size = new System.Drawing.Size(99, 20);
            this.tbReset.Text = "Начать заново";
            // 
            // buOther
            // 
            this.buOther.BackColor = System.Drawing.Color.LightCyan;
            this.buOther.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.buOther.Location = new System.Drawing.Point(54, 110);
            this.buOther.Name = "buOther";
            this.buOther.Size = new System.Drawing.Size(161, 77);
            this.buOther.TabIndex = 17;
            this.buOther.Text = "Другое число";
            this.buOther.UseVisualStyleBackColor = false;
            // 
            // laNumber
            // 
            this.laNumber.AutoSize = true;
            this.laNumber.Font = new System.Drawing.Font("Segoe UI", 22F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point);
            this.laNumber.Location = new System.Drawing.Point(479, 110);
            this.laNumber.Name = "laNumber";
            this.laNumber.Size = new System.Drawing.Size(100, 41);
            this.laNumber.TabIndex = 18;
            this.laNumber.Text = "Число";
            this.laNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FmLoto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.laNumber);
            this.Controls.Add(this.buOther);
            this.Controls.Add(this.laTime);
            this.Controls.Add(this.buStop);
            this.Controls.Add(this.buStart);
            this.Controls.Add(this.menuStrip1);
            this.Name = "FmLoto";
            this.Text = "Лото";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label laTime;
        private System.Windows.Forms.Button buStop;
        private System.Windows.Forms.Button buStart;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tbRules;
        private System.Windows.Forms.ToolStripMenuItem tbReset;
        private System.Windows.Forms.Button oth;
        private System.Windows.Forms.Button buOther;
        private System.Windows.Forms.Label laNumber;
    }
}