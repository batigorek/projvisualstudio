﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GamesProject
{
    public partial class FmVisel : Form
    {
        private string[] arrWords;
        private string word;
        public char[] wordsLetter, wordGame;
        int mistake = 0;
        private DateTime startUp;

        Timer tmUp = new Timer();

        public FmVisel(Fm fm)
        {
            InitializeComponent();

            fm.Hide();

            this.FormClosed += (s, e) => fm.Show();

            
            arrWords = Properties.Resources.words.Split("\n");
            buStart.Click += BuStart_Click;
            tbEdLetter.KeyDown += TbEdLetter_KeyDown;
            tbRules.Text = "Один участник (программа) загадывает слово и рисует на листе такое количество подчёркиваний, сколько букв в слове." +
                "Другой участник начинает называть буквы, чтобы отгадать слово. Если буква есть в слове, то водящий обязан вписать её на своё место " +
                "в слово (если таких букв несколько — то вписываются все), а если нет — то он рисует один элемент виселицы с человечком (стойка, " +
                "перекладина, распорка, верёвка, голова, туловище, 2 руки, 2 ноги — всего 10 элементов-попыток). " +
                "Если отгадывающий не успел угадать слово раньше, чем виселица нарисована полностью, то он считается «повешенным» " +
                "и должен угадывать следующее слово.Если слово отгадано — то игроки меняются местами." +
                "Чтобы посмотреть ход прошлой игры, нажмите на кнопку в меню 'Прошлая игра'." +
                "Чтобы начать игру заново, нажмите на кнопку в меню 'Начать заново'." +
                "Чтобы начать игру, нажмите на кнопку 'Начать'." +
                "Чтобы завершить игру, нажмите на кнопку 'Завершить'." +
                "Чтобы ввести букву, нажмите на поле ввода, введите букву и нажмите enter.";
            buStop.Click += BuStop_Click;
            tbEdLetter.Click += (s, e) => tbEdLetter.Text = "";
            tbWord.Click += (s, e) => tbWord.Text = "";
            tbWord.KeyDown += TbWord_KeyDown;
            tbReset.Click += (s, e) => { tmUp.Stop(); tmUp.Dispose(); Start(); };
        }

        private void TbWord_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && tbWord.Text != "")
            {
                if (tbWord.Text == word)
                    MessageBox.Show("Поздравляем! Вы выиграли!");
                else
                    MessageBox.Show("К сожалению, вы проиграли :(");
            }
        }

        private void BuStop_Click(object sender, EventArgs e)
        {
            tmUp.Stop();

            tmUp.Dispose();
        }

        private void TbEdLetter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter  && tbEdLetter.Text != "")
            {
                laLetters.Text += $" {tbEdLetter.Text.ToLower()}; ";
                if (word.Contains(tbEdLetter.Text.ToLower()))
                {
                    for (int i = 0; i < wordsLetter.Length; i++)
                    {
                        if (wordsLetter[i].ToString() == tbEdLetter.Text.ToLower())
                            wordGame[i] = wordsLetter[i];
                    }
                    laWord.Text = string.Join(' ', wordGame);
                }
                else
                {
                    mistake++;
                    switch (mistake)
                    {
                        case 1:
                            pxVisel.Image = Properties.Resources.ViselStart;
                            break;
                        case 2:
                            pxVisel.Image = Properties.Resources.Visel1;
                            break;
                        case 3:
                            pxVisel.Image = Properties.Resources.Visel2;
                            break;
                        case 4:
                            pxVisel.Image = Properties.Resources.Visel3;
                            break;
                        case 5:
                            pxVisel.Image = Properties.Resources.Visel4;
                            break;
                        case 6:
                            pxVisel.Image = Properties.Resources.Visel5;
                            break;
                        case 7:
                            pxVisel.Image = Properties.Resources.Visel6;
                            break;
                        case 8:
                            pxVisel.Image = Properties.Resources.Visel7;
                            break;
                        case 9:
                            pxVisel.Image = Properties.Resources.Visel8;
                            break;
                        case 10:
                            pxVisel.Image = Properties.Resources.Visel9;
                            break;
                        case 11:
                            pxVisel.Image = Properties.Resources.Visel10;
                            MessageBox.Show("Вы проиграли");
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void BuStart_Click(object sender, EventArgs e)
        {
            Start();
        }

        private void Start()
        {
            mistake = 0;
            laWord.Text = "";
            Random rnd = new Random();
            word = arrWords[rnd.Next(0, arrWords.Length)];
            wordsLetter = word.ToCharArray();
            wordGame = word.ToCharArray();
            for (int i = 0; i < wordGame.Length - 1; i++)
            {
                wordGame[i] = '_';
            }
            laWord.Text = string.Join(' ', wordGame);
            tmUp.Enabled = !tmUp.Enabled;
            startUp = DateTime.Now;
            tmUp.Tick += TmUp_Tick;
        }

        private void TmUp_Tick(object sender, EventArgs e)
        {
            var x = DateTime.Now - startUp;
            laTime.Text = $"Время игры: {x.ToString(@"mm\:ss")}";
        }
    }
}
