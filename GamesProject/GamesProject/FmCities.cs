﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GamesProject
{
    public partial class FmCities : Form
    {
        private string[] arr;
        private char[] arrGW, arrPW, arrCheck;
        List<string> arrGame = new List<string>();
        Random rnd = new Random();
        private DateTime startUp;

        Timer tmUp = new Timer();

        public FmCities(Fm fm)
        {
            InitializeComponent();

            fm.Hide();

            this.FormClosed += (s, e) => fm.Show();

            tbRules.Click += (s, e) => MessageBox.Show("Города́ — игра для нескольких (двух или более) человек," +
                " в которой каждый участник в свою очередь называет реально существующий город любой страны, " +
                "название которого начинается на ту букву, которой оканчивается название предыдущего города." +
                "Для начала игры нажмите кнопку 'Начать'. Чтобы завершить игру, нажмите кнопку 'Завершить'." +
                "Чтобы ввести свой город, необходимо нажать на поле ввода, ввести город и затем нажать enter." +
                "Вы можете начать игру заново, нажав на панели меню 'Начать заново'. " +
                "Также вы можете просмотреть историю прошлой игры, нажав на панели меню 'Прошлая игра'.");

            arr = Properties.Resources.goroda.Split('\n');

            buStart.Click += BuStart_Click;
            tbEditCity.KeyDown += TbEditCity_KeyUp;
            buStop.Click += BuStop_Click;
            tbLast.Click += TbLast_Click;
            tbReset.Click += (s, e) => { tmUp.Stop(); tmUp.Dispose(); Start(); };
        }

        private void TbLast_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process txt = new System.Diagnostics.Process();
            txt.StartInfo.FileName = "notepad.exe";
            txt.StartInfo.Arguments = @"game.txt";
            txt.Start();
        }

        private void BuStop_Click(object sender, EventArgs e)
        {
            string filename =  @"game.txt";
            string text = "";
            for (int i = 0; i < arrGame.Count; i++)
            {
            text += $"{arrGame[i]}\n";
            }
            // сохраняем текст в файл
            System.IO.File.WriteAllText(filename, text);
            tmUp.Stop();

            tmUp.Dispose();
        }

        private void TbEditCity_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                arrPW = laCity.Text.ToUpper().ToCharArray();
                arrGW = tbEditCity.Text.ToUpper().ToCharArray();
                if (arrPW[arrPW.Length-1]==arrGW[0] && !arrGame.Contains(tbEditCity.Text))
                {
                    arrGame.Add(tbEditCity.Text);
                    for (int i = 0; i < arr.Length; i++)
                    {
                        arrCheck = arr[i].ToUpper().ToCharArray();
                        if (arrCheck[0]==arrGW[arrGW.Length - 1] && !arrGame.Contains(arr[i]))
                        {
                         laCity.Text = arr[i];
                         tbEditCity.Text = "";
                            break;
                        }
                    }
                }
            }
        }

        private void BuStart_Click(object sender, EventArgs e)
        {
            Start();
        }
        private void Start()
        {
            arrGame.Clear();
            laCity.Text = arr[rnd.Next(0, arr.Length)];
            arrGame.Add(laCity.Text);
            tmUp.Enabled = !tmUp.Enabled;
            startUp = DateTime.Now;
            tmUp.Tick += TmUp_Tick;
        }

        private void TmUp_Tick(object sender, EventArgs e)
        {
            var x = DateTime.Now - startUp;
            laTime.Text = $"Время игры: {x.ToString(@"mm\:ss")}";
        }
    }
}
 