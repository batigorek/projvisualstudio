﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GamesProject
{
    public partial class Fm : Form
    {
        public Fm()
        {
            InitializeComponent();

            buAbout.Click += (s, e) => MessageBox.Show("Данная программа разработана в рамках курса по Мобильной разработке студенткой группы 1881-324 Миленой Крюковой.");
            buCities.Click += (s, e) => { FmCities frmC = new FmCities(this); frmC.Show(); this.Hide(); };
            buSudoky.Click += (s, e) => { FmSudoky frmS = new FmSudoky(this); frmS.Show(); this.Hide(); };
            buBingo.Click += (s, e) => { FmLoto frmB = new FmLoto(this); frmB.Show(); this.Hide(); };
            buVisel.Click += (s, e) => { FmVisel frmV = new FmVisel(this); frmV.Show(); this.Hide(); };
        }

    }
}
