﻿
namespace GamesProject
{
    partial class FmCities
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tbRules = new System.Windows.Forms.ToolStripMenuItem();
            this.tbLast = new System.Windows.Forms.ToolStripMenuItem();
            this.tbReset = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.laCity = new System.Windows.Forms.Label();
            this.buStart = new System.Windows.Forms.Button();
            this.buStop = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tbEditCity = new System.Windows.Forms.TextBox();
            this.laTime = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbRules,
            this.tbLast,
            this.tbReset});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(355, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tbRules
            // 
            this.tbRules.Name = "tbRules";
            this.tbRules.Size = new System.Drawing.Size(67, 20);
            this.tbRules.Text = "Правила";
            // 
            // tbLast
            // 
            this.tbLast.Name = "tbLast";
            this.tbLast.Size = new System.Drawing.Size(100, 20);
            this.tbLast.Text = "Прошлая игра";
            // 
            // tbReset
            // 
            this.tbReset.Name = "tbReset";
            this.tbReset.Size = new System.Drawing.Size(99, 20);
            this.tbReset.Text = "Начать заново";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(88, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 32);
            this.label1.TabIndex = 1;
            this.label1.Text = "Слово для вас";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // laCity
            // 
            this.laCity.AutoSize = true;
            this.laCity.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point);
            this.laCity.Location = new System.Drawing.Point(62, 89);
            this.laCity.Name = "laCity";
            this.laCity.Size = new System.Drawing.Size(0, 37);
            this.laCity.TabIndex = 2;
            this.laCity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buStart
            // 
            this.buStart.BackColor = System.Drawing.Color.LightCyan;
            this.buStart.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.buStart.Location = new System.Drawing.Point(12, 321);
            this.buStart.Name = "buStart";
            this.buStart.Size = new System.Drawing.Size(161, 62);
            this.buStart.TabIndex = 3;
            this.buStart.Text = "Начать";
            this.buStart.UseVisualStyleBackColor = false;
            // 
            // buStop
            // 
            this.buStop.BackColor = System.Drawing.Color.LightCyan;
            this.buStop.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.buStop.Location = new System.Drawing.Point(179, 321);
            this.buStop.Name = "buStop";
            this.buStop.Size = new System.Drawing.Size(164, 62);
            this.buStop.TabIndex = 4;
            this.buStop.Text = "Закончить";
            this.buStop.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(104, 157);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 32);
            this.label2.TabIndex = 5;
            this.label2.Text = "Ваше слово";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbEditCity
            // 
            this.tbEditCity.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.tbEditCity.Location = new System.Drawing.Point(88, 212);
            this.tbEditCity.Name = "tbEditCity";
            this.tbEditCity.Size = new System.Drawing.Size(183, 43);
            this.tbEditCity.TabIndex = 6;
            // 
            // laTime
            // 
            this.laTime.AutoSize = true;
            this.laTime.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.laTime.Location = new System.Drawing.Point(62, 273);
            this.laTime.Name = "laTime";
            this.laTime.Size = new System.Drawing.Size(234, 32);
            this.laTime.TabIndex = 7;
            this.laTime.Text = "Время игры: 00:00";
            this.laTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FmCities
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(355, 395);
            this.Controls.Add(this.laTime);
            this.Controls.Add(this.tbEditCity);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buStop);
            this.Controls.Add(this.buStart);
            this.Controls.Add(this.laCity);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FmCities";
            this.Text = "Города";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tbRules;
        private System.Windows.Forms.ToolStripMenuItem tbLast;
        private System.Windows.Forms.ToolStripMenuItem tbReset;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label laCity;
        private System.Windows.Forms.Button buStart;
        private System.Windows.Forms.Button buStop;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbEditCity;
        private System.Windows.Forms.Label laTime;
    }
}