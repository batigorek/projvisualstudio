﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace labTask
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start");

            var t1 = new Task(MyTask1);
            t1.Start();
            //t1.Status;

            new Task(() => Console.WriteLine("Task2")).Start();

            Task.Run(() => Console.WriteLine("Task3"));

            var t4 = new Task(MyTask4);
            t4.Start();

            new Task(async () => { await Task.Delay(500); Console.WriteLine("Task5"); }).Start();

            Console.WriteLine("End");
            Console.ReadKey();
        }

        private static void MyTask4()
        {
            Console.WriteLine("Task4 start");

            Thread.Sleep(1000);//Task.Delay(500);

            Console.WriteLine("Task4 end");
        }

        private static void MyTask1()
        {
            Console.WriteLine("Task1");
        }
    }
}
