﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labParallel
{
    public partial class Fm : Form
    {
        private const int FILES_COUNT = 100;

        public Fm()
        {
            InitializeComponent();

            edDirTemp.Text = Path.Combine(Path.GetTempPath(), Application.ProductName);
            Directory.CreateDirectory(edDirTemp.Text);

            edLogs.Text = "";

            buCreateFiles.Click += BuCreateFiles_Click;
            buDeleteFiles.Click += BuDeleteFiles_Click;
            button3.Click += Button3_Click;
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Parallel.Invoke(
                () => { /*task1*/},
                () => { /*task2*/},
                () => { /*task3*/},
                () => { /*task4*/}
                );
        }

        private void BuDeleteFiles_Click(object sender, EventArgs e)
        {
            Parallel.For(0, FILES_COUNT, (v) => File.Delete(Path.Combine(edDirTemp.Text, $"file_{v:000}.txt")));
        }

        private void BuCreateFiles_Click(object sender, EventArgs e)
        {
            Parallel.For(0, FILES_COUNT, CreateFile);
        }

        private void CreateFile(int arg1, ParallelLoopState arg2)
        {
            File.Create(Path.Combine(edDirTemp.Text, $"file_{arg1:000}.txt")).Close();
            Thread.Sleep(50);
            edLogs.Text += $"file_{arg1:000}.txt" + Environment.NewLine;
        }
    }
}
