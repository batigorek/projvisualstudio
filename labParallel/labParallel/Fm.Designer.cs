﻿namespace labParallel
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.edDirTemp = new System.Windows.Forms.TextBox();
            this.buCreateFiles = new System.Windows.Forms.Button();
            this.buDeleteFiles = new System.Windows.Forms.Button();
            this.edLogs = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.edDirTemp.Location = new System.Drawing.Point(26, 20);
            this.edDirTemp.Name = "textBox1";
            this.edDirTemp.Size = new System.Drawing.Size(740, 23);
            this.edDirTemp.TabIndex = 0;
            // 
            // button1
            // 
            this.buCreateFiles.Location = new System.Drawing.Point(28, 71);
            this.buCreateFiles.Name = "button1";
            this.buCreateFiles.Size = new System.Drawing.Size(239, 67);
            this.buCreateFiles.TabIndex = 1;
            this.buCreateFiles.Text = "Create n files";
            this.buCreateFiles.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.buDeleteFiles.Location = new System.Drawing.Point(273, 71);
            this.buDeleteFiles.Name = "button2";
            this.buDeleteFiles.Size = new System.Drawing.Size(256, 68);
            this.buDeleteFiles.TabIndex = 2;
            this.buDeleteFiles.Text = "Delte n files";
            this.buDeleteFiles.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.edLogs.Location = new System.Drawing.Point(28, 170);
            this.edLogs.Multiline = true;
            this.edLogs.Name = "textBox2";
            this.edLogs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.edLogs.Size = new System.Drawing.Size(738, 268);
            this.edLogs.TabIndex = 3;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(537, 71);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(228, 67);
            this.button3.TabIndex = 4;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.edLogs);
            this.Controls.Add(this.buDeleteFiles);
            this.Controls.Add(this.buCreateFiles);
            this.Controls.Add(this.edDirTemp);
            this.Name = "Fm";
            this.Text = "labParallel";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edDirTemp;
        private System.Windows.Forms.Button buCreateFiles;
        private System.Windows.Forms.Button buDeleteFiles;
        private System.Windows.Forms.TextBox edLogs;
        private System.Windows.Forms.Button button3;
    }
}

