﻿using System;
using System.Collections;

namespace labQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue x = new Queue();
            x.Enqueue(8);
            x.Enqueue("Text");
            x.Enqueue(22);
            Console.WriteLine(x.Peek()); //watch
            Console.WriteLine("_____");

            while (x.Count > 0)
            {
                Console.WriteLine(x.Dequeue()); //out from queue
            }
        }
    }
}
