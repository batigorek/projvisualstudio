﻿using System;
using System.Collections.Generic;

namespace labQueueT
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<int> x = new Queue<int>();
            x.Enqueue(8);
            x.Enqueue(10);
            x.Enqueue(22);
            Console.WriteLine(x.Peek()); //watch
            Console.WriteLine("_____");

                int sum = 0;
            while (x.Count > 0)
            {
                int v = x.Dequeue(); //out from queue
                sum += v;
                Console.WriteLine(v);
            }
            Console.WriteLine("_____");
                Console.WriteLine(sum);

            
        }
    }
}
