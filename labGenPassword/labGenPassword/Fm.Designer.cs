﻿namespace labGenPassword
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.edPassword = new System.Windows.Forms.TextBox();
            this.buPassword = new System.Windows.Forms.Button();
            this.ckLower = new System.Windows.Forms.CheckBox();
            this.ckUpper = new System.Windows.Forms.CheckBox();
            this.ckNumber = new System.Windows.Forms.CheckBox();
            this.ckSpec = new System.Windows.Forms.CheckBox();
            this.edLength = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.edLength)).BeginInit();
            this.SuspendLayout();
            // 
            // edPassword
            // 
            this.edPassword.Dock = System.Windows.Forms.DockStyle.Top;
            this.edPassword.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.edPassword.Location = new System.Drawing.Point(0, 0);
            this.edPassword.Name = "edPassword";
            this.edPassword.Size = new System.Drawing.Size(312, 43);
            this.edPassword.TabIndex = 0;
            this.edPassword.Text = "<Password>";
            this.edPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buPassword
            // 
            this.buPassword.Location = new System.Drawing.Point(14, 96);
            this.buPassword.Name = "buPassword";
            this.buPassword.Size = new System.Drawing.Size(286, 58);
            this.buPassword.TabIndex = 1;
            this.buPassword.Text = "Generate";
            this.buPassword.UseVisualStyleBackColor = true;
            // 
            // ckLower
            // 
            this.ckLower.AutoSize = true;
            this.ckLower.Location = new System.Drawing.Point(20, 184);
            this.ckLower.Name = "ckLower";
            this.ckLower.Size = new System.Drawing.Size(86, 19);
            this.ckLower.TabIndex = 2;
            this.ckLower.Text = "Lower Case";
            this.ckLower.UseVisualStyleBackColor = true;
            // 
            // ckUpper
            // 
            this.ckUpper.AutoSize = true;
            this.ckUpper.Location = new System.Drawing.Point(20, 209);
            this.ckUpper.Name = "ckUpper";
            this.ckUpper.Size = new System.Drawing.Size(86, 19);
            this.ckUpper.TabIndex = 2;
            this.ckUpper.Text = "Upper Case";
            this.ckUpper.UseVisualStyleBackColor = true;
            // 
            // ckNumber
            // 
            this.ckNumber.AutoSize = true;
            this.ckNumber.Location = new System.Drawing.Point(20, 234);
            this.ckNumber.Name = "ckNumber";
            this.ckNumber.Size = new System.Drawing.Size(75, 19);
            this.ckNumber.TabIndex = 2;
            this.ckNumber.Text = "Numbers";
            this.ckNumber.UseVisualStyleBackColor = true;
            // 
            // ckSpec
            // 
            this.ckSpec.AutoSize = true;
            this.ckSpec.Location = new System.Drawing.Point(20, 259);
            this.ckSpec.Name = "ckSpec";
            this.ckSpec.Size = new System.Drawing.Size(110, 19);
            this.ckSpec.TabIndex = 2;
            this.ckSpec.Text = "Special symbols";
            this.ckSpec.UseVisualStyleBackColor = true;
            // 
            // edLength
            // 
            this.edLength.Location = new System.Drawing.Point(85, 332);
            this.edLength.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.edLength.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.edLength.Name = "edLength";
            this.edLength.Size = new System.Drawing.Size(120, 23);
            this.edLength.TabIndex = 3;
            this.edLength.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 450);
            this.Controls.Add(this.edLength);
            this.Controls.Add(this.ckSpec);
            this.Controls.Add(this.ckNumber);
            this.Controls.Add(this.ckUpper);
            this.Controls.Add(this.ckLower);
            this.Controls.Add(this.buPassword);
            this.Controls.Add(this.edPassword);
            this.Name = "Fm";
            this.Text = "labGenPassword";
            ((System.ComponentModel.ISupportInitialize)(this.edLength)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edPassword;
        private System.Windows.Forms.Button buPassword;
        private System.Windows.Forms.CheckBox ckLower;
        private System.Windows.Forms.CheckBox ckUpper;
        private System.Windows.Forms.CheckBox ckNumber;
        private System.Windows.Forms.CheckBox ckSpec;
        private System.Windows.Forms.NumericUpDown edLength;
    }
}

