﻿using System;
using System.Text;

namespace labGenPassword_Core
{
    public class Utils
    {
        public static string RandomStr(int length, bool lower, bool upper, bool number, bool special)
        {
            string c1 = "qwertyuiopasdfghjklzxcvbnm";
            string c2 = "0123456789";
            string c3 = "<>{}[]%$#@";

            var x = new StringBuilder(); //simbols
            var xResult = new StringBuilder(); //result
            var rnd = new Random();

            //forming simbols with parametrs 
            if (lower) x.Append(c1);
            if (upper) x.Append(c1.ToUpper());
            if (number) x.Append(c2);
            if (special) x.Append(c3);
            //if no parametrs -> lower
            if (x.ToString() == "") x.Append(c1);

            while (xResult.Length < length)
            {
                xResult.Append(x[rnd.Next(x.Length)]);
            }

            return xResult.ToString();
        }

        public void Check()
        {

        }

    }
}
