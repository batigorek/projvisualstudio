﻿using labGenPassword_Core;
using System;

namespace labGeneratePasswordCNS
{
    class Program
    {
        // HW
        // another parametrs
        static void Main(string[] args)
        {
            string edit;
            bool lower, upper, number, special;
            int length;

            do
            {
            Console.Clear();
            Console.WriteLine("Count");
            length = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Lower Case");
            edit = Console.ReadLine().ToString();
            if (edit == "yes")
                lower = true;
            else lower = false;

            Console.WriteLine("Upper Case");
            edit = Console.ReadLine().ToString();
            if (edit == "yes")
                upper = true;
            else upper = false;

            Console.WriteLine("Numbers");
            edit = Console.ReadLine().ToString();
            if (edit == "yes")
                number = true;
            else number = false;

            Console.WriteLine("Special symbols");
            edit = Console.ReadLine().ToString();
            if (edit == "yes")
                special = true;
            else special = false;


            Console.WriteLine(Utils.RandomStr(length, lower, upper, number, special));

            Console.WriteLine("Generate new?");
            } while (Console.ReadLine() != "no");
        }

    }
}
