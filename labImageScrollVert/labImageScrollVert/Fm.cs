﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageScrollVert
{
    public partial class Fm : Form
    {
        private readonly Bitmap b;
        private readonly Graphics g;
        private Bitmap imBG;
        private int deltaX;
        private Point startPoint;
        System.Threading.Thread hold;
        int speed = 2;

        public Fm()
        {
            InitializeComponent();

            imBG = Properties.Resources.fonvert;
            this.Width = imBG.Width;

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);

            this.DoubleBuffered = true;
            this.Paint += (s, e) => { UpdateBG(); e.Graphics.DrawImage(b, 0, 0); };

            this.MouseDown += (s, e) => startPoint = e.Location;
            this.MouseMove += Fm_MouseMove;
            this.KeyDown += Fm_KeyDown;
            this.KeyUp += Fm_KeyUp;
            // HW
            // research background asking "background hor game"
            // different speed bg

        }

        private void Fm_KeyUp(object sender, KeyEventArgs e)
        {
            speed = 2;
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            hold = new System.Threading.Thread(() => ButtonIsHeld());
            hold.Start();

            switch (e.KeyCode)
            {
                case Keys.Up:
                    UpdateDeltaX(speed);
                    break;
                case Keys.Down:
                    UpdateDeltaX(-speed);
                    break;
                default:
                    break;
            }
            this.Invalidate();
        }

        private void ButtonIsHeld()
        {
            System.Threading.Thread.Sleep(1000);
            if (speed < 20)
            {
                speed *= 2;
            }
        }

        private void Fm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                UpdateDeltaX(e.Y - startPoint.Y);
                startPoint = e.Location;
                this.Invalidate();
            }
        }

        private void UpdateDeltaX(int v)
        {
            this.Text = $"{Application.ProductName}: {deltaX}, {v}";
            deltaX += v;
            if (deltaX > 0)
                deltaX -= imBG.Height;
            else
                if (deltaX < -imBG.Height)
                deltaX += imBG.Height;
        }

        private void UpdateBG()
        {
            for (int i = 0; i < Screen.PrimaryScreen.Bounds.Height / imBG.Height * 3; i++)
            {
                g.DrawImage(imBG, 0, deltaX + i * imBG.Height);
            }
        }
    }
}
