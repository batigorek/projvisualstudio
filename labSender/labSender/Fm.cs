﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labSender
{
    public partial class Fm : Form
    {
        public Fm()
        {
            InitializeComponent();

            button1.Click += button1_Click;
            button2.Click += button1_Click;
            button3.Click += button1_Click;
            label1.Click += button1_Click;
            label2.Click += button1_Click;
            label3.Click += button1_Click;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //if (sender is Control)
          //      ((Control)sender).Text = "Click";
          if (sender is Control x)
               x.Text = "Click";

           // (sender as Control).Text = "Click";
        }
    }
}
