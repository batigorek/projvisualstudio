﻿namespace labSoundPlayer
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buPlay = new System.Windows.Forms.Button();
            this.buStop = new System.Windows.Forms.Button();
            this.buPlayLooping = new System.Windows.Forms.Button();
            this.buSystem = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.buPlay.Location = new System.Drawing.Point(95, 53);
            this.buPlay.Name = "button1";
            this.buPlay.Size = new System.Drawing.Size(75, 23);
            this.buPlay.TabIndex = 0;
            this.buPlay.Text = "button1";
            this.buPlay.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.buStop.Location = new System.Drawing.Point(241, 63);
            this.buStop.Name = "button2";
            this.buStop.Size = new System.Drawing.Size(75, 23);
            this.buStop.TabIndex = 1;
            this.buStop.Text = "button2";
            this.buStop.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.buPlayLooping.Location = new System.Drawing.Point(161, 127);
            this.buPlayLooping.Name = "button3";
            this.buPlayLooping.Size = new System.Drawing.Size(113, 26);
            this.buPlayLooping.TabIndex = 2;
            this.buPlayLooping.Text = "button3";
            this.buPlayLooping.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.buSystem.Location = new System.Drawing.Point(170, 175);
            this.buSystem.Name = "button4";
            this.buSystem.Size = new System.Drawing.Size(75, 23);
            this.buSystem.TabIndex = 3;
            this.buSystem.Text = "button4";
            this.buSystem.UseVisualStyleBackColor = true;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 450);
            this.Controls.Add(this.buSystem);
            this.Controls.Add(this.buPlayLooping);
            this.Controls.Add(this.buStop);
            this.Controls.Add(this.buPlay);
            this.Name = "Fm";
            this.Text = "labSoundPlayer";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buPlay;
        private System.Windows.Forms.Button buStop;
        private System.Windows.Forms.Button buPlayLooping;
        private System.Windows.Forms.Button buSystem;
    }
}

