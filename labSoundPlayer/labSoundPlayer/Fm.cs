﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labSoundPlayer
{
    public partial class Fm : Form
    {

        private SoundPlayer soundPlayer = new SoundPlayer();

        public Fm()
        {
            InitializeComponent();

            //soundPlayer.Stream = @""; error
            soundPlayer.Stream = Properties.Resources.K_SONIC_KICKLE;

            buPlay.Click += (s, e) => soundPlayer.Play();
            buStop.Click += (s, e) => soundPlayer.Stop();
            buPlayLooping.Click += (s, e) => soundPlayer.PlayLooping();

            buSystem.Click += (s, e) => SystemSounds.Beep.Play();
        }

    }
}
