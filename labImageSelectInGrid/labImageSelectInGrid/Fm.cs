﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageSelectInGrid
{
    public partial class Fm : Form
    {
        private readonly Bitmap b;
        private readonly Graphics g;
        private int curRow;
        private int curCol;

        public int Cols { get; private set; } = 4;
        public int Rows { get; private set; } = 5;
        public int CellWidth { get; private set; }
        public int CellHeight { get; private set; }

        int k = 0, l = 0;
        private (int Row, int Col) selBegin;
        private (int Row, int Col) selEnd;
        private (int Row, int Col) selBeginR;
        private (int Row, int Col) selEndR;
        private SelMode selMode = SelMode.Normal;
        private Point curPoint;
        private Point startPoint;

        public Fm()
        {
            InitializeComponent();

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);

            this.DoubleBuffered = true;
            //this.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);
            this.Paint += (s, e) => e.Graphics.DrawImage(b, curPoint);
            this.Resize += (s, e) => ResizeCells();
            this.MouseMove += Fm_MouseMove;
            this.KeyDown += Fm_KeyDown;
            this.MouseDown += Fm_MouseDown;
            this.KeyUp += Fm_KeyUp;

            this.Text = $"{Application.ProductName} : (F5/F6 - Rows, F7/F8 - Cols)";

            ResizeCells();

            // HW
            // moving field with red mouse click
            // few fields down to up
            // wheel - size 
        }

        private void Fm_KeyUp(object sender, KeyEventArgs e)
        {
            selMode = SelMode.Normal;
            ResizeCells();
        }

        private void Fm_MouseDown(object sender, MouseEventArgs e)
        {
            startPoint = e.Location;
           if (e.Button == MouseButtons.Left)
            {
                selBegin = selEnd = (curRow, curCol);
                DrawCells();
            }
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    l++;
                    break;
                case Keys.Down:
                    l--;
                    break;
                case Keys.Left:
                    k--;
                    break;
                case Keys.Right:
                    k++;
                    break;
                case Keys.W:
                    Rows++;
                    break;
                case Keys.S:
                    if (Rows > 2)
                    Rows--;
                    break;
                case Keys.A:
                    Cols++;
                    break;
                case Keys.D:
                    if (Cols > 2)
                        Cols--;
                    break;
                default:
                    break;
            }
            selMode = e.Shift ? SelMode.Line : e.Alt ? SelMode.Square : SelMode.Normal;
            ResizeCells();
        }

        private void Fm_MouseMove(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < Rows; curRow = i, i++)
            {
                if (i * CellWidth > e.Y - curPoint.Y) break;
            }
            for (int i = 0; i < Cols; curCol = i, i++)
            {
                if (i * CellWidth > e.X - curPoint.X) break;
            }
            if (e.Button == MouseButtons.Left)
            {
                selEnd = (curRow, curCol);
                selBeginR = (Math.Min(selBegin.Row, selEnd.Row), Math.Min(selBegin.Col, selEnd.Col));
                selEndR = (Math.Max(selBegin.Row, selEnd.Row), Math.Max(selBegin.Col, selEnd.Col));

                switch (selMode)
                {
                    case SelMode.Line:
                        if (Math.Abs(selBegin.Row - selEnd.Row) > Math.Abs(selBegin.Col - selEnd.Col))
                            selBeginR.Col = selEndR.Col = selBegin.Col;
                        else
                            selBeginR.Row = selEndR.Row = selBegin.Row;
                        //if (selBeginR.col < selEndR.col) selEndR.col = selBeginR.col;
                        break;
                    case SelMode.Square:
                        selBeginR.Col = selEndR.Col = selBegin.Col;
                        selBeginR.Row = selEndR.Row = selBegin.Row;
                        break;
                    case SelMode.Normal:
                        // none
                        break;
                }
            }
            if (e.Button == MouseButtons.Right)
            {
                curPoint.X += e.X - startPoint.X;
                curPoint.Y += e.Y - startPoint.Y;
                startPoint = e.Location;
            }

            DrawCells();
            this.Text = $"{selBegin.Row}:{selBegin.Col} - {selEnd.Row}:{selEnd.Col}";
        }

        private void ResizeCells()
        {
            CellWidth = this.ClientSize.Width / Cols;
            CellHeight = this.ClientSize.Height /  Rows;
            DrawCells();
        }

        private void DrawCells()
        {
            //g.Clear(SystemColors.Control);
            g.Clear(DefaultBackColor);

            g.FillRectangle(new SolidBrush(Color.Green),
                selBeginR.Col * CellWidth,
                selBeginR.Row * CellHeight,
                (selEndR.Col - selBeginR.Col + 1) * CellWidth,
                (selEndR.Row - selBeginR.Row + 1) * CellHeight
            );

            for (int i = 0; i <= Rows; i++)
                g.DrawLine(new Pen(Color.Green, 1), 0, i * CellHeight, Cols * CellWidth, i * CellHeight);
            for (int j = 0; j <= Cols ; j++)
                g.DrawLine(new Pen(Color.Green, 1), j * CellWidth,0,  j * CellWidth, Rows * CellHeight);
            g.DrawRectangle(new Pen(Color.Red, 3), curCol * CellWidth, curRow * CellHeight, CellWidth, CellHeight);
            //g.DrawString($"{curRow+l},{curCol+k}", new Font("", 800/(Rows*Cols)), new SolidBrush(Color.Black), curCol * CellWidth, curRow * CellHeight);
            g.DrawString($"{curRow+l},{curCol+k}", new Font("", 800/(Rows*Cols)), new SolidBrush(Color.Black),
                new Rectangle(curCol * CellWidth, curRow * CellHeight, CellWidth,CellHeight),
                new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center});
            this.Invalidate();
        }
    }

    enum SelMode
    {
        Line,
        Square,
        Normal
    }
}
