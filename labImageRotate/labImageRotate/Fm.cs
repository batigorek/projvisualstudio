﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageRotate
{
    public partial class Fm : Form
    {
        private Bitmap imHero;

        public Fm()
        {
            InitializeComponent();

            imHero = new Bitmap(Properties.Resources._2);

            pictureBox1.Paint += PictureBox1_Paint;
            trackBar1.ValueChanged += (s, e) => pictureBox1.Invalidate();

            button1.Click += (s, e) => { imHero.RotateFlip(RotateFlipType.RotateNoneFlipX); pictureBox1.Invalidate(); };
            button2.Click += (s, e) => { imHero.RotateFlip(RotateFlipType.RotateNoneFlipY); pictureBox1.Invalidate(); };
        }

        private void PictureBox1_Paint(object sender, PaintEventArgs e)
        {
            //e.Graphics.TranslateTransform(ClientSize.Width / 2, ClientSize.Height / 2);
            e.Graphics.TranslateTransform(imHero.Width / 2, imHero.Height / 2);
            e.Graphics.RotateTransform(trackBar1.Value);
            e.Graphics.DrawImage(imHero, -imHero.Width/2, -imHero.Height/2);

            e.Graphics.RotateTransform(-trackBar1.Value);
            e.Graphics.DrawImage(imHero, -imHero.Width / 2, -imHero.Height / 2);
            e.Graphics.DrawImage(imHero, 100, 100);
        }
    }
}
