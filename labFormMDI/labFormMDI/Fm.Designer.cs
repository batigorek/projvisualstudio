﻿
namespace labFormMDI
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.miCreateNewForm = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.miWindowsCascade = new System.Windows.Forms.ToolStripMenuItem();
            this.miWindowsTileHorizontal = new System.Windows.Forms.ToolStripMenuItem();
            this.miWindowsTileVertical = new System.Windows.Forms.ToolStripMenuItem();
            this.miWindowsArrangeIcons = new System.Windows.Forms.ToolStripMenuItem();
            this.miCloseActiveForm = new System.Windows.Forms.ToolStripMenuItem();
            this.miCloseAllForms = new System.Windows.Forms.ToolStripMenuItem();
            this.miAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCreateNewForm,
            this.toolStripMenuItem2,
            this.miCloseActiveForm,
            this.miCloseAllForms,
            this.miAbout});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // miCreateNewForm
            // 
            this.miCreateNewForm.Name = "miCreateNewForm";
            this.miCreateNewForm.Size = new System.Drawing.Size(71, 20);
            this.miCreateNewForm.Text = "NewForm";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miWindowsCascade,
            this.miWindowsTileHorizontal,
            this.miWindowsTileVertical,
            this.miWindowsArrangeIcons});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(50, 20);
            this.toolStripMenuItem2.Text = "Menu";
            // 
            // miWindowsCascade
            // 
            this.miWindowsCascade.Name = "miWindowsCascade";
            this.miWindowsCascade.Size = new System.Drawing.Size(144, 22);
            this.miWindowsCascade.Text = "Cascade";
            // 
            // miWindowsTileHorizontal
            // 
            this.miWindowsTileHorizontal.Name = "miWindowsTileHorizontal";
            this.miWindowsTileHorizontal.Size = new System.Drawing.Size(144, 22);
            this.miWindowsTileHorizontal.Text = "Horizontal";
            // 
            // miWindowsTileVertical
            // 
            this.miWindowsTileVertical.Name = "miWindowsTileVertical";
            this.miWindowsTileVertical.Size = new System.Drawing.Size(144, 22);
            this.miWindowsTileVertical.Text = "Vertical";
            // 
            // miWindowsArrangeIcons
            // 
            this.miWindowsArrangeIcons.Name = "miWindowsArrangeIcons";
            this.miWindowsArrangeIcons.Size = new System.Drawing.Size(144, 22);
            this.miWindowsArrangeIcons.Text = "ArrangeIcons";
            // 
            // miCloseActiveForm
            // 
            this.miCloseActiveForm.Name = "miCloseActiveForm";
            this.miCloseActiveForm.Size = new System.Drawing.Size(79, 20);
            this.miCloseActiveForm.Text = "Close Form";
            // 
            // miCloseAllForms
            // 
            this.miCloseAllForms.Name = "miCloseAllForms";
            this.miCloseAllForms.Size = new System.Drawing.Size(65, 20);
            this.miCloseAllForms.Text = "Close All";
            // 
            // miAbout
            // 
            this.miAbout.Name = "miAbout";
            this.miAbout.Size = new System.Drawing.Size(52, 20);
            this.miAbout.Text = "About";
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Fm";
            this.Text = "labFormMDI";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miCreateNewForm;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem miWindowsCascade;
        private System.Windows.Forms.ToolStripMenuItem miWindowsTileHorizontal;
        private System.Windows.Forms.ToolStripMenuItem miWindowsTileVertical;
        private System.Windows.Forms.ToolStripMenuItem miWindowsArrangeIcons;
        private System.Windows.Forms.ToolStripMenuItem miCloseActiveForm;
        private System.Windows.Forms.ToolStripMenuItem miCloseAllForms;
        private System.Windows.Forms.ToolStripMenuItem miAbout;
    }
}

