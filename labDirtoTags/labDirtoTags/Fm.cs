﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labDirtoTags
{
    public partial class Fm : Form
    {
        public Fm()
        {
            InitializeComponent();

            //edDir.Text = Directory.GetCurrentDirectory();
            edDir.Text = @"C:\MosPol\2020.jpg";

            var s = edDir.Text.Split(new char[] { ' ', '\\' }).ToArray();
            s = s.Select(v => v.TrimStart(new char[] { '(', '"', })
            .TrimEnd(new char[] { ')', '"', ',', '.' })).ToArray();

            edTags.Text = string.Join(Environment.NewLine, s);

            // HW
            // string[] tags = DirToTags(string path); //C:\MosPol *.*
            // form list all the dir to files and folders + form tags + sort be popular
        }

    }
}
