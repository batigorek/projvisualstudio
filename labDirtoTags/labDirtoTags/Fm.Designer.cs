﻿namespace labDirtoTags
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.edDir = new System.Windows.Forms.TextBox();
            this.edTags = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // edDir
            // 
            this.edDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edDir.Location = new System.Drawing.Point(11, 18);
            this.edDir.Name = "edDir";
            this.edDir.Size = new System.Drawing.Size(767, 23);
            this.edDir.TabIndex = 0;
            // 
            // edTags
            // 
            this.edTags.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edTags.Location = new System.Drawing.Point(11, 58);
            this.edTags.Multiline = true;
            this.edTags.Name = "edTags";
            this.edTags.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.edTags.Size = new System.Drawing.Size(766, 295);
            this.edTags.TabIndex = 1;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.edTags);
            this.Controls.Add(this.edDir);
            this.Name = "Fm";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edDir;
        private System.Windows.Forms.TextBox edTags;
    }
}

