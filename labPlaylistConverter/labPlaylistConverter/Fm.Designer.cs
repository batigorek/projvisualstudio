﻿
namespace labPlaylistConverter
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbOpen = new System.Windows.Forms.TextBox();
            this.buOpenFile = new System.Windows.Forms.Button();
            this.buLoad = new System.Windows.Forms.Button();
            this.lv = new System.Windows.Forms.ListView();
            this.buCreate = new System.Windows.Forms.Button();
            this.buSave = new System.Windows.Forms.Button();
            this.tbCreate = new System.Windows.Forms.TextBox();
            this.buUp = new System.Windows.Forms.Button();
            this.buDown = new System.Windows.Forms.Button();
            this.buDelete = new System.Windows.Forms.Button();
            this.edTemp = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // tbOpen
            // 
            this.tbOpen.Location = new System.Drawing.Point(12, 12);
            this.tbOpen.Name = "tbOpen";
            this.tbOpen.Size = new System.Drawing.Size(540, 23);
            this.tbOpen.TabIndex = 0;
            // 
            // buOpenFile
            // 
            this.buOpenFile.Location = new System.Drawing.Point(558, 12);
            this.buOpenFile.Name = "buOpenFile";
            this.buOpenFile.Size = new System.Drawing.Size(73, 23);
            this.buOpenFile.TabIndex = 1;
            this.buOpenFile.Text = "...";
            this.buOpenFile.UseVisualStyleBackColor = true;
            // 
            // buLoad
            // 
            this.buLoad.Location = new System.Drawing.Point(637, 12);
            this.buLoad.Name = "buLoad";
            this.buLoad.Size = new System.Drawing.Size(151, 23);
            this.buLoad.TabIndex = 2;
            this.buLoad.Text = "Load M3U File";
            this.buLoad.UseVisualStyleBackColor = true;
            // 
            // lv
            // 
            this.lv.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lv.HideSelection = false;
            this.lv.Location = new System.Drawing.Point(13, 41);
            this.lv.Name = "lv";
            this.lv.Size = new System.Drawing.Size(424, 361);
            this.lv.TabIndex = 3;
            this.lv.UseCompatibleStateImageBehavior = false;
            // 
            // buCreate
            // 
            this.buCreate.Location = new System.Drawing.Point(638, 408);
            this.buCreate.Name = "buCreate";
            this.buCreate.Size = new System.Drawing.Size(151, 23);
            this.buCreate.TabIndex = 6;
            this.buCreate.Text = "Create folder ";
            this.buCreate.UseVisualStyleBackColor = true;
            // 
            // buSave
            // 
            this.buSave.Location = new System.Drawing.Point(559, 408);
            this.buSave.Name = "buSave";
            this.buSave.Size = new System.Drawing.Size(73, 23);
            this.buSave.TabIndex = 5;
            this.buSave.Text = "...";
            this.buSave.UseVisualStyleBackColor = true;
            // 
            // tbCreate
            // 
            this.tbCreate.Location = new System.Drawing.Point(13, 408);
            this.tbCreate.Name = "tbCreate";
            this.tbCreate.Size = new System.Drawing.Size(540, 23);
            this.tbCreate.TabIndex = 4;
            // 
            // buUp
            // 
            this.buUp.Location = new System.Drawing.Point(463, 70);
            this.buUp.Name = "buUp";
            this.buUp.Size = new System.Drawing.Size(73, 38);
            this.buUp.TabIndex = 7;
            this.buUp.Text = "▲";
            this.buUp.UseVisualStyleBackColor = true;
            // 
            // buDown
            // 
            this.buDown.Location = new System.Drawing.Point(463, 137);
            this.buDown.Name = "buDown";
            this.buDown.Size = new System.Drawing.Size(73, 38);
            this.buDown.TabIndex = 8;
            this.buDown.Text = "▼";
            this.buDown.UseVisualStyleBackColor = true;
            // 
            // buDelete
            // 
            this.buDelete.Location = new System.Drawing.Point(463, 203);
            this.buDelete.Name = "buDelete";
            this.buDelete.Size = new System.Drawing.Size(73, 38);
            this.buDelete.TabIndex = 9;
            this.buDelete.Text = "✖";
            this.buDelete.UseVisualStyleBackColor = true;
            // 
            // edTemp
            // 
            this.edTemp.Location = new System.Drawing.Point(558, 41);
            this.edTemp.Multiline = true;
            this.edTemp.Name = "edTemp";
            this.edTemp.ReadOnly = true;
            this.edTemp.Size = new System.Drawing.Size(229, 360);
            this.edTemp.TabIndex = 10;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(877, 443);
            this.Controls.Add(this.edTemp);
            this.Controls.Add(this.buDelete);
            this.Controls.Add(this.buDown);
            this.Controls.Add(this.buUp);
            this.Controls.Add(this.buCreate);
            this.Controls.Add(this.buSave);
            this.Controls.Add(this.tbCreate);
            this.Controls.Add(this.lv);
            this.Controls.Add(this.buLoad);
            this.Controls.Add(this.buOpenFile);
            this.Controls.Add(this.tbOpen);
            this.Name = "Fm";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbOpen;
        private System.Windows.Forms.Button buOpenFile;
        private System.Windows.Forms.Button buLoad;
        private System.Windows.Forms.ListView lv;
        private System.Windows.Forms.Button buCreate;
        private System.Windows.Forms.Button buSave;
        private System.Windows.Forms.TextBox tbCreate;
        private System.Windows.Forms.Button buUp;
        private System.Windows.Forms.Button buDown;
        private System.Windows.Forms.Button buDelete;
        private System.Windows.Forms.TextBox edTemp;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

