﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPlaylistConverter
{
    public partial class Fm : Form
    {
             private  string PATH_FILE = @"C:\Users\batig\OneDrive\Рабочий стол\1 семестр\Мобильная разработка\Music\playlist.m3u8";
        private string[] textFromFile;
        private int n = 1;
        private string[] musicName;
        ListView.SelectedListViewItemCollection selectedMusic;
        
        public Fm()
        {

            InitializeComponent();

            buOpenFile.Click += BuOpenFile_Click;
            buLoad.Click += (s,e) => LoadMusic();
            lv.ItemActivate += Lv_ItemActivate;
            buUp.Click += BuUp_Click;
            buCreate.Click += BuCreate_Click;
            buSave.Click += BuSave_Click;
            // HW
            // change by buttons list (delete, order etc)
            // move elements in list view with mouse
            // add choose folder for import result + check on existing
            // Alt+up/down - move selected item in list view
            // progress bar on creating file (parallel process)
        }

        private void BuSave_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            tbCreate.Text = saveFileDialog1.FileName;
        }

        private void BuCreate_Click(object sender, EventArgs e)
        {
            string filename;
            filename = tbCreate.Text;
            // сохраняем текст в файл
                System.IO.File.WriteAllText(filename, edTemp.Text);
            MessageBox.Show("Файл сохранен");
        }

        private void BuUp_Click(object sender, EventArgs e)
        {
            if (lv.Items.IndexOf(selectedMusic[0]) != 0)
            {
                //TODO
                lv.BeginUpdate();
                var x = lv.Items[lv.Items.IndexOf(selectedMusic[0]) - 1];
                lv.Items[lv.Items.IndexOf(selectedMusic[0]) - 1].Remove();
                 lv.Items[lv.Items.IndexOf(selectedMusic[0]) - 1] = lv.Items[lv.Items.IndexOf(selectedMusic[0])];
                 lv.Items[lv.Items.IndexOf(selectedMusic[0])] = x;
                lv.EndUpdate();
            }
        }

        private void Lv_ItemActivate(object sender, EventArgs e)
        {
            selectedMusic = this.lv.SelectedItems;
        }

        private void LoadMusic()
        {
            PATH_FILE = tbOpen.Text;
            if (File.Exists(PATH_FILE))
            {
                edTemp.Text = File.ReadAllText(PATH_FILE, Encoding.Unicode);
                textFromFile = File.ReadAllText(PATH_FILE, Encoding.Unicode).Split(Environment.NewLine);

                textFromFile = textFromFile
                .Where(v => !v.StartsWith("#"))
                .Select(v => Path.Combine(Directory.GetParent(PATH_FILE).ToString(), v))
                .ToArray();
                edTemp.Text = string.Join(Environment.NewLine, textFromFile);

                lv.View = View.List;
                foreach (var item in textFromFile)
                {
                    lv.Items.Add(item);
                }

                var xPathNew = Path.Combine(Directory.GetParent(PATH_FILE).ToString(), "OUT");
                Directory.CreateDirectory(xPathNew);
                foreach (var item in textFromFile)
                {
                    //musicName = Path.GetFileName(item).Split((char) UInt16.Parse(@"\"));
                    //Path.get
                    var xItemNew = Path.Combine(xPathNew, $"{n++:000} - {musicName[musicName.Length - 1]}");
                    //File.Copy(item, xItemNew);
                }
            }
        }

        private void BuOpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.FilterIndex = 2;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                tbOpen.Text = @$"{openFileDialog.FileName}";
            }
        }
    }
}
