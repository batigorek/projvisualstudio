﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labTimer
{
    public partial class Fm : Form
    {
        private int SEC_MAX;
        Timer tm = new Timer();
        private DateTime start;
        private DateTime pauseTime;

        public Fm()
        {
            InitializeComponent();

            tm.Tick += TmUp_Tick;
            buUp.Click += BuUp_Click;
            buDown.Click += BuDown_Click;
            buStart.Click += BuStart_Click;
            buStop.Click += BuStop_Click;
            tbTime.ValueChanged += (s, e) => { SEC_MAX = tbTime.Value;
                pbTime.Value = 0;
                pbTime.Maximum = SEC_MAX;
            };
            
            // HW
            // ++time and --time by but
            // % выполнения таймера 
        }

        private void BuStop_Click(object sender, EventArgs e)
        {
            if (tm.Enabled)
            {
                pauseTime = DateTime.Now;
                tm.Stop();
            } else
            {
                start = start + (DateTime.Now - pauseTime);
                //start = DateTime.Now + (start - pauseTime); для обратного отсчета
                tm.Start();
            }
        }

        private void BuStart_Click(object sender, EventArgs e)
        {
            tm.Enabled = !tm.Enabled;
            start = DateTime.Now;
            var x = DateTime.Now - start;
            if (x.TotalSeconds > SEC_MAX)
            {
                tm.Stop();
                x = TimeSpan.FromSeconds(SEC_MAX);
            }
            laTime.Text = x.ToString(@"mm\:ss\:fff");
            pbTime.Value = (int)x.TotalSeconds;
        }


        private void BuDown_Click(object sender, EventArgs e)
        {
            tm.Enabled = !tm.Enabled;
            start = DateTime.Now.AddSeconds(SEC_MAX);
        }

        private void TmDown_Tick(object sender, EventArgs e)
        {
            var x = start - DateTime.Now;
            if (x.Ticks < 0)
            {
                tm.Stop();
                x = TimeSpan.Zero;
            }
            laTime.Text = x.ToString(@"mm\:ss\:fff");
        }

        private void BuUp_Click(object sender, EventArgs e)
        {
            tm.Enabled = !tm.Enabled;
            start = DateTime.Now;
        }

        private void TmUp_Tick(object sender, EventArgs e)
        {
            //2
            //buUp.Text = (DateTime.Now - startUp).ToString();
            
            var x = DateTime.Now - start;
            if (x.TotalSeconds > SEC_MAX)
            {
                tm.Stop();
                x = TimeSpan.FromSeconds(SEC_MAX);
            }
            laTime.Text = x.ToString(@"mm\:ss\:fff");
            pbTime.Value = (int)x.TotalSeconds;
        }

        private void Timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //this.Text += "_";
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            this.Text += "*";
        }

    }
}
