﻿namespace labTimer
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buUp = new System.Windows.Forms.Button();
            this.buDown = new System.Windows.Forms.Button();
            this.tbTime = new System.Windows.Forms.TrackBar();
            this.laTime = new System.Windows.Forms.Label();
            this.buStop = new System.Windows.Forms.Button();
            this.buStart = new System.Windows.Forms.Button();
            this.pbTime = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.tbTime)).BeginInit();
            this.SuspendLayout();
            // 
            // buUp
            // 
            this.buUp.Location = new System.Drawing.Point(12, 12);
            this.buUp.Name = "buUp";
            this.buUp.Size = new System.Drawing.Size(96, 92);
            this.buUp.TabIndex = 0;
            this.buUp.Text = "+";
            this.buUp.UseVisualStyleBackColor = true;
            // 
            // buDown
            // 
            this.buDown.Location = new System.Drawing.Point(12, 121);
            this.buDown.Name = "buDown";
            this.buDown.Size = new System.Drawing.Size(96, 94);
            this.buDown.TabIndex = 1;
            this.buDown.Text = "-";
            this.buDown.UseVisualStyleBackColor = true;
            // 
            // tbTime
            // 
            this.tbTime.Location = new System.Drawing.Point(12, 234);
            this.tbTime.Maximum = 100;
            this.tbTime.Minimum = 10;
            this.tbTime.Name = "tbTime";
            this.tbTime.Size = new System.Drawing.Size(494, 45);
            this.tbTime.TabIndex = 2;
            this.tbTime.Value = 10;
            // 
            // laTime
            // 
            this.laTime.AutoSize = true;
            this.laTime.Font = new System.Drawing.Font("Segoe UI", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laTime.Location = new System.Drawing.Point(155, 84);
            this.laTime.Name = "laTime";
            this.laTime.Size = new System.Drawing.Size(0, 54);
            this.laTime.TabIndex = 3;
            // 
            // buStop
            // 
            this.buStop.Location = new System.Drawing.Point(410, 121);
            this.buStop.Name = "buStop";
            this.buStop.Size = new System.Drawing.Size(96, 94);
            this.buStop.TabIndex = 5;
            this.buStop.Text = "Stop";
            this.buStop.UseVisualStyleBackColor = true;
            // 
            // buStart
            // 
            this.buStart.Location = new System.Drawing.Point(410, 12);
            this.buStart.Name = "buStart";
            this.buStart.Size = new System.Drawing.Size(96, 92);
            this.buStart.TabIndex = 4;
            this.buStart.Text = "Start";
            this.buStart.UseVisualStyleBackColor = true;
            // 
            // pbTime
            // 
            this.pbTime.Location = new System.Drawing.Point(18, 286);
            this.pbTime.Name = "pbTime";
            this.pbTime.Size = new System.Drawing.Size(487, 39);
            this.pbTime.TabIndex = 6;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(519, 339);
            this.Controls.Add(this.pbTime);
            this.Controls.Add(this.buStop);
            this.Controls.Add(this.buStart);
            this.Controls.Add(this.laTime);
            this.Controls.Add(this.tbTime);
            this.Controls.Add(this.buDown);
            this.Controls.Add(this.buUp);
            this.Name = "Fm";
            this.Text = "labTimer";
            ((System.ComponentModel.ISupportInitialize)(this.tbTime)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buUp;
        private System.Windows.Forms.Button buDown;
        private System.Windows.Forms.TrackBar tbTime;
        private System.Windows.Forms.Label laTime;
        private System.Windows.Forms.Button buStop;
        private System.Windows.Forms.Button buStart;
        private System.Windows.Forms.ProgressBar pbTime;
    }
}

