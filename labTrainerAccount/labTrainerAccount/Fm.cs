﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labTrainerAccount
{
    public partial class Fm : Form
    {
        private Game g;

        public Fm()
        {
            InitializeComponent();

            g = new Game();
            g.Change += G_Change;
            g.DoReset();

            buYes.Click += (s, e) => g.DoAnswer(true);
            buNo.Click += (s, e) => g.DoAnswer(false);


            buMinus.Click += (s, e) => g.DoLevel(false);
            buPlus.Click += (s, e) => g.DoLevel(true);
        }

        private void G_Change(object sender, EventArgs e)
        {
            laCode.Text = g.CodeText;
            laCorrect.Text = $"Верно = {g.CountCorrect}";
            laWrong.Text = $"Неверно = {g.CountWrong}";
            laLevel.Text = $"{g.NumLevel}";
        }
    }
}
