﻿using System;
using System.Collections.Generic;
using System.Text;

namespace labTrainerAccount
{
    class Game
    {
        public int CountCorrect { get; private set; }
        public int CountWrong { get; private set; }
        public string CodeText { get; private set; }
        public int NumLevel { get; private set; } = 1;
        public int ToNextLevel { get; private set; } = 0;

        //Random rnd = new Random();

        private bool AnswerCorrect;
        public event EventHandler Change;

        public void DoReset()
        {
            CountCorrect = 0;
            CountWrong = 0;
            DoContinue();

        }

        private void DoContinue()
        {
            //MY METHOD
            //int val1 = rnd.Next(0, 20);
            //int val2 = rnd.Next(0, 20);
            //int res = rnd.Next(val1 + val2 - 1, val1 + val2 + 1);

            //CodeText = $"{val1} + {val2} = {res}";
            //AnswerCorrect = (res == val1 + val2) ? true : false;

            //Change?.Invoke(this, EventArgs.Empty);

            Random rnd = new Random();
            int xValue1 = rnd.Next(10*NumLevel);
            int xValue2 = rnd.Next(10*NumLevel);
                 int xResult;
                 int xResultNew;

            switch(rnd.Next(4))
            {
                case 0:
                    xResult = xValue1 + xValue2;
                    xResultNew = xResult;
                    if (rnd.Next(2) == 1)
                        xResultNew += rnd.Next(1, 7) * (rnd.Next(2) == 1 ? 1 : -1);
                    AnswerCorrect = xResult == xResultNew;
                    CodeText = $"{xValue1} + {xValue2} = {xResultNew}";
                    break;
                case 1:
                    xResult = xValue1 * xValue2;
                    xResultNew = xResult;
                    if (rnd.Next(2) == 1)
                        xResultNew += rnd.Next(1, 7) * (rnd.Next(2) == 1 ? 1 : -1);
                    AnswerCorrect = xResult == xResultNew;
                    CodeText = $"{xValue1} * {xValue2} = {xResultNew}";
                    break; 
                case 2:
                    if (xValue2 == 0)
                        xValue2++;

                    xResult = xValue1 * xValue2;
                    xResultNew = xResult;

                    if (rnd.Next(2) == 1)
                        xResultNew += rnd.Next(1, 7) * (rnd.Next(2) == 1 ? 1 : -1);
                    AnswerCorrect = xResult == xResultNew;
                    CodeText = $"{xResultNew} / {xValue2} = {xValue1}";
                    break;
                case 3:
                    xResult = xValue1 - xValue2;
                    xResultNew = xResult;
                    if (rnd.Next(2) == 1)
                        xResultNew += rnd.Next(1, 7) * (rnd.Next(2) == 1 ? 1 : -1);
                    AnswerCorrect = xResult == xResultNew;
                    CodeText = $"{xValue1} - {xValue2} = {xResultNew}";
                    break;
                default:
                    break;
            }
            


            Change?.Invoke(this, EventArgs.Empty);
        }

        public void DoAnswer (bool v)
        {
            if (v == AnswerCorrect)
            {
                CountCorrect++;
                ToNextLevel++;
            }
            else
            {
                CountWrong++;
                ToNextLevel = 0;
            }

            if (ToNextLevel == 10)
                NumLevel++;
            DoContinue();
        }

        public void DoLevel (bool v)
        {
            if (v == true)
                NumLevel++;
            else if (NumLevel > 1)
                NumLevel--;
            else
                NumLevel = 1;
            DoContinue();
        }
    }
}
