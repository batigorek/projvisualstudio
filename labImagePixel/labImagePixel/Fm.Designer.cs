﻿
namespace labImagePixel
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Fm));
            this.pxImage = new System.Windows.Forms.PictureBox();
            this.pxResult = new System.Windows.Forms.PictureBox();
            this.pxColor = new System.Windows.Forms.PictureBox();
            this.buResultGray = new System.Windows.Forms.Button();
            this.buResultInvert = new System.Windows.Forms.Button();
            this.trDelta = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trDelta)).BeginInit();
            this.SuspendLayout();
            // 
            // pxImage
            // 
            this.pxImage.Image = ((System.Drawing.Image)(resources.GetObject("pxImage.Image")));
            this.pxImage.Location = new System.Drawing.Point(12, 12);
            this.pxImage.Name = "pxImage";
            this.pxImage.Size = new System.Drawing.Size(500, 500);
            this.pxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pxImage.TabIndex = 0;
            this.pxImage.TabStop = false;
            // 
            // pxResult
            // 
            this.pxResult.Location = new System.Drawing.Point(518, 12);
            this.pxResult.Name = "pxResult";
            this.pxResult.Size = new System.Drawing.Size(500, 500);
            this.pxResult.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pxResult.TabIndex = 1;
            this.pxResult.TabStop = false;
            // 
            // pxColor
            // 
            this.pxColor.Location = new System.Drawing.Point(472, 518);
            this.pxColor.Name = "pxColor";
            this.pxColor.Size = new System.Drawing.Size(100, 50);
            this.pxColor.TabIndex = 2;
            this.pxColor.TabStop = false;
            // 
            // buResultGray
            // 
            this.buResultGray.Location = new System.Drawing.Point(16, 521);
            this.buResultGray.Name = "buResultGray";
            this.buResultGray.Size = new System.Drawing.Size(119, 39);
            this.buResultGray.TabIndex = 3;
            this.buResultGray.Text = "Gray";
            this.buResultGray.UseVisualStyleBackColor = true;
            // 
            // buResultInvert
            // 
            this.buResultInvert.Location = new System.Drawing.Point(141, 521);
            this.buResultInvert.Name = "buResultInvert";
            this.buResultInvert.Size = new System.Drawing.Size(119, 39);
            this.buResultInvert.TabIndex = 4;
            this.buResultInvert.Text = "Invertate";
            this.buResultInvert.UseVisualStyleBackColor = true;
            // 
            // trDelta
            // 
            this.trDelta.Location = new System.Drawing.Point(623, 522);
            this.trDelta.Name = "trDelta";
            this.trDelta.Size = new System.Drawing.Size(319, 45);
            this.trDelta.TabIndex = 5;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1047, 579);
            this.Controls.Add(this.trDelta);
            this.Controls.Add(this.buResultInvert);
            this.Controls.Add(this.buResultGray);
            this.Controls.Add(this.pxColor);
            this.Controls.Add(this.pxResult);
            this.Controls.Add(this.pxImage);
            this.Name = "Fm";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trDelta)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pxImage;
        private System.Windows.Forms.PictureBox pxResult;
        private System.Windows.Forms.PictureBox pxColor;
        private System.Windows.Forms.Button buResultGray;
        private System.Windows.Forms.Button buResultInvert;
        private System.Windows.Forms.TrackBar trDelta;
    }
}

