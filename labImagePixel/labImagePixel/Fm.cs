﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImagePixel
{
    public partial class Fm : Form
    {
        private Color selectColor;

        public Fm()
        {
            InitializeComponent();

            pxImage.MouseMove += PxImage_MouseMove;

            buResultGray.Click += BuResultGray_Click;
            buResultInvert.Click += BuResultInvert_Click;

            trDelta.Maximum = 255 * 3;
            trDelta.Value = 20;


            pxImage.Click += (s, e) => pxImage.Image = new Bitmap(Clipboard.GetImage());
            pxResult.Click += (s, e) => Clipboard.SetImage(pxResult.Image);

            // HW
            // obrab image

            //Note: get/set pixel works slowly
        }

        private void BuResultInvert_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = new Bitmap(pxImage.Image);
            for (int i = 0; i < bitmap.Width; i++) //X
                for (int j = 0; j < bitmap.Height; j++) //Y
                {
                    var color = bitmap.GetPixel(i, j);
                    
                    bitmap.SetPixel(i, j, Color.FromArgb(255 - color.R, 255 - color.G, 255 - color.B));
                }
            pxResult.Image = new Bitmap(bitmap);
        }

        private void BuResultGray_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = new Bitmap(pxImage.Image);
            for (int i = 0; i < bitmap.Width; i++) //X
                for (int j = 0; j < bitmap.Height; j++) //Y
                {
                    var color = bitmap.GetPixel(i, j);
                    var gray = (color.R + color.G + color.B) / 3;
                    bitmap.SetPixel(i, j, Color.FromArgb(gray, gray, gray));
                }
            pxResult.Image = new Bitmap(bitmap);
        }

        private void PxImage_MouseMove(object sender, MouseEventArgs e)
        {
            var bitmap = new Bitmap(pxImage.Image);
            selectColor = bitmap.GetPixel(e.X, e.Y);
            pxColor.BackColor = selectColor;

            for (int i = 0; i < bitmap.Width; i++)
                for (int j = 0; j < bitmap.Height; j++)
                {
                    var color = bitmap.GetPixel(i, j);
                    //bitmap.SetPixel(i, j, (selectColor == color) ? color : Color.Transparent);

                    var v =
                        (Math.Max(selectColor.R, color.R) - Math.Min(selectColor.R, color.R)) +
                        (Math.Max(selectColor.G, color.G) - Math.Min(selectColor.G, color.G)) +
                        (Math.Max(selectColor.B, color.B) - Math.Min(selectColor.B, color.B));

                    bitmap.SetPixel(i, j, v < trDelta.Value ? color : Color.Transparent);
                }
            pxResult.Image = new Bitmap(bitmap);

            this.Text = $"{Application.ProductName} : {selectColor} : {trDelta.Value}";
        }
    }
}
