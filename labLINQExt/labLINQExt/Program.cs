﻿using System;
using System.Linq;

namespace labLINQExt
{
    class Program
    {
        static void Main(string[] args)
        {
            var arr = new string[] { "Sochi", "Moscow", "Orel", "Samara", "Tula", "Yalta", "Sum" };

            //Console.WriteLine(string.Join(" ", arr));
            Console.WriteLine(string.Join(Environment.NewLine, arr));

            //var x1 = arr.Where(v => v.StartsWith("S") && v.Length > 4 ).ToArray();
            var x1 = arr.Where(v => v.StartsWith("S")).OrderBy(v =>v).ToArray();
            Console.WriteLine(string.Join(Environment.NewLine,x1));

            var x2 = arr.OrderBy(v => v).Select(v => $"[{v}] - {v.Contains('a')}").ToArray();

            Console.WriteLine(string.Join(Environment.NewLine,x2));
        }
    }
}
