﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labControlPuzzle
{
    public partial class Fm : Form
    {
        private const int HOLD_PIXEL = 25;
        private PictureBox[,] pics;
        private int cellWidth;
        private int cellHeight;
        private Point startMouseDown;

        public int Rows { get; private set; } = 5;
        public int Cols { get; private set; } = 3;

        public Bitmap x;

        public Fm()
        {
            InitializeComponent();

            x = new Bitmap(Properties.Resources.BTS,new Size(this.ClientSize.Width, this.ClientSize.Height - panel1.Height));
            CreateCells();
            ResizeCells();
            StartLocationCells();
            this.KeyDown += Fm_KeyDown;
            this.Text += " : (F1 - собрать, F2 - перемешать, F3 - , F4 - )";

            buShake.Click += BuShake_Click;
            buCollect.Click += (s, e) => { StartLocationCells(); };
            buSelect.Click += BuSelect_Click;
            this.MouseMove += Fm_MouseMove;
            this.MouseDown += Fm_MouseDown;

            //new Size(this.ClientSize.Width, this.ClientSize.Height - panel1.Height);

            // HW 
            // выделение нескольких компонентов и совместное перемещение (с клавишей контрл) и визуально наблюдать какие компоненты выделены
            // проверка на победу
            // если плитка на своем месте - блокировать или менят цвет
        }

        private void Fm_MouseDown(object sender, MouseEventArgs e)
        {
           //TODO
        }

        private void Fm_MouseMove(object sender, MouseEventArgs e)
        {
            //TODO
        }

        private void BuSelect_Click(object sender, EventArgs e)
        {
            x.Dispose();
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j].Dispose();
                }

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "img files (*.img)|*.img|All files (*.*)|*.*";
            openFileDialog.FilterIndex = 2;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                Bitmap b1 = new Bitmap(@$"{openFileDialog.FileName}");
                
                x = new Bitmap(b1, new Size(this.ClientSize.Width, this.ClientSize.Height - panel1.Height));
                CreateCells();
                ResizeCells();
                StartLocationCells();
            }
        }

        private void BuShake_Click(object sender, EventArgs e)
        {
            RandomCells();
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
           switch (e.KeyCode)
            {
                case Keys.F1:
                    StartLocationCells();
                    break;
                case Keys.F2:
                    RandomCells();
                    break;
                case Keys.F3:
                    Random2Cells();
                    break;
                case Keys.F4:
                    ResizeCells();
                    break;
                case Keys.ControlKey:
                    MessageBox.Show("Works");
                    break;
            }
        }

        private void Random2Cells()
        {
            StartLocationCells();
            Random rnd = new Random();
            for (int i = 0; i < 15; i++)
            {
                var x1 = rnd.Next(Rows);
                var y1 = rnd.Next(Cols);
                
                var x2 = rnd.Next(Rows);
                var y2 = rnd.Next(Cols);

                var pointTemp = pics[x1, y1].Location;
                pics[x1, y1].Location = pics[x2, y2].Location;
                pics[x2, y2].Location = pointTemp;
            }
        }

        private void RandomCells()
        {
            Random rnd = new Random();
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j].Location = new Point(
                        rnd.Next(this.ClientSize.Width-pics[i,j].Width),
                        rnd.Next(this.ClientSize.Height - panel1.Height - pics[i, j].Height));
                }
        }

        private void StartLocationCells()
        {
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j].Location = new Point(j * cellWidth, i * cellHeight + panel1.Height);
                }
        }

        private void ResizeCells()
        {
            cellWidth = x.Width / Cols;
            cellHeight = x.Height / Rows;
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j].Width = cellWidth;
                    pics[i, j].Height = cellHeight;
                    pics[i, j].Tag = (i, j);
                    pics[i, j].Image = new Bitmap(cellWidth, cellHeight);
                    var g = Graphics.FromImage(pics[i, j].Image);
                    g.Clear(DefaultBackColor);
                    g.DrawImage(x,
                        new Rectangle(0, 0, pics[i,j].Width,pics[i,j].Height),
                        new Rectangle(j*cellWidth, i*cellHeight + panel1.Height, cellWidth, cellHeight),
                        GraphicsUnit.Pixel);
                    g.DrawString($"[{i},{j}]",
                        new Font("", 10, FontStyle.Bold),
                        new SolidBrush(Color.Black),
                        new Point(0, 0));
                    g.Dispose();
                }
            }
        }

        private void CreateCells()
        {
            pics = new PictureBox[Rows, Cols];

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    pics[i,j] = new PictureBox();
                    pics[i, j].BorderStyle = BorderStyle.FixedSingle;
                    pics[i, j].MouseDown += PictureBoxAll_MouseDown;
                    pics[i, j].MouseMove += PictureBoxAll_MouseMove;
                    pics[i, j].MouseUp += PictureBoxAll_MouseUp;
                    this.Controls.Add(pics[i, j]);
                }
            }
        }

        private void PictureBoxAll_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (sender is Control x)
                {
                    var xLocation = x.Location;
                    for (int i = 0; i < Rows; i++)
                    {
                        for (int j = 0; j < Cols; j++)
                        {
                            if (x.Location.X > j * cellWidth - HOLD_PIXEL && x.Location.X < j * cellWidth + HOLD_PIXEL)
                                xLocation.X = j * cellWidth;
                            if (x.Location.Y > i * cellHeight - HOLD_PIXEL && x.Location.Y < i * cellHeight + HOLD_PIXEL)
                                xLocation.Y = i * cellHeight;
                               //var g = Graphics.FromImage(pics[i, j].BackgroundImage);
                               //g.Dispose();
                        }
                    }
                    x.Location = xLocation;

                    //(int r, int c) = ((int),(int))x.Tag; mistake
                    //var xColor = (r * cellWidth == x.Location.X && c * cellHeight == x.Location.Y) ? Color.Green : Color.Red;
                    //g.DrawRectangle(new Pen(xColor, 1),
                    //    new Rectangle(0, 0, x.Width - 1, x.Height - 1);

                    CheckWin();
                }
            }
        }

        private void CheckWin()
        {
            bool isWin = true;
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    if (j*cellWidth != pics[i,j].Location.X || i*cellHeight != pics[i,j].Location.Y)
                    {
                        isWin = false;
                    }
                }
            }
            //(isWin == true) ? MessageBox.Show("Win") : MessageBox.Show("Win");
        }

        private void PictureBoxAll_MouseMove(object sender, MouseEventArgs e)
        {
            if (sender is Control x)
            {
                if (e.Button == MouseButtons.Left)
                {
                    x.Location = new Point(
                        x.Location.X + e.X - startMouseDown.X,
                        x.Location.Y + e.Y - startMouseDown.Y);

                    var xLocation = x.Location;
                    for (int i = 0; i < Rows; i++)
                    {
                        for (int j = 0; j < Cols; j++)
                        {
                            if (x.Location.X > j * cellWidth - HOLD_PIXEL && x.Location.X < j * cellWidth + HOLD_PIXEL)
                                xLocation.X = j * cellWidth;
                            if (x.Location.Y > i * cellHeight - HOLD_PIXEL && x.Location.Y < i * cellHeight + HOLD_PIXEL)
                                xLocation.Y = i * cellHeight;
                        }
                    }
                    x.Location = xLocation;
                }
                //(int r, int c) = ((int),(int))x.Tag;
                //Text = $"{r}:{c}";
            }
        }

        private void PictureBoxAll_MouseDown(object sender, MouseEventArgs e)
        {
            startMouseDown = e.Location;
            if (sender is Control x)
            {
                x.BringToFront();
            }
        }
    }
}
