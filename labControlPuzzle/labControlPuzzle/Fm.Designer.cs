﻿
namespace labControlPuzzle
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buCollect = new System.Windows.Forms.Button();
            this.buShake = new System.Windows.Forms.Button();
            this.buSelect = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.buCollect);
            this.panel1.Controls.Add(this.buShake);
            this.panel1.Controls.Add(this.buSelect);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 52);
            this.panel1.TabIndex = 0;
            // 
            // buCollect
            // 
            this.buCollect.Location = new System.Drawing.Point(600, 8);
            this.buCollect.Name = "buCollect";
            this.buCollect.Size = new System.Drawing.Size(187, 36);
            this.buCollect.TabIndex = 2;
            this.buCollect.Text = "Собрать";
            this.buCollect.UseVisualStyleBackColor = true;
            // 
            // buShake
            // 
            this.buShake.Location = new System.Drawing.Point(306, 8);
            this.buShake.Name = "buShake";
            this.buShake.Size = new System.Drawing.Size(187, 36);
            this.buShake.TabIndex = 1;
            this.buShake.Text = "Перемешать";
            this.buShake.UseVisualStyleBackColor = true;
            // 
            // buSelect
            // 
            this.buSelect.Location = new System.Drawing.Point(7, 8);
            this.buSelect.Name = "buSelect";
            this.buSelect.Size = new System.Drawing.Size(187, 36);
            this.buSelect.TabIndex = 0;
            this.buSelect.Text = "Выбрать изображение";
            this.buSelect.UseVisualStyleBackColor = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.Name = "Fm";
            this.Text = "labControlPuzzle";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buCollect;
        private System.Windows.Forms.Button buShake;
        private System.Windows.Forms.Button buSelect;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

