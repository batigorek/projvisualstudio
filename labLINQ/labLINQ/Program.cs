﻿using System;
using System.Linq;

namespace labLINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            //1
            //var arr = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var arr = Enumerable.Range(0, 10).ToArray();

            var myQuery =
                from v in arr
                where v> 1&& v< 7
                orderby v descending
                select v * 2;

            Console.WriteLine(string.Join(" ", arr));
            Console.WriteLine(string.Join(" ", myQuery));
            Console.WriteLine($"Count={myQuery.Count()}, Sum={myQuery.Sum()}");


            //2
            var arr2 = new string[] { "Name, Name2, Name3, Name4" };

            var myQuery2 =
                from v in arr2
                where v.ToUpper().StartsWith("N")
                orderby v
                select v;

            Console.WriteLine(string.Join(" ", myQuery2));
        }
    }
}
