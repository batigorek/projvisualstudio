﻿using System;
using System.Collections.Generic;

namespace labDictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, string> x = new Dictionary<int, string>();
            x.Add(10, "City");
            x.Add(20, "City New York");
            x.Add(30, "City London");
            x.Add(40, "City Moscow");
            x.Add(50, "City Boston");
            x.Add(60, "City Cena");

            x[60] = "Cheboksary";
            x.Remove(40);
            Console.WriteLine(x[50]);
            Console.WriteLine("________");

            foreach (KeyValuePair<int, string> v in x)
            {
                Console.WriteLine($"Key = {v.Key}, Value = {v.Value}");
            }
        }
    }
}
