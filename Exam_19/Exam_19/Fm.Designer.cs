﻿
namespace Exam_19
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buStartStop = new System.Windows.Forms.Button();
            this.laTimer = new System.Windows.Forms.Label();
            this.laHard = new System.Windows.Forms.Label();
            this.buMinus = new System.Windows.Forms.Button();
            this.buPlus = new System.Windows.Forms.Button();
            this.laMistake = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buStartStop
            // 
            this.buStartStop.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buStartStop.Location = new System.Drawing.Point(15, 363);
            this.buStartStop.Name = "buStartStop";
            this.buStartStop.Size = new System.Drawing.Size(295, 75);
            this.buStartStop.TabIndex = 0;
            this.buStartStop.Text = "Начать/остановить игру";
            this.buStartStop.UseVisualStyleBackColor = true;
            // 
            // laTimer
            // 
            this.laTimer.AutoSize = true;
            this.laTimer.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laTimer.Location = new System.Drawing.Point(15, 44);
            this.laTimer.Name = "laTimer";
            this.laTimer.Size = new System.Drawing.Size(295, 30);
            this.laTimer.TabIndex = 1;
            this.laTimer.Text = "Время до конца игры: 00:00";
            // 
            // laHard
            // 
            this.laHard.AutoSize = true;
            this.laHard.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laHard.Location = new System.Drawing.Point(34, 210);
            this.laHard.Name = "laHard";
            this.laHard.Size = new System.Drawing.Size(237, 30);
            this.laHard.TabIndex = 2;
            this.laHard.Text = "Уровень сложности: 1";
            // 
            // buMinus
            // 
            this.buMinus.Location = new System.Drawing.Point(62, 243);
            this.buMinus.Name = "buMinus";
            this.buMinus.Size = new System.Drawing.Size(59, 44);
            this.buMinus.TabIndex = 3;
            this.buMinus.Text = "-";
            this.buMinus.UseVisualStyleBackColor = true;
            // 
            // buPlus
            // 
            this.buPlus.Location = new System.Drawing.Point(175, 243);
            this.buPlus.Name = "buPlus";
            this.buPlus.Size = new System.Drawing.Size(59, 44);
            this.buPlus.TabIndex = 4;
            this.buPlus.Text = "+";
            this.buPlus.UseVisualStyleBackColor = true;
            // 
            // laMistake
            // 
            this.laMistake.AutoSize = true;
            this.laMistake.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.laMistake.Location = new System.Drawing.Point(86, 131);
            this.laMistake.Name = "laMistake";
            this.laMistake.Size = new System.Drawing.Size(121, 30);
            this.laMistake.TabIndex = 5;
            this.laMistake.Text = "Ошибок: 0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.laMistake);
            this.Controls.Add(this.buPlus);
            this.Controls.Add(this.buMinus);
            this.Controls.Add(this.laHard);
            this.Controls.Add(this.laTimer);
            this.Controls.Add(this.buStartStop);
            this.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Name = "Form1";
            this.Text = "Exam_19";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buStartStop;
        private System.Windows.Forms.Label laTimer;
        private System.Windows.Forms.Label laHard;
        private System.Windows.Forms.Button buMinus;
        private System.Windows.Forms.Button buPlus;
        private System.Windows.Forms.Label laMistake;
    }
}

