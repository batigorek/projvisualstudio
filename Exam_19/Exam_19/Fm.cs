﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exam_19
{
    public partial class Form1 : Form
    {
        private Play play = new Play();
        private bool flag = true;
        public const int COUNT = 5;
        private Button[,] arrBu = new Button[COUNT, COUNT];
        private DateTime startDown;
        private const int SEC_MAX = 30;
        Timer tmDown = new Timer();


        public Form1()
        {
            InitializeComponent();

            buStartStop.Click += BuStartStop_Click;
            buMinus.Click += (s,e) => { if (play.hardLevel > 1) play.hardLevel--; laHard.Text = $"Уровень сложности: {play.hardLevel}"; };
            buPlus.Click += (s, e) => { if (play.hardLevel < 5) play.hardLevel++; laHard.Text = $"Уровень сложности: {play.hardLevel}"; };
        }

        private void BuStartStop_Click(object sender, EventArgs e)
        {
            if (flag == false)
            {
                flag = true;
                for (int i = 0; i < COUNT; i++)
                {
                    for (int j = 0; j < COUNT; j++)
                    {
                        arrBu[i, j].Dispose();
                    }
                }
                tmDown.Stop();
            }
            else
            {
                Random rnd = new Random();

                for (int i = 0; i < COUNT; i++)
                {
                    for (int j = 0; j < COUNT; j++)
                    {
                        Button btn = new Button();
                        arrBu[i, j] = btn;
                        btn.Size = new Size(400/(COUNT + play.hardLevel), 400 / (COUNT + play.hardLevel));
                        play.Numeration();
                        btn.Text = play.arr[i, j].ToString();
                        btn.Location = new Point(400 + j * btn.Size.Width, 50 + i * btn.Size.Height);
                        btn.Click += Btn_Click;
                        if (rnd.Next(0,2) == 0)
                        {
                            btn.ForeColor = Color.Red;
                            btn.Font = new Font(this.Font, FontStyle.Bold);
                        } else
                        {
                            btn.ForeColor = Color.Black;
                            btn.Font = new Font(this.Font, FontStyle.Italic);
                        }

                        this.Controls.Add(btn);
                    }
                }
                play.Start();
                flag = false;
                startDown = DateTime.Now;
                tmDown.Enabled = !tmDown.Enabled;
                tmDown.Tick += TmUp_Tick;
            }
        }
        private void TmUp_Tick(object sender, EventArgs e)
        {
            var x = startDown.AddSeconds(SEC_MAX) - DateTime.Now;
            if (x.Ticks < 0)
            {
                tmDown.Stop();
                x = TimeSpan.Zero;
            }
            laTimer.Text = "Время до конца игры: " + x.ToString(@"mm\:ss");
        }


        private void Btn_Click(object sender, EventArgs e)
        {
            var x = (Button)sender;
            play.Check(x.Text);
            laMistake.Text = $"Ошибок: {play.mistakes}";
        }
    }
}
