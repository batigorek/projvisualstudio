﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Exam_19
{
    class Play
    {
        public int hardLevel = 1, mistakes = 0;
        public int[,] arr = new int[5,5];
        private int prev = 0;


        public void Start()
        {
            //TODO
        }

        internal void Numeration()
        {
            Random rnd = new Random();
            int number = 1;

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    arr[i, j] = number++;
                }
            }

            var btn1 = rnd.Next(0, 5);
            var btn2 = rnd.Next(0, 5);
            while (btn1 == btn2)
                btn2 = rnd.Next(0, 5);

            for (int i = 0; i < 5; i++)
            {
                int temp = arr[i, btn2];
                int temp1 = arr[i, btn1];
                arr[i, btn1] = temp;
                arr[i, btn2] = temp1;

                int temp2 = arr[btn2, i];
                int temp3 = arr[btn1, i];
                arr[btn1, i] = temp2;
                arr[btn2, i] = temp3;
            }
        }

        internal void Check(string x)
        {
            if (prev == 0 && x == "1")
            {
                prev = Int32.Parse(x);
            }
            else if (x == $"{++prev}")
            {
                prev = Int32.Parse(x);
            }
            else
            {
                ++mistakes;
            }
        }
    }
}
