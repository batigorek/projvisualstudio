﻿using System;
using System.ComponentModel.DataAnnotations;

namespace labTuple
{
    class Program
    {
        static void Main(string[] args)
        {
            var x1 = (2, 4);


            //(1)
            (int, int) x11 = (2, 4);

            Console.WriteLine(x1.Item1);
            Console.WriteLine(x1.Item2);

            //(2)
            (int min, int max) x2 = (2, 4);
            Console.WriteLine(x2.min);
            Console.WriteLine(x2.max);

            //(3)
            var x3 = (min: 2, max: 4);
            Console.WriteLine(x3.min);
            Console.WriteLine(x3.max);

            //(4)
            var (min, max) = (2, 4);
            Console.WriteLine(min);
            Console.WriteLine(max);

            //(5)
            var x5 = GetX5();
            Console.WriteLine(x5.Item1);
            Console.WriteLine(x5.Item2);

            //(6)
            var x6 = GetX6();
            Console.WriteLine(x6.min);
            Console.WriteLine(x6.max);

            //(7)
            var x7 = GetX7((4, 5), 6);
            Console.WriteLine(x7.Item1);
            Console.WriteLine(x7.Item2);
        }

        private static (int, int) GetX7((int, int) p, int v) => (p.Item1 + p.Item2, v);

        private static (int, int) GetX5() => (3, 6);
        private static (int min, int max) GetX6() => (3, 6);

        //private static (int, int) GetX5()
        //{
        //    return (3, 6);
        //}
    }
}
