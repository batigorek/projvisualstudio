﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labControlCreate
{
    public partial class Fm : Form
    {
        public Fm()
        {
            InitializeComponent();

            this.MouseDown += Fm_MouseDown;
        }

        private void Fm_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                //Label x = new Label();
                var x = new Label();
                x.Location = e.Location;
                x.Text = $"({e.Location.X}, {e.Y})";
                x.BackColor = Color.LightPink;
                x.AutoSize = true;
                this.Controls.Add(x);
            }
            if (e.Button == MouseButtons.Right)
            {
                var rnd = new Random(); 
                for (int i = 0; i < 13; i++)
                {
                    var x = new Label();
                    x.Location = new Point(rnd.Next(this.ClientSize.Width),rnd.Next(this.ClientSize.Height));
                    x.Text = $"({e.Location.X}, {e.Location.Y})";
                    x.BackColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
                    x.AutoSize = true;
                    this.Controls.Add(x);
                }
            }
            if(e.Button == MouseButtons.Middle)
            {
                this.Controls.Clear();
            }
        }
    }
}
