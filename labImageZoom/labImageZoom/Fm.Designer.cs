﻿namespace labImageZoom
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Fm));
            this.pxImage = new System.Windows.Forms.PictureBox();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.pxZoom = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.laZoom = new System.Windows.Forms.Label();
            this.buUpload = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxZoom)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pxImage
            // 
            this.pxImage.Dock = System.Windows.Forms.DockStyle.Right;
            this.pxImage.Image = ((System.Drawing.Image)(resources.GetObject("pxImage.Image")));
            this.pxImage.Location = new System.Drawing.Point(217, 0);
            this.pxImage.Name = "pxImage";
            this.pxImage.Size = new System.Drawing.Size(758, 470);
            this.pxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pxImage.TabIndex = 0;
            this.pxImage.TabStop = false;
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // pxZoom
            // 
            this.pxZoom.Dock = System.Windows.Forms.DockStyle.Top;
            this.pxZoom.Location = new System.Drawing.Point(0, 0);
            this.pxZoom.Name = "pxZoom";
            this.pxZoom.Size = new System.Drawing.Size(216, 219);
            this.pxZoom.TabIndex = 1;
            this.pxZoom.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.laZoom);
            this.panel1.Controls.Add(this.buUpload);
            this.panel1.Controls.Add(this.pxZoom);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(216, 470);
            this.panel1.TabIndex = 2;
            // 
            // laZoom
            // 
            this.laZoom.AutoSize = true;
            this.laZoom.Location = new System.Drawing.Point(31, 264);
            this.laZoom.Name = "laZoom";
            this.laZoom.Size = new System.Drawing.Size(38, 15);
            this.laZoom.TabIndex = 3;
            this.laZoom.Text = "label1";
            // 
            // buUpload
            // 
            this.buUpload.Location = new System.Drawing.Point(31, 419);
            this.buUpload.Name = "buUpload";
            this.buUpload.Size = new System.Drawing.Size(149, 39);
            this.buUpload.TabIndex = 2;
            this.buUpload.Text = "Upload picture";
            this.buUpload.UseVisualStyleBackColor = true;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 470);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pxImage);
            this.Name = "Fm";
            this.Text = "labImageZom";
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxZoom)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pxImage;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.PictureBox pxZoom;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buUpload;
        private System.Windows.Forms.Label laZoom;
    }
}

