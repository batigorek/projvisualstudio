﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageZoom
{
    public partial class Fm : Form
    {
        private Point startPoint;

        public int ZoomDelta { get; private set; } = 50;

        public Fm()
        {
            InitializeComponent();

            pxImage.MouseMove += PxImage_MouseMove;
            //pxImage.MouseWheel += PxImage_MouseWheel;
            //pxImage.MouseDown += PxImage_MouseDown;
            pxZoom.Paint += PxZoom_Paint;

            buUpload.Click += BuUpload_Click;

            // HW
            // исправить ошибку при работе зума, когда вытянутая форма
            // ...

        }

        private void BuUpload_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                pxImage.Image = Image.FromFile(dialog.FileName);
                pxImage.Invalidate();
            }
        }

        private void PxImage_MouseWheel(object sender, MouseEventArgs e)
        {
            if ((e.Delta > 0)&&(e.Delta < 50))
                ZoomDelta -= 2;
            else
                ZoomDelta += 2;
            //ZoomDelta += e.Delta > 0 ? -2 : +2;
            pxZoom.Invalidate();
        }

        private void PxZoom_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(pxImage.Image,
                new Rectangle(0, 0, pxZoom.Width, pxZoom.Height),
                // new Rectangle(startPoint.X-50, startPoint.Y-50,100,100),
                new Rectangle(startPoint.X - ZoomDelta, startPoint.Y - ZoomDelta, ZoomDelta * 2, ZoomDelta * 2),
                GraphicsUnit.Pixel);
            laZoom.Text = $"Текущий уровень зума: {ZoomDelta}";
        }

        private void PxImage_MouseMove(object sender, MouseEventArgs e)
        {
            this.Text = $"{Application.ProductName}: ({e.X},{e.Y})";
            if (pxImage.SizeMode == PictureBoxSizeMode.Zoom)
            {
                //ОШИБКА
                startPoint.X = e.X * pxImage.Image.Width / pxImage.Width;
                startPoint.Y = e.Y * pxImage.Image.Height / pxImage.Height;
            }
            else
            {
                startPoint = e.Location;
            }
            pxZoom.Invalidate();

        }
    }
}
