﻿
namespace labFileExplorer
{
    partial class FmFileProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.laName = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.laExtension = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.laLength = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.laCreationTime = new System.Windows.Forms.Label();
            this.laLastWriteTime = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "LastWriteTime";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name";
            // 
            // laName
            // 
            this.laName.AutoSize = true;
            this.laName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.laName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.laName.Location = new System.Drawing.Point(146, 0);
            this.laName.Name = "laName";
            this.laName.Size = new System.Drawing.Size(282, 17);
            this.laName.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Extension";
            // 
            // laExtension
            // 
            this.laExtension.AutoSize = true;
            this.laExtension.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.laExtension.Dock = System.Windows.Forms.DockStyle.Fill;
            this.laExtension.Location = new System.Drawing.Point(146, 17);
            this.laExtension.Name = "laExtension";
            this.laExtension.Size = new System.Drawing.Size(282, 17);
            this.laExtension.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(3, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(137, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Length";
            // 
            // laLength
            // 
            this.laLength.AutoSize = true;
            this.laLength.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.laLength.Dock = System.Windows.Forms.DockStyle.Fill;
            this.laLength.Location = new System.Drawing.Point(146, 34);
            this.laLength.Name = "laLength";
            this.laLength.Size = new System.Drawing.Size(282, 17);
            this.laLength.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(3, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(137, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "CreationTime";
            // 
            // laCreationTime
            // 
            this.laCreationTime.AutoSize = true;
            this.laCreationTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.laCreationTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.laCreationTime.Location = new System.Drawing.Point(146, 51);
            this.laCreationTime.Name = "laCreationTime";
            this.laCreationTime.Size = new System.Drawing.Size(282, 17);
            this.laCreationTime.TabIndex = 8;
            // 
            // laLastWriteTime
            // 
            this.laLastWriteTime.AutoSize = true;
            this.laLastWriteTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.laLastWriteTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.laLastWriteTime.Location = new System.Drawing.Point(146, 68);
            this.laLastWriteTime.Name = "laLastWriteTime";
            this.laLastWriteTime.Size = new System.Drawing.Size(282, 20);
            this.laLastWriteTime.TabIndex = 9;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66667F));
            this.tableLayoutPanel1.Controls.Add(this.laLastWriteTime, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.laCreationTime, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.laLength, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.laExtension, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.laName, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(431, 88);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // FmFileProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(431, 88);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "FmFileProperties";
            this.Text = "FmFileProperties";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label laName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label laExtension;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label laLength;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label laCreationTime;
        private System.Windows.Forms.Label laLastWriteTime;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}