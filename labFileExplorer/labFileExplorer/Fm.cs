﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labFileExplorer
{
    public partial class Fm : Form
    {
        public string CurDir { get; private set; }
        public string SelItem { get; private set; }
        public string PrivItem { get; private set; }
        public bool flag = true;

        public Fm()
        {
            InitializeComponent();

            //CurDir = "D:/";
            CurDir = Directory.GetCurrentDirectory();

            buBack.Click += BuBack_Click;

            buUp.Click += (s, e) => { LoadDir(Directory.GetParent(CurDir).ToString()); };
            edDir.Text = CurDir;

            buForward.Click += BuBack_Click;
            edDir.KeyDown += EdDir_KeyDown;
            buDirSelect.Click += BuDirSelect_Click;
            lv.KeyDown += Fm_KeyDown;

            buCopyFileNamesToClipboard.Click += BuCopyFileNamesToClipboard_Click;

            tv.DoubleClick += Tv_DoubleClick;
            tv.Click += Tv_Click;
            
            lv.ItemSelectionChanged += Lv_ItemSelectionChanged;
            lv.DoubleClick += (s, e) => LoadDir(SelItem);

            //lv.ColumnClick +=

            lv.ContextMenuStrip = contextMenuStrip1;
            miPopupOpen.Click += (s, e) => LoadDir(SelItem);
            miPopupDelete.Click += MiPopupDelete_Click;
            miPopupProperty.Click += MiPopupProperty_Click;
            
            miPopupCreateFile.Click += MiPopupCreateFile_Click;
            miPopupCreateFolder.Click += MiPopupCreateFolder_Click;

            miViewLargeIcon.Click += (s, e) => lv.View = View.LargeIcon;
            miViewSmallIcon.Click += (s, e) => lv.View = View.SmallIcon;
            miViewList.Click += (s, e) => lv.View = View.List;
            miViewDetails.Click += (s, e) => lv.View = View.Details;
            miViewTile.Click += (s, e) => lv.View = View.Tile;

            //var c1 = new ColumnHeader();
            //c1.Text = "Name";
            //c1.Width = 350;
            //lv.Columns.Add(c1);

            //lv.Columns.Add(new ColumnHeader() { Text = "Name", Width = 350 });

            lv.Columns.Add("Name", 350);
            lv.Columns.Add("Changed", 150);
            lv.Columns.Add("Type", 100);
            lv.Columns.Add("Size", 150);

            LoadDir(CurDir);

            Text += ": Drivers = " + string.Join(" ", Directory.GetLogicalDrives());


            foreach (var item in Directory.GetLogicalDrives())
            {
                tv.Nodes.Add("",item,0);

            }

            // HW
            // check on buUp
            // opening files
            // add mech on buBack and buForward from stack
            // buttons change disk (with panel) 
            // space.click count size of folder and show in "size"
            // size of files show in format 00 000 000 (byte kb mb)
            // buttons showing size of file: auto, byte, kb, mb...
            // add new folder (by button)
            // button to show hidden folders
            // button show: only folders/files/all
            // add panel with detail info about folder/file + on/off panel
            // favourite folders (with list) add/delete/sort...
            // sorting by methods
            // spec symbols

            // delete folder
            // open file
            // add smth to contex menu
            // by button del delete file/folder with warning
            //fmfileproperties todo
            // statusbar todo
            // save names of file to buffer by button
            // clipboard

            // HW 4
            // add button navigation - show/hide  component treeview
            // showing data in treeview
            // add icons to tree view (dvd, empty and hiden folders
            // contex menu
            // loadd folder to tv

        }

        private void Tv_Click(object sender, EventArgs e)
        {
            LoadDir(tv.SelectedNode.FullPath);
        }

        private void Tv_DoubleClick(object sender, EventArgs e)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(tv.SelectedNode.FullPath);

            tv.BeginUpdate();
            tv.SelectedNode.Nodes.Clear();
            foreach (var item in directoryInfo.GetDirectories())
            {
                tv.SelectedNode.Nodes.Add("",item.Name,2); //todo
            }
            tv.SelectedNode.Expand();
            tv.EndUpdate();
            LoadDir(tv.SelectedNode.FullPath);
        }

        private void MiPopupCreateFolder_Click(object sender, EventArgs e)
        {
           //TODO
        }

        private void MiPopupCreateFile_Click(object sender, EventArgs e)
        {
          //TODO
        }


        private void BuCopyFileNamesToClipboard_Click(object sender, EventArgs e)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(CurDir);

            string st = "";
            foreach (var item in directoryInfo.GetDirectories())
            {
                st += item.Name + Environment.NewLine;
            }
            foreach (var item in directoryInfo.GetFiles())
            {
                st += item.Name + Environment.NewLine;
            }

            Clipboard.SetText(st);
        }

        private void MiPopupProperty_Click(object sender, EventArgs e)
        {
            //DirectoryInfo directoryInfo = new DirectoryInfo(CurDir);
            var x = new FmFileProperties(SelItem);
            x.ShowDialog();
        }

        private void MiPopupDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Delete?", Application.ProductName,
                MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
            {
                MessageBox.Show($"File {SelItem} has been deleted");
                File.Delete(SelItem);
                //TODO folder delete
            }
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back)
            {
                LoadDir(Directory.GetParent(CurDir).ToString());
            }
        }

        private void BuBack_Click(object sender, EventArgs e)
        {
            flag = true;
            LoadDir(PrivItem);
            flag = false;
        }

        private void Lv_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            SelItem = Path.Combine(CurDir, e.Item.Text);
            laStatus.Text = $"Elements: {lv.Items.Count}, Chosen: {lv.SelectedItems.Count}, (Size //TODO)";
        }

        private void BuDirSelect_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
                LoadDir(dialog.SelectedPath);
        }

        private void EdDir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                if (edDir.Text != "")
                    LoadDir(edDir.Text);
                else LoadDir(CurDir);
        }

        private void LoadDir(string newDir)
        {

            lv.BeginUpdate();
            lv.Items.Clear();

            DirectoryInfo directoryInfo = new DirectoryInfo(newDir);
            //MessageBox.Show(directoryInfo.ToString());
            if (flag == true)
            {
                PrivItem = directoryInfo.ToString();
                flag = false;
            }
            else flag = true;

            if (directoryInfo.Extension != ".*")
            {
                foreach (var item in directoryInfo.GetDirectories())
                {
                    //lv.Items.Add(item.Name, 1);
                    lv.Items.Add(new ListViewItem(
                        new string[] { item.Name, item.LastWriteTime.ToString(), "Folder", "" }, 1));
                }

                foreach (var item in directoryInfo.GetFiles())
                {
                    //lv.Items.Add(item.Name, 0);

                    switch (item.Extension.ToString())
                    {
                        case ".dll":
                            lv.Items.Add(new ListViewItem(
                        new string[] { item.Name, item.LastWriteTime.ToString(), "File", item.Length.ToString() + "byte" }, 2));
                            break;
                        case ".exe":
                            lv.Items.Add(new ListViewItem(
                        new string[] { item.Name, item.LastWriteTime.ToString(), "File", item.Length.ToString() + "byte" }, 3));
                            break;
                        case ".jpg":
                            lv.Items.Add(new ListViewItem(
                        new string[] { item.Name, item.LastWriteTime.ToString(), "File", item.Length.ToString() + "byte" }, 4));
                            break;
                        case ".png":
                            lv.Items.Add(new ListViewItem(
                        new string[] { item.Name, item.LastWriteTime.ToString(), "File", item.Length.ToString() + "byte" }, 5));
                            break;
                        case ".docx":
                            lv.Items.Add(new ListViewItem(
                        new string[] { item.Name, item.LastWriteTime.ToString(), "File", item.Length.ToString() + "byte" }, 6));
                            break;
                        case ".pdf":
                            lv.Items.Add(new ListViewItem(
                        new string[] { item.Name, item.LastWriteTime.ToString(), "File", item.Length.ToString() + "byte" }, 7));
                            break;
                        default:
                            lv.Items.Add(new ListViewItem(
                        new string[] { item.Name, item.LastWriteTime.ToString(), "File", item.Length.ToString() + "byte" }, 0));
                            break;
                    }
                }
            } else
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();

                openFileDialog.InitialDirectory = directoryInfo.FullName;
                openFileDialog.RestoreDirectory = true;
                var fileContent = string.Empty;
                var filePath = string.Empty;


                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;

                    //Read the contents of the file into a stream
                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        fileContent = reader.ReadToEnd();
                    }
                }
            }

            lv.EndUpdate();
            edDir.Text = newDir;
            CurDir = newDir;

        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}