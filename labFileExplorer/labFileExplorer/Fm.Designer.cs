﻿namespace labFileExplorer
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Fm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.buBack = new System.Windows.Forms.ToolStripButton();
            this.buForward = new System.Windows.Forms.ToolStripButton();
            this.buUp = new System.Windows.Forms.ToolStripButton();
            this.edDir = new System.Windows.Forms.ToolStripTextBox();
            this.buDirSelect = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.miViewLargeIcon = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewSmallIcon = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewList = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewDetails = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewTile = new System.Windows.Forms.ToolStripMenuItem();
            this.buCopyFileNamesToClipboard = new System.Windows.Forms.ToolStripButton();
            this.ilLargeIcons = new System.Windows.Forms.ImageList(this.components);
            this.ilSmallIcons = new System.Windows.Forms.ImageList(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miPopupOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miPopupDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.miPopupProperty = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.miPopupCreateFolder = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.miPopupCreateFile = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.laStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tv = new System.Windows.Forms.TreeView();
            this.imTreeView = new System.Windows.Forms.ImageList(this.components);
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.lv = new System.Windows.Forms.ListView();
            this.toolStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buBack,
            this.buForward,
            this.buUp,
            this.edDir,
            this.buDirSelect,
            this.toolStripDropDownButton1,
            this.buCopyFileNamesToClipboard});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(886, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // buBack
            // 
            this.buBack.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buBack.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buBack.Name = "buBack";
            this.buBack.Size = new System.Drawing.Size(24, 22);
            this.buBack.Text = "<-";
            // 
            // buForward
            // 
            this.buForward.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buForward.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buForward.Name = "buForward";
            this.buForward.Size = new System.Drawing.Size(24, 22);
            this.buForward.Text = "->";
            // 
            // buUp
            // 
            this.buUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buUp.Name = "buUp";
            this.buUp.Size = new System.Drawing.Size(23, 22);
            this.buUp.Text = "^";
            // 
            // edDir
            // 
            this.edDir.Name = "edDir";
            this.edDir.Size = new System.Drawing.Size(600, 25);
            // 
            // buDirSelect
            // 
            this.buDirSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buDirSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buDirSelect.Name = "buDirSelect";
            this.buDirSelect.Size = new System.Drawing.Size(23, 22);
            this.buDirSelect.Text = "...";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miViewLargeIcon,
            this.miViewSmallIcon,
            this.miViewList,
            this.miViewDetails,
            this.miViewTile});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(40, 22);
            this.toolStripDropDownButton1.Text = "Вид";
            // 
            // miViewLargeIcon
            // 
            this.miViewLargeIcon.Name = "miViewLargeIcon";
            this.miViewLargeIcon.Size = new System.Drawing.Size(126, 22);
            this.miViewLargeIcon.Text = "LargeIcon";
            // 
            // miViewSmallIcon
            // 
            this.miViewSmallIcon.Name = "miViewSmallIcon";
            this.miViewSmallIcon.Size = new System.Drawing.Size(126, 22);
            this.miViewSmallIcon.Text = "SmallIcon";
            // 
            // miViewList
            // 
            this.miViewList.Name = "miViewList";
            this.miViewList.Size = new System.Drawing.Size(126, 22);
            this.miViewList.Text = "List";
            // 
            // miViewDetails
            // 
            this.miViewDetails.Name = "miViewDetails";
            this.miViewDetails.Size = new System.Drawing.Size(126, 22);
            this.miViewDetails.Text = "Details";
            // 
            // miViewTile
            // 
            this.miViewTile.Name = "miViewTile";
            this.miViewTile.Size = new System.Drawing.Size(126, 22);
            this.miViewTile.Text = "Tile";
            // 
            // buCopyFileNamesToClipboard
            // 
            this.buCopyFileNamesToClipboard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buCopyFileNamesToClipboard.Image = ((System.Drawing.Image)(resources.GetObject("buCopyFileNamesToClipboard.Image")));
            this.buCopyFileNamesToClipboard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buCopyFileNamesToClipboard.Name = "buCopyFileNamesToClipboard";
            this.buCopyFileNamesToClipboard.Size = new System.Drawing.Size(77, 22);
            this.buCopyFileNamesToClipboard.Text = "Copy names";
            // 
            // ilLargeIcons
            // 
            this.ilLargeIcons.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.ilLargeIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilLargeIcons.ImageStream")));
            this.ilLargeIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.ilLargeIcons.Images.SetKeyName(0, "file.png");
            this.ilLargeIcons.Images.SetKeyName(1, "folder.png");
            this.ilLargeIcons.Images.SetKeyName(2, "dll.png");
            this.ilLargeIcons.Images.SetKeyName(3, "exe.png");
            this.ilLargeIcons.Images.SetKeyName(4, "jpg.png");
            this.ilLargeIcons.Images.SetKeyName(5, "png.png");
            this.ilLargeIcons.Images.SetKeyName(6, "docx.png");
            this.ilLargeIcons.Images.SetKeyName(7, "thin.png");
            // 
            // ilSmallIcons
            // 
            this.ilSmallIcons.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.ilSmallIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilSmallIcons.ImageStream")));
            this.ilSmallIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.ilSmallIcons.Images.SetKeyName(0, "file.png");
            this.ilSmallIcons.Images.SetKeyName(1, "folder.png");
            this.ilSmallIcons.Images.SetKeyName(2, "dll.png");
            this.ilSmallIcons.Images.SetKeyName(3, "exe.png");
            this.ilSmallIcons.Images.SetKeyName(4, "jpg.png");
            this.ilSmallIcons.Images.SetKeyName(5, "png.png");
            this.ilSmallIcons.Images.SetKeyName(6, "docx.png");
            this.ilSmallIcons.Images.SetKeyName(7, "thin.png");
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miPopupOpen,
            this.toolStripSeparator1,
            this.miPopupDelete,
            this.toolStripSeparator2,
            this.miPopupProperty,
            this.toolStripSeparator3,
            this.miPopupCreateFolder,
            this.toolStripSeparator4,
            this.miPopupCreateFile});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(143, 138);
            // 
            // miPopupOpen
            // 
            this.miPopupOpen.Name = "miPopupOpen";
            this.miPopupOpen.Size = new System.Drawing.Size(142, 22);
            this.miPopupOpen.Text = "Open";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(139, 6);
            // 
            // miPopupDelete
            // 
            this.miPopupDelete.Name = "miPopupDelete";
            this.miPopupDelete.Size = new System.Drawing.Size(142, 22);
            this.miPopupDelete.Text = "Delete";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(139, 6);
            // 
            // miPopupProperty
            // 
            this.miPopupProperty.Name = "miPopupProperty";
            this.miPopupProperty.Size = new System.Drawing.Size(142, 22);
            this.miPopupProperty.Text = "Property";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(139, 6);
            // 
            // miPopupCreateFolder
            // 
            this.miPopupCreateFolder.Name = "miPopupCreateFolder";
            this.miPopupCreateFolder.Size = new System.Drawing.Size(142, 22);
            this.miPopupCreateFolder.Text = "Create folder";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(139, 6);
            // 
            // miPopupCreateFile
            // 
            this.miPopupCreateFile.Name = "miPopupCreateFile";
            this.miPopupCreateFile.Size = new System.Drawing.Size(142, 22);
            this.miPopupCreateFile.Text = "Create file";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.laStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(886, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // laStatus
            // 
            this.laStatus.Name = "laStatus";
            this.laStatus.Size = new System.Drawing.Size(39, 17);
            this.laStatus.Text = "Status";
            // 
            // tv
            // 
            this.tv.Dock = System.Windows.Forms.DockStyle.Left;
            this.tv.ImageIndex = 2;
            this.tv.ImageList = this.imTreeView;
            this.tv.Location = new System.Drawing.Point(0, 25);
            this.tv.Name = "tv";
            this.tv.SelectedImageIndex = 1;
            this.tv.Size = new System.Drawing.Size(162, 403);
            this.tv.TabIndex = 4;
            // 
            // imTreeView
            // 
            this.imTreeView.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imTreeView.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imTreeView.ImageStream")));
            this.imTreeView.TransparentColor = System.Drawing.Color.Transparent;
            this.imTreeView.Images.SetKeyName(0, "disk.png");
            this.imTreeView.Images.SetKeyName(1, "strel.png");
            this.imTreeView.Images.SetKeyName(2, "question_mark_school-128.png");
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(162, 25);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 403);
            this.splitter1.TabIndex = 5;
            this.splitter1.TabStop = false;
            // 
            // lv
            // 
            this.lv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lv.HideSelection = false;
            this.lv.LargeImageList = this.ilLargeIcons;
            this.lv.Location = new System.Drawing.Point(165, 25);
            this.lv.Name = "lv";
            this.lv.Size = new System.Drawing.Size(721, 403);
            this.lv.SmallImageList = this.ilSmallIcons;
            this.lv.TabIndex = 6;
            this.lv.UseCompatibleStateImageBehavior = false;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(886, 450);
            this.Controls.Add(this.lv);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.tv);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "Fm";
            this.Text = "labFileExplorer";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton buBack;
        private System.Windows.Forms.ToolStripButton buForward;
        private System.Windows.Forms.ToolStripButton buUp;
        private System.Windows.Forms.ToolStripTextBox edDir;
        private System.Windows.Forms.ToolStripButton buDirSelect;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem miViewLargeIcon;
        private System.Windows.Forms.ToolStripMenuItem miViewSmallIcon;
        private System.Windows.Forms.ToolStripMenuItem miViewList;
        private System.Windows.Forms.ToolStripMenuItem miViewDetails;
        private System.Windows.Forms.ToolStripMenuItem miViewTile;
        private System.Windows.Forms.ImageList ilLargeIcons;
        private System.Windows.Forms.ImageList ilSmallIcons;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miPopupOpen;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem miPopupDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem miPopupProperty;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel laStatus;
        private System.Windows.Forms.ToolStripButton buCopyFileNamesToClipboard;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem miPopupCreateFolder;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem miPopupCreateFile;
        private System.Windows.Forms.TreeView tv;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.ListView lv;
        private System.Windows.Forms.ImageList imTreeView;
    }
}

