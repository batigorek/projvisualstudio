﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace labFileExplorer
{
    public partial class FmFileProperties : Form
    {
        public FmFileProperties(string path)
        {
            
            InitializeComponent();

            if (File.Exists(path))
            {
                var x = new FileInfo(path);
                //this.Text = $"{x.Name}, {x.Extension}, {x.Length}";
                laName.Text = x.Name;
                laExtension.Text = x.Extension;
                laLength.Text = x.Length.ToString();
                laCreationTime.Text = x.CreationTime.ToString();
                laLastWriteTime.Text = x.LastWriteTime.ToString();
            }
            else if (Directory.Exists(path))
            {
                long len = 0;
                var x = new DirectoryInfo(path);
                laName.Text = x.Name;
                laExtension.Text = "Folder";
                //TODO for folder in folder
                    foreach (var item in x.GetFiles())
                    {
                        len += item.Length;
                    }
                laLength.Text = len.ToString();
                laCreationTime.Text = x.CreationTime.ToString();
                laLastWriteTime.Text = x.LastWriteTime.ToString();
            }
        }
    }
}
