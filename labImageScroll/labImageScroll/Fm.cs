﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageScroll
{
    public partial class Fm : Form
    {
        private readonly Bitmap b;
        private Point curPoint;
        private Point startPoint;

        public Fm()
        {
            InitializeComponent();

            b = new Bitmap(Properties.Resources.fonbig);

            pxImage.Paint += (s, e) => e.Graphics.DrawImage(b, curPoint);
            pxImage.MouseDown += (s, e) => startPoint = e.Location;
            pxImage.MouseMove += PxImage_MouseMove;

            // HW
            // wheel - change size of image under the cursor
        }

        private void PxImage_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                curPoint.X += e.X - startPoint.X;
                curPoint.Y += e.Y - startPoint.Y;
                startPoint = e.Location;
                pxImage.Invalidate();
            }
        }
    }
}
